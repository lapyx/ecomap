<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldUuiInRegions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('regions', function (Blueprint $table) {
            $table->string('uuid')->nullable()->after('color');
        });

        DB::statement("update `regions` set `uuid` = '3709f140-5bc6-4893-bee8-e3f895fe0464' where id=1;");  // Адыгея
        DB::statement("update `regions` set `uuid` = 'bdc95b2b-50eb-4a3f-a573-7108f47bddb6' where id=2;");  // Алтайский край                 Самарская область   941bde71-0d49-471d-b397-59c23265e063
        DB::statement("update `regions` set `uuid` = '60dc8e31-6edd-4701-9925-1b1ad93fbd1b' where id=3;");  // Амурская область                 Воронежская область e30e7bb3-3e5c-4d6f-8d68-e3d50444230b
        DB::statement("update `regions` set `uuid` = '99656850-0707-4152-a2e9-b930049306a0' where id=4;");  // Архангельская область                 Белгородская область    c539d481-c8d0-4bf8-b27d-e8f60ab4a587
        DB::statement("update `regions` set `uuid` = 'b14f0f03-b5f0-4aa4-b914-7d293834ca21' where id=5;");  // Астраханская область                 Брянская область    e230bf30-0539-4396-918b-0440bbf31875
        DB::statement("update `regions` set `uuid` = '18978def-9516-44df-a437-314e4748be3e' where id=6;");  // Башкортостан                 Владимирская область    4eed6ec0-c292-4f02-8811-bb7674d9b7a3
        DB::statement("update `regions` set `uuid` = 'c539d481-c8d0-4bf8-b27d-e8f60ab4a587' where id=7;");  // Белгородская область                 Ивановская область  387babeb-3f02-40c3-9ec4-fe6835668354
        DB::statement("update `regions` set `uuid` = 'e230bf30-0539-4396-918b-0440bbf31875' where id=8;");  // Брянская область                 Калужская область   93bd33b6-b97a-4567-82b0-5b11db1f91aa
        DB::statement("update `regions` set `uuid` = 'b05c69e5-c976-464e-8841-d7fd2a1acf81' where id=9;");  // Бурятия                 Костромская область 58d06561-4e96-421a-877e-5bb0f5e5a6ac
        DB::statement("update `regions` set `uuid` = '4eed6ec0-c292-4f02-8811-bb7674d9b7a3' where id=10"); //  ;Владимирская область                 Курская область 2a94cf3a-7ef7-42a8-b4cb-0d8e13d0c36f
        DB::statement("update `regions` set `uuid` = 'd4c9c0ad-c450-47aa-9d66-119dfa5c12f9' where id=11"); //  ;Волгоградская область                 Липецкая область    6b20f4b5-d1bb-4a12-8fb3-7482c4d74b3e
        DB::statement("update `regions` set `uuid` = '9c30ee40-4786-411b-8303-e710c4c4c807' where id=12"); //  ;Вологодская область                 Республика Коми e18b5019-e127-4994-8a00-b1de4f5a8275
        DB::statement("update `regions` set `uuid` = 'e30e7bb3-3e5c-4d6f-8d68-e3d50444230b' where id=13"); //  ;Воронежская область                 Орловская область   856d9ba6-6bf4-45e7-8830-5e5fed35191a
        DB::statement("update `regions` set `uuid` = '8714ec4c-91cd-4f37-9c39-98c564496db7' where id=14"); //  ;Дагестан                 Смоленская область  c751d80b-5d72-4319-85d6-86d5452d392c
        DB::statement("update `regions` set `uuid` = '66bfa55a-dc48-4535-8e0a-b51204421cdc' where id=15"); //  ;Еврейская автономная область                 Тамбовская область  1c22fc23-6842-4015-8c1f-1144aa5995c9
        DB::statement("update `regions` set `uuid` = '7d93d131-aece-4726-aacc-527a413ca05c' where id=16"); //  ;Забайкальский край                 Тверская область    9ab08bef-3d23-407e-a459-27488e54e59a
        DB::statement("update `regions` set `uuid` = '387babeb-3f02-40c3-9ec4-fe6835668354' where id=17"); //  ;Ивановская область                 Тульская область    f5e0dadc-d641-463a-b9d3-d9a494e4ecee
        DB::statement("update `regions` set `uuid` = 'e8257e7e-a7e3-42d7-b5b4-6eed2dff8d93' where id=18"); //  ;Ингушетия                 Ярославская область 6e0aa4ea-8125-43d9-a123-c4137f35d3d5
        DB::statement("update `regions` set `uuid` = '4564efa7-5d50-4b2f-872f-e5094f4f249f' where id=19"); //  ;Иркутская область                 Республика Карелия  e88f903d-06cf-4cb5-a7fb-e456cd9f1ff9
        DB::statement("update `regions` set `uuid` = 'aaca0e2e-01a0-482a-bf79-f300f4803148' where id=20"); //  ;Кабардино-Балкария                 Архангельская область   99656850-0707-4152-a2e9-b930049306a0
        DB::statement("update `regions` set `uuid` = 'c1b7e34c-0ace-4f5c-9fe4-7534eb3338e1' where id=21"); //  ;Калининградская область                 Ненецкий автономный округ   577e050b-2ba0-4be0-b679-3f1ef5a3c468
        DB::statement("update `regions` set `uuid` = '444019c6-a880-4d2d-a5ca-f23abdf2b7ba' where id=22"); //  ;Калмыкия                 Калининградская область c1b7e34c-0ace-4f5c-9fe4-7534eb3338e1
        DB::statement("update `regions` set `uuid` = '93bd33b6-b97a-4567-82b0-5b11db1f91aa' where id=23"); //  ;Калужская область                 Ленинградская область   439be0d6-1f7e-48eb-9228-3b974a0dd78f
        DB::statement("update `regions` set `uuid` = '60af856b-3972-4e9c-8312-9583a1e13e57' where id=24"); //  ;Камчатский край                 Санкт-Петербург d852f0b6-812b-45b2-89cf-47ec48e577c2
        DB::statement("update `regions` set `uuid` = 'd500eba9-d6b1-45bb-8203-ad2c5a881694' where id=25"); //  ;Карачаево-Черкесия                 Мурманская область  1e0c80ef-ebdf-4594-8554-0949b0b4d1ad
        DB::statement("update `regions` set `uuid` = '6ad6035f-de60-479a-afc2-a1841914aff0' where id=26"); //  ;Кемеровская область                 Новгородская область    b9970184-8543-4d2c-af61-4c545084face
        DB::statement("update `regions` set `uuid` = '0b953260-aa0e-447d-b836-fd415017db92' where id=27"); //  ;Кировская область                 Псковская область   16a4362b-ebd8-4edb-90b5-36e2e42ded5d
        DB::statement("update `regions` set `uuid` = '58d06561-4e96-421a-877e-5bb0f5e5a6ac' where id=28"); //  ;Костромская область                 Республика Адыгея   3709f140-5bc6-4893-bee8-e3f895fe0464
        DB::statement("update `regions` set `uuid` = 'ee82492b-5ca7-4a58-a589-38eb39057e86' where id=29"); //  ;Краснодарский край                 Республика Дагестан 8714ec4c-91cd-4f37-9c39-98c564496db7
        DB::statement("update `regions` set `uuid` = 'fda81e03-5e5e-4b8e-931b-791d7fb63c87' where id=30"); //  ;Красноярский край                 Республика Ингушетия    e8257e7e-a7e3-42d7-b5b4-6eed2dff8d93
        DB::statement("update `regions` set `uuid` = '99b4ebd8-e929-4d8e-b0aa-e72369271f6c' where id=31"); //  ;Курганская область                 Чеченская Республика    aead0b7d-7b5a-4dc2-abc7-40fbb454c64e
        DB::statement("update `regions` set `uuid` = '2a94cf3a-7ef7-42a8-b4cb-0d8e13d0c36f' where id=32"); //  ;Курская область                 Кабардино-Балкарская Республика aaca0e2e-01a0-482a-bf79-f300f4803148
        DB::statement("update `regions` set `uuid` = '439be0d6-1f7e-48eb-9228-3b974a0dd78f' where id=33"); //  ;Ленинградская область                 Республика Калмыкия 444019c6-a880-4d2d-a5ca-f23abdf2b7ba
        DB::statement("update `regions` set `uuid` = '6b20f4b5-d1bb-4a12-8fb3-7482c4d74b3e' where id=34"); //  ;Липецкая область                 Карачаево-Черкесская Республика d500eba9-d6b1-45bb-8203-ad2c5a881694
        DB::statement("update `regions` set `uuid` = '9b2f2d6d-785a-4f74-be9b-ef51d407e142' where id=35"); //  ;Магаданская область                 Республика Северная Осетия — Алания f31e4dec-0d77-4807-8a0a-6eae3b5ea51e
        DB::statement("update `regions` set `uuid` = '6ee469d3-10c7-459e-ba78-953e63509acd' where id=36"); //  ;Марий Эл                 Краснодарский край  ee82492b-5ca7-4a58-a589-38eb39057e86
        DB::statement("update `regions` set `uuid` = 'c42afce9-e413-4b01-b0eb-55d62da7105e' where id=37"); //  ;Мордовия                 Ставропольский край 883bbbb7-c450-440a-a79a-039e27f89773
        DB::statement("update `regions` set `uuid` = 'bd068ce0-155c-4989-93d0-f3bac1ac6131' where id=38"); //  ;Москва                 Астраханская область    b14f0f03-b5f0-4aa4-b914-7d293834ca21
        DB::statement("update `regions` set `uuid` = '2e9efc97-4e35-4205-a5a2-0a3b9896ec6f' where id=39"); //  ;Московская область                 Ростовская область  5f7003a1-b3f1-428d-9483-8da512164b97
        DB::statement("update `regions` set `uuid` = '1e0c80ef-ebdf-4594-8554-0949b0b4d1ad' where id=40"); //  ;Мурманская область                 Республика Башкортостан 18978def-9516-44df-a437-314e4748be3e
        DB::statement("update `regions` set `uuid` = '577e050b-2ba0-4be0-b679-3f1ef5a3c468' where id=41"); //  ;Ненецкий автономный округ                 Республика Марий Эл 6ee469d3-10c7-459e-ba78-953e63509acd
        DB::statement("update `regions` set `uuid` = 'e4178c63-5f2d-40e9-993a-00c04f35d09e' where id=42"); //  ;Нижегородская область                 Республика Мордовия c42afce9-e413-4b01-b0eb-55d62da7105e
        DB::statement("update `regions` set `uuid` = 'b9970184-8543-4d2c-af61-4c545084face' where id=43"); //  ;Новгородская область                 Республика Татарстан    124b6cc3-1d30-480d-af5a-6acf74a05941
        DB::statement("update `regions` set `uuid` = 'ad5e5c23-73ce-4f00-b76c-fe68d547503d' where id=44"); //  ;Новосибирская область                 Удмуртская Республика   0636431c-f5f5-4fa8-a68e-a431ace709d4
        DB::statement("update `regions` set `uuid` = '90870f3a-02ca-47c8-9cbe-d54b72ca7079' where id=45"); //  ;Омская область                 Чувашская Республика    5b26a42d-d584-4b28-adb0-80b891ce2c88
        DB::statement("update `regions` set `uuid` = '19afcbfd-c98d-4156-b55e-aa25a8b0ebf4' where id=46"); //  ;Оренбургская область                 Кировская область   0b953260-aa0e-447d-b836-fd415017db92
        DB::statement("update `regions` set `uuid` = '856d9ba6-6bf4-45e7-8830-5e5fed35191a' where id=47"); //  ;Орловская область                 Нижегородская область   e4178c63-5f2d-40e9-993a-00c04f35d09e
        DB::statement("update `regions` set `uuid` = 'cca52c8f-317f-42de-8b94-74002169345e' where id=48"); //  ;Пензенская область                 Оренбургская область    19afcbfd-c98d-4156-b55e-aa25a8b0ebf4
        DB::statement("update `regions` set `uuid` = '9036a621-6045-4473-b68a-75438a9624d4' where id=49"); //  ;Пермский край                 Пензенская область  cca52c8f-317f-42de-8b94-74002169345e
        DB::statement("update `regions` set `uuid` = 'c9b55bac-76e6-4483-87bd-23cd751b0d9a' where id=50"); //  ;Приморский край                 Саратовская область db34dc29-1506-4621-b5c6-51af3b30f100
        DB::statement("update `regions` set `uuid` = '16a4362b-ebd8-4edb-90b5-36e2e42ded5d' where id=51"); //  ;Псковская область                 Ульяновская область 0f10f030-84c9-4677-887f-e9299eab9823
        DB::statement("update `regions` set `uuid` = '9f34587e-b3d9-4b6c-9a27-279afae4cfe6' where id=52"); //  ;Республика Алтай                 Курганская область  99b4ebd8-e929-4d8e-b0aa-e72369271f6c
        DB::statement("update `regions` set `uuid` = 'e88f903d-06cf-4cb5-a7fb-e456cd9f1ff9' where id=53"); //  ;Республика Карелия                 Свердловская область    00fccd7b-6a18-4aed-9015-699a7b25fb4b
        DB::statement("update `regions` set `uuid` = 'e18b5019-e127-4994-8a00-b1de4f5a8275' where id=54"); //  ;Республика Коми                 Тюменская область   f64bbe32-dbf8-4a92-8d64-55a0802c2a58
        DB::statement("update `regions` set `uuid` = 'f59fc44b-646a-4636-a34e-6a2e3a77b4d7' where id=55"); //  ;Республика Крым                 Ханты-Мансийский автономный округ   575c712a-dfc8-4f09-aa58-a9c3c8bc7a2a
        DB::statement("update `regions` set `uuid` = '5f7003a1-b3f1-428d-9483-8da512164b97' where id=56"); //  ;Ростовская область                 Ямало-Ненецкий автономный округ 0b0b2d20-f6d8-4bc1-9440-bda0638fd070
        DB::statement("update `regions` set `uuid` = '57a53964-7c91-43bf-8812-c127b554eafc' where id=57"); //  ;Рязанская область                 Челябинская область 9e5aaf90-45fb-4f5a-8392-54b9585b1197
        DB::statement("update `regions` set `uuid` = '941bde71-0d49-471d-b397-59c23265e063' where id=58"); //  ;Самарская область                 Республика Алтай    9f34587e-b3d9-4b6c-9a27-279afae4cfe6
        DB::statement("update `regions` set `uuid` = 'd852f0b6-812b-45b2-89cf-47ec48e577c2' where id=59"); //  ;Санкт-Петербург                 Республика Бурятия  b05c69e5-c976-464e-8841-d7fd2a1acf81
        DB::statement("update `regions` set `uuid` = 'db34dc29-1506-4621-b5c6-51af3b30f100' where id=60"); //  ;Саратовская область                 Республика Тыва e453c696-fa7c-495a-93e5-2cf5e1092658
        DB::statement("update `regions` set `uuid` = 'fcc879e8-c7bb-422d-8fc3-e77555e40046' where id=61"); //  ;Сахалинская область                 Республика Хакасия  ad8d7565-e124-49a9-877e-e2bc09be5765
        DB::statement("update `regions` set `uuid` = '00fccd7b-6a18-4aed-9015-699a7b25fb4b' where id=62"); //  ;Свердловская область                 Алтайский край  bdc95b2b-50eb-4a3f-a573-7108f47bddb6
        DB::statement("update `regions` set `uuid` = 'c25e2ae5-bcff-4c30-b2ec-28083371b2e5' where id=63"); //  ;Севастополь                 Красноярский край   fda81e03-5e5e-4b8e-931b-791d7fb63c87
        DB::statement("update `regions` set `uuid` = 'f31e4dec-0d77-4807-8a0a-6eae3b5ea51e' where id=64"); //  ;Северная Осетия                 Москва  bd068ce0-155c-4989-93d0-f3bac1ac6131
        DB::statement("update `regions` set `uuid` = 'c751d80b-5d72-4319-85d6-86d5452d392c' where id=65"); //  ;Смоленская область                 Рязанская область   57a53964-7c91-43bf-8812-c127b554eafc
        DB::statement("update `regions` set `uuid` = '883bbbb7-c450-440a-a79a-039e27f89773' where id=66"); //  ;Ставропольский край                 Вологодская область 9c30ee40-4786-411b-8303-e710c4c4c807
        DB::statement("update `regions` set `uuid` = '1c22fc23-6842-4015-8c1f-1144aa5995c9' where id=67"); //  ;Тамбовская область                 Волгоградская область   d4c9c0ad-c450-47aa-9d66-119dfa5c12f9
        DB::statement("update `regions` set `uuid` = '124b6cc3-1d30-480d-af5a-6acf74a05941' where id=68"); //  ;Татарстан                 Кемеровская область 6ad6035f-de60-479a-afc2-a1841914aff0
        DB::statement("update `regions` set `uuid` = '9ab08bef-3d23-407e-a459-27488e54e59a' where id=69"); //  ;Тверская область                 Новосибирская область   ad5e5c23-73ce-4f00-b76c-fe68d547503d
        DB::statement("update `regions` set `uuid` = '3ad6334b-6ed0-4cbd-912a-6bfd329bf27d' where id=70"); //  ;Томская область                 Омская область  90870f3a-02ca-47c8-9cbe-d54b72ca7079
        DB::statement("update `regions` set `uuid` = 'f5e0dadc-d641-463a-b9d3-d9a494e4ecee' where id=71"); //  ;Тульская область                 Томская область 3ad6334b-6ed0-4cbd-912a-6bfd329bf27d
        DB::statement("update `regions` set `uuid` = 'e453c696-fa7c-495a-93e5-2cf5e1092658' where id=72"); //  ;Тыва                 Республика Саха (Якутия)    24f40612-6096-4341-b9ad-3e15577c94aa
        DB::statement("update `regions` set `uuid` = 'f64bbe32-dbf8-4a92-8d64-55a0802c2a58' where id=73"); //  ;Тюменская область                 Приморский край c9b55bac-76e6-4483-87bd-23cd751b0d9a
        DB::statement("update `regions` set `uuid` = '0636431c-f5f5-4fa8-a68e-a431ace709d4' where id=74"); //  ;Удмуртия                 Хабаровский край    f78ec8e3-03e3-46cc-ac58-c69aba640ad1
        DB::statement("update `regions` set `uuid` = '0f10f030-84c9-4677-887f-e9299eab9823' where id=75"); //  ;Ульяновская область                 Амурская область    60dc8e31-6edd-4701-9925-1b1ad93fbd1b
        DB::statement("update `regions` set `uuid` = 'f78ec8e3-03e3-46cc-ac58-c69aba640ad1' where id=76"); //  ;Хабаровский край                 Магаданская область 9b2f2d6d-785a-4f74-be9b-ef51d407e142
        DB::statement("update `regions` set `uuid` = 'ad8d7565-e124-49a9-877e-e2bc09be5765' where id=77"); //  ;Хакасия                 Сахалинская область fcc879e8-c7bb-422d-8fc3-e77555e40046
        DB::statement("update `regions` set `uuid` = '575c712a-dfc8-4f09-aa58-a9c3c8bc7a2a' where id=78"); //  ;Ханты-Мансийский автономный округ                 Еврейская автономная область    66bfa55a-dc48-4535-8e0a-b51204421cdc
        DB::statement("update `regions` set `uuid` = '9e5aaf90-45fb-4f5a-8392-54b9585b1197' where id=79"); //  ;Челябинская область                 Чукотский автономный округ  ffd14108-8243-4fcf-99c1-2f39f7f33cfd
        DB::statement("update `regions` set `uuid` = 'aead0b7d-7b5a-4dc2-abc7-40fbb454c64e' where id=80"); //  ;Чечня                 Севастополь c25e2ae5-bcff-4c30-b2ec-28083371b2e5
        DB::statement("update `regions` set `uuid` = '5b26a42d-d584-4b28-adb0-80b891ce2c88' where id=81"); //  ;Чувашия                 Пермский край   9036a621-6045-4473-b68a-75438a9624d4
        DB::statement("update `regions` set `uuid` = 'ffd14108-8243-4fcf-99c1-2f39f7f33cfd' where id=82"); //  ;Чукотский автономный округ                 Республика Крым f59fc44b-646a-4636-a34e-6a2e3a77b4d7
        DB::statement("update `regions` set `uuid` = '24f40612-6096-4341-b9ad-3e15577c94aa' where id=83"); //  ;Якутия                 Забайкальский край  7d93d131-aece-4726-aacc-527a413ca05c
        DB::statement("update `regions` set `uuid` = '0b0b2d20-f6d8-4bc1-9440-bda0638fd070' where id=84"); //  ;Ямало-Ненецкий автономный округ                 Камчатский край 60af856b-3972-4e9c-8312-9583a1e13e57
        DB::statement("update `regions` set `uuid` = '6e0aa4ea-8125-43d9-a123-c4137f35d3d5' where id=85"); //  ;Ярославская область                 Иркутская область   4564efa7-5d50-4b2f-872f-e5094f4f249f
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
