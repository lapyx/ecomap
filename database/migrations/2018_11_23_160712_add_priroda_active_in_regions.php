<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPrirodaActiveInRegions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('regions', function (Blueprint $table) {
            $table->boolean('priroda_active')->default(0)->after('federal_uuid');
        });

        DB::table('regions')->whereIn('id', [11, 2, 33, 9, 68, 58, 85, 39, 38, 57, 12, 19, 61])
            ->update(['priroda_active' => true]);

        // "Волгоградская область"
        // "Алтайский край"
        // "Ленинградская область"
        // "Республика Бурятия"
        // "Республика Татарстан"
        // "Самарская область"
        // "Ярославская область"
        // "Московская область"
        // "Москва"
        // "Рязанская область"
        // "Вологодская область"
        // "Иркутская область"
        // "Республика Саха (Якутия)"
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
