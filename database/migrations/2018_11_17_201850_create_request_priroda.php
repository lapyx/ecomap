<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestPriroda extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_priroda', function (Blueprint $table) {
            $table->increments('id');
            $table->string('priroda_id')->unique();
            $table->string('number')->nullable();
            $table->string('subject')->nullable();
            $table->string('address')->nullable();
            $table->text('description')->nullable();
            $table->string('name')->nullable();
            $table->text('comment')->nullable();
            $table->string('phone')->nullable();
            $table->text('map_point')->nullable();
            $table->text('answer')->nullable();
            $table->text('photo')->nullable();
            $table->string('status')->nullable();
            $table->integer('state_id')->default(1);
            $table->dateTime('date_state_repeat')->nullable(); // дата когда был отправлен повторно
            $table->string('status_code')->nullable();
            $table->integer('region_id')->nullable();
            $table->timestamps();

            $table->index('priroda_id');
            $table->index('number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_priroda');
    }
}
