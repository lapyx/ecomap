<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject')->nullable();
            $table->string('address')->nullable();
            $table->text('description')->nullable();
            $table->string('name')->nullable();
            $table->string('phone')->nullable();
            $table->text('map_point')->nullable();
            $table->string('photo')->nullable();
            $table->string('photo_admin')->nullable();
            $table->string('youtube')->nullable();
            $table->integer('status')->default(0);
            $table->integer('region_id')->default(0);
            $table->integer('participate')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
