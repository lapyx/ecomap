$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
        }
    });

    /* Regions colors */
    $('#reg-colors li').click(function () {
        $(this).parent().find('li').removeClass('active');
        $(this).addClass('active');
        $.ajax({
            type: "PUT",
            url: $(this).parent().attr('data-action'),
            data: {color: $(this).attr('data-color')}
        });
    });

    /**
     * Request filters
     */
    $('#select-status, #select-region').change(function () {
        $('#filter-form').submit();
    });

    /**
     * Request user photos
     */
    $('.delete-user-photo').click(function () {
        var photo = $(this).parents('.photo').find('a').attr('href');

        if (confirm('Вы уверены, что хотите удалить это фото?')) {
            $('#deleted-photos').append('<input type="hidden" name="deleted_photos[]" value="' + photo + '">');
            $(this).parents('.photo').fadeOut();
        }

    });

    /* Request photos */
    /*
     var photos = [];

     $('#req-select-files').click(function(){
     $('#req-files').click();
     });

     $('#req-files').change(function(){
     if (this.files && this.files[0]) {
     var reader = new FileReader();

     $('#request-photos').empty();

     for(var file in this.files)
     {
     photos.push(this.files[file]);
     $('#request-photos').append('<div><img src="'+ window.URL.createObjectURL(this.files[file]) +'" alt=""></div>');
     //init_delete_photos();
     }
     }
     });

     function init_delete_photos()
     {
     $('.delete-photo').unbind().click(function(){
     var id = $(this).attr('data-id');
     $(this).parent().remove();
     delete photos[id];
     });
     }

     if($('#request-photos').length > 0)
     {
     init_delete_photos();
     }
     */


    /**
     * Ajax form callback
     * @param form
     */
    function ajax_callback(form) {
        switch (form.attr('id')) {
            /* Donate form */
            case 'donate-form':
                location.reload();
                break;

            default:
                break;
        }
    }

    /**
     * Request photos
     */
    if ($('#edit-request-form').length > 0) {
        $('#upload-photos').click(function () {
            $('#fileupload').click();
        });

        var form = $('#edit-request-form');
        var files;

        $('#fileupload').fileupload({
            url: form.attr('action'),
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf)$/i,
            autoUpload: true,
            type: 'POST',
            singleFileUploads: false,
            submit: function (e, data) {
                $('#edit-request-form .progress').show()
                // data.formData = {
                // 	description: data.context.find('input[name="description"]').val(),
                // 	type: data.files[0].type,
                // 	size: data.files[0].size
                // }
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#edit-request-form .progress div').css('width', progress + '%').attr('aria-valuenow', progress);
            },
            done: function (e, data) {
                $('#edit-request-form .progress div').css('width', 0).attr('aria-valuenow', 0);

                var item, srcA, photo = data.result.photo, uploaded = $('#uploaded');
                for (var i in photo) {
                    if (photo.hasOwnProperty(i)) {
                        item = photo[i];
                        srcA = /\.doc(?=\?|#|$)/.test(item) ? '/images/doc.png' : item;
                        srcA = /\.docx(?=\?|#|$)/.test(item) ? '/images/doc.png' : item;
                        //srcA = (item.indexOf('.pdf')+1) ? '/images/pdf.png' : item;
                        uploaded.append('<div class="uploaded-photo">' +
                            '<div class="preview">' +
                            '<a href="' + item + '" rel="gallery_admin" class="fancybox" target="_blank">' +
                            '<img src="' + srcA + '" >' +
                            '</a>' +
                            '</div>' +
                            '<input class="hidden_image" type="image" name="files[' + i + ']" id="fileupload" multiple style="display: none" src="' + srcA + '">' +
                            '<button type="button" class="btn btn-xs btn-block btn-danger delete-file">Удалить</button>' +
                            '<div style="margin-top:5px;"><i class="fa fa-repeat" style="float: left;margin: 5px 5px 0 5px;" onclick="rotateImg(this,true)" aria-hidden="true"></i>Повернуть' +
                            '<i class="fa fa-undo" style="float:right ;margin: 5px 0 5px 5px;"onclick="rotateImg(this,false)" aria-hidden="true"></i></div>' +
                            '</div>');
                    }
                }
                initDeleteUploaded();
            }
        });

        function initDeleteUploaded() {
            $('#uploaded .delete-file').unbind().click(function () {
                $(this).parents('.uploaded-photo').hide();
                var file = $(this).parents('.uploaded-photo').find('a').attr('href');
                $('#edit-request-form').append('<input type="hidden" name="delete_photo[]" value="' + file + '">');
            });
        }

        initDeleteUploaded();

    }

    $('#uploaded img').each(function () {
        if (/\.doc(?=\?|#|$)/.test(this.src)||/\.docx(?=\?|#|$)/.test(this.src)) {
            this.src = '/images/doc.png';
            this.style.cssText = 'width:64px !important;height:64px';
        }
    });


});

function rotateImg(img, isClockwise) {
    var path = $(img).parent().parent().find('img')[0].src;
    $(img).parent().parent().find('img')[0].src = rotateBase64Image90deg($(img).parent().parent().find('img')[0].src, isClockwise);

    $.post(
            $('#edit-request-form').attr('action') + '/rotate_img',
        {
            'path': path,
            'src': $(img).parent().parent().find('img')[0].src
        }
        )
        .done(function (data) {
            $($(img).parent().parent().find('img')[0]).removeAttr('src');
            $($(img).parent().parent().find('img')[0]).attr('src', data.path+'?'+Math.random());
        });
}
function rotateBase64Image90deg(base64Image, isClockwise) {
    // create an off-screen canvas
    var offScreenCanvas = document.createElement('canvas');
    offScreenCanvasCtx = offScreenCanvas.getContext('2d');

    // cteate Image
    var img = new Image();
    img.src = base64Image;

    // set its dimension to rotated size
    offScreenCanvas.height = img.width;
    offScreenCanvas.width = img.height;

    // rotate and draw source image into the off-screen canvas:
    if (isClockwise) {
        offScreenCanvasCtx.rotate(90 * Math.PI / 180);
        offScreenCanvasCtx.translate(0, -offScreenCanvas.width);
    } else {
        offScreenCanvasCtx.rotate(-90 * Math.PI / 180);
        offScreenCanvasCtx.translate(-offScreenCanvas.height, 0);
    }
    offScreenCanvasCtx.drawImage(img, 0, 0);

    // encode image to data-uri with base64
    return offScreenCanvas.toDataURL("image/jpeg", 100);
}