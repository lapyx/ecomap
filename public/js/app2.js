/* axios v0.16.2 | (c) 2017 by Matt Zabriskie */

!function (e, t) {
    "object" == typeof exports && "object" == typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define([], t) : "object" == typeof exports ? exports.axios = t() : e.axios = t()
}(this, function () {
    return function (e) {
        function t(r) {
            if (n[r])return n[r].exports;
            var o = n[r] = {exports: {}, id: r, loaded: !1};
            return e[r].call(o.exports, o, o.exports, t), o.loaded = !0, o.exports
        }

        var n = {};
        return t.m = e, t.c = n, t.p = "", t(0)
    }([function (e, t, n) {
        e.exports = n(1)
    }, function (e, t, n) {
        "use strict";
        function r(e) {
            var t = new s(e), n = i(s.prototype.request, t);
            return o.extend(n, s.prototype, t), o.extend(n, t), n
        }

        var o = n(2), i = n(3), s = n(5), u = n(6), a = r(u);
        a.Axios = s, a.create = function (e) {
            return r(o.merge(u, e))
        }, a.Cancel = n(23), a.CancelToken = n(24), a.isCancel = n(20), a.all = function (e) {
            return Promise.all(e)
        }, a.spread = n(25), e.exports = a, e.exports.default = a
    }, function (e, t, n) {
        "use strict";
        function r(e) {
            return "[object Array]" === R.call(e)
        }

        function o(e) {
            return "[object ArrayBuffer]" === R.call(e)
        }

        function i(e) {
            return "undefined" != typeof FormData && e instanceof FormData
        }

        function s(e) {
            var t;
            return t = "undefined" != typeof ArrayBuffer && ArrayBuffer.isView ? ArrayBuffer.isView(e) : e && e.buffer && e.buffer instanceof ArrayBuffer
        }

        function u(e) {
            return "string" == typeof e
        }

        function a(e) {
            return "number" == typeof e
        }

        function c(e) {
            return "undefined" == typeof e
        }

        function f(e) {
            return null !== e && "object" == typeof e
        }

        function p(e) {
            return "[object Date]" === R.call(e)
        }

        function d(e) {
            return "[object File]" === R.call(e)
        }

        function l(e) {
            return "[object Blob]" === R.call(e)
        }

        function h(e) {
            return "[object Function]" === R.call(e)
        }

        function m(e) {
            return f(e) && h(e.pipe)
        }

        function y(e) {
            return "undefined" != typeof URLSearchParams && e instanceof URLSearchParams
        }

        function w(e) {
            return e.replace(/^\s*/, "").replace(/\s*$/, "")
        }

        function v() {
            return ("undefined" == typeof navigator || "ReactNative" !== navigator.product) && ("undefined" != typeof window && "undefined" != typeof document)
        }

        function g(e, t) {
            if (null !== e && "undefined" != typeof e)if ("object" == typeof e || r(e) || (e = [e]), r(e))for (var n = 0, o = e.length; n < o; n++)t.call(null, e[n], n, e); else for (var i in e)Object.prototype.hasOwnProperty.call(e, i) && t.call(null, e[i], i, e)
        }

        function x() {
            function e(e, n) {
                "object" == typeof t[n] && "object" == typeof e ? t[n] = x(t[n], e) : t[n] = e
            }

            for (var t = {}, n = 0, r = arguments.length; n < r; n++)g(arguments[n], e);
            return t
        }

        function b(e, t, n) {
            return g(t, function (t, r) {
                n && "function" == typeof t ? e[r] = E(t, n) : e[r] = t
            }), e
        }

        var E = n(3), C = n(4), R = Object.prototype.toString;
        e.exports = {
            isArray: r,
            isArrayBuffer: o,
            isBuffer: C,
            isFormData: i,
            isArrayBufferView: s,
            isString: u,
            isNumber: a,
            isObject: f,
            isUndefined: c,
            isDate: p,
            isFile: d,
            isBlob: l,
            isFunction: h,
            isStream: m,
            isURLSearchParams: y,
            isStandardBrowserEnv: v,
            forEach: g,
            merge: x,
            extend: b,
            trim: w
        }
    }, function (e, t) {
        "use strict";
        e.exports = function (e, t) {
            return function () {
                for (var n = new Array(arguments.length), r = 0; r < n.length; r++)n[r] = arguments[r];
                return e.apply(t, n)
            }
        }
    }, function (e, t) {
        function n(e) {
            return !!e.constructor && "function" == typeof e.constructor.isBuffer && e.constructor.isBuffer(e)
        }

        function r(e) {
            return "function" == typeof e.readFloatLE && "function" == typeof e.slice && n(e.slice(0, 0))
        }

        /*!
         * Determine if an object is a Buffer
         *
         * @author   Feross Aboukhadijeh <feross@feross.org> <http://feross.org>
         * @license  MIT
         */
        e.exports = function (e) {
            return null != e && (n(e) || r(e) || !!e._isBuffer)
        }
    }, function (e, t, n) {
        "use strict";
        function r(e) {
            this.defaults = e, this.interceptors = {request: new s, response: new s}
        }

        var o = n(6), i = n(2), s = n(17), u = n(18), a = n(21), c = n(22);
        r.prototype.request = function (e) {
            "string" == typeof e && (e = i.merge({url: arguments[0]}, arguments[1])), e = i.merge(o, this.defaults, {method: "get"}, e), e.method = e.method.toLowerCase(), e.baseURL && !a(e.url) && (e.url = c(e.baseURL, e.url));
            var t = [u, void 0], n = Promise.resolve(e);
            for (this.interceptors.request.forEach(function (e) {
                t.unshift(e.fulfilled, e.rejected)
            }), this.interceptors.response.forEach(function (e) {
                t.push(e.fulfilled, e.rejected)
            }); t.length;)n = n.then(t.shift(), t.shift());
            return n
        }, i.forEach(["delete", "get", "head", "options"], function (e) {
            r.prototype[e] = function (t, n) {
                return this.request(i.merge(n || {}, {method: e, url: t}))
            }
        }), i.forEach(["post", "put", "patch"], function (e) {
            r.prototype[e] = function (t, n, r) {
                return this.request(i.merge(r || {}, {method: e, url: t, data: n}))
            }
        }), e.exports = r
    }, function (e, t, n) {
        "use strict";
        function r(e, t) {
            !i.isUndefined(e) && i.isUndefined(e["Content-Type"]) && (e["Content-Type"] = t)
        }

        function o() {
            var e;
            return "undefined" != typeof XMLHttpRequest ? e = n(8) : "undefined" != typeof process && (e = n(8)), e
        }

        var i = n(2), s = n(7), u = {"Content-Type": "application/x-www-form-urlencoded"}, a = {
            adapter: o(),
            transformRequest: [function (e, t) {
                return s(t, "Content-Type"), i.isFormData(e) || i.isArrayBuffer(e) || i.isBuffer(e) || i.isStream(e) || i.isFile(e) || i.isBlob(e) ? e : i.isArrayBufferView(e) ? e.buffer : i.isURLSearchParams(e) ? (r(t, "application/x-www-form-urlencoded;charset=utf-8"), e.toString()) : i.isObject(e) ? (r(t, "application/json;charset=utf-8"), JSON.stringify(e)) : e
            }],
            transformResponse: [function (e) {
                if ("string" == typeof e)try {
                    e = JSON.parse(e)
                } catch (e) {
                }
                return e
            }],
            timeout: 0,
            xsrfCookieName: "XSRF-TOKEN",
            xsrfHeaderName: "X-XSRF-TOKEN",
            maxContentLength: -1,
            validateStatus: function (e) {
                return e >= 200 && e < 300
            }
        };
        a.headers = {common: {Accept: "application/json, text/plain, */*"}}, i.forEach(["delete", "get", "head"], function (e) {
            a.headers[e] = {}
        }), i.forEach(["post", "put", "patch"], function (e) {
            a.headers[e] = i.merge(u)
        }), e.exports = a
    }, function (e, t, n) {
        "use strict";
        var r = n(2);
        e.exports = function (e, t) {
            r.forEach(e, function (n, r) {
                r !== t && r.toUpperCase() === t.toUpperCase() && (e[t] = n, delete e[r])
            })
        }
    }, function (e, t, n) {
        "use strict";
        var r = n(2), o = n(9), i = n(12), s = n(13), u = n(14), a = n(10), c = "undefined" != typeof window && window.btoa && window.btoa.bind(window) || n(15);
        e.exports = function (e) {
            return new Promise(function (t, f) {
                var p = e.data, d = e.headers;
                r.isFormData(p) && delete d["Content-Type"];
                var l = new XMLHttpRequest, h = "onreadystatechange", m = !1;
                if ("undefined" == typeof window || !window.XDomainRequest || "withCredentials" in l || u(e.url) || (l = new window.XDomainRequest, h = "onload", m = !0, l.onprogress = function () {
                    }, l.ontimeout = function () {
                    }), e.auth) {
                    var y = e.auth.username || "", w = e.auth.password || "";
                    d.Authorization = "Basic " + c(y + ":" + w)
                }
                if (l.open(e.method.toUpperCase(), i(e.url, e.params, e.paramsSerializer), !0), l.timeout = e.timeout, l[h] = function () {
                        if (l && (4 === l.readyState || m) && (0 !== l.status || l.responseURL && 0 === l.responseURL.indexOf("file:"))) {
                            var n = "getAllResponseHeaders" in l ? s(l.getAllResponseHeaders()) : null, r = e.responseType && "text" !== e.responseType ? l.response : l.responseText, i = {
                                data: r,
                                status: 1223 === l.status ? 204 : l.status,
                                statusText: 1223 === l.status ? "No Content" : l.statusText,
                                headers: n,
                                config: e,
                                request: l
                            };
                            o(t, f, i), l = null
                        }
                    }, l.onerror = function () {
                        f(a("Network Error", e, null, l)), l = null
                    }, l.ontimeout = function () {
                        f(a("timeout of " + e.timeout + "ms exceeded", e, "ECONNABORTED", l)), l = null
                    }, r.isStandardBrowserEnv()) {
                    var v = n(16), g = (e.withCredentials || u(e.url)) && e.xsrfCookieName ? v.read(e.xsrfCookieName) : void 0;
                    g && (d[e.xsrfHeaderName] = g)
                }
                if ("setRequestHeader" in l && r.forEach(d, function (e, t) {
                        "undefined" == typeof p && "content-type" === t.toLowerCase() ? delete d[t] : l.setRequestHeader(t, e)
                    }), e.withCredentials && (l.withCredentials = !0), e.responseType)try {
                    l.responseType = e.responseType
                } catch (t) {
                    if ("json" !== e.responseType)throw t
                }
                "function" == typeof e.onDownloadProgress && l.addEventListener("progress", e.onDownloadProgress), "function" == typeof e.onUploadProgress && l.upload && l.upload.addEventListener("progress", e.onUploadProgress), e.cancelToken && e.cancelToken.promise.then(function (e) {
                    l && (l.abort(), f(e), l = null)
                }), void 0 === p && (p = null), l.send(p)
            })
        }
    }, function (e, t, n) {
        "use strict";
        var r = n(10);
        e.exports = function (e, t, n) {
            var o = n.config.validateStatus;
            n.status && o && !o(n.status) ? t(r("Request failed with status code " + n.status, n.config, null, n.request, n)) : e(n)
        }
    }, function (e, t, n) {
        "use strict";
        var r = n(11);
        e.exports = function (e, t, n, o, i) {
            var s = new Error(e);
            return r(s, t, n, o, i)
        }
    }, function (e, t) {
        "use strict";
        e.exports = function (e, t, n, r, o) {
            return e.config = t, n && (e.code = n), e.request = r, e.response = o, e
        }
    }, function (e, t, n) {
        "use strict";
        function r(e) {
            return encodeURIComponent(e).replace(/%40/gi, "@").replace(/%3A/gi, ":").replace(/%24/g, "$").replace(/%2C/gi, ",").replace(/%20/g, "+").replace(/%5B/gi, "[").replace(/%5D/gi, "]")
        }

        var o = n(2);
        e.exports = function (e, t, n) {
            if (!t)return e;
            var i;
            if (n)i = n(t); else if (o.isURLSearchParams(t))i = t.toString(); else {
                var s = [];
                o.forEach(t, function (e, t) {
                    null !== e && "undefined" != typeof e && (o.isArray(e) && (t += "[]"), o.isArray(e) || (e = [e]), o.forEach(e, function (e) {
                        o.isDate(e) ? e = e.toISOString() : o.isObject(e) && (e = JSON.stringify(e)), s.push(r(t) + "=" + r(e))
                    }))
                }), i = s.join("&")
            }
            return i && (e += (e.indexOf("?") === -1 ? "?" : "&") + i), e
        }
    }, function (e, t, n) {
        "use strict";
        var r = n(2);
        e.exports = function (e) {
            var t, n, o, i = {};
            return e ? (r.forEach(e.split("\n"), function (e) {
                o = e.indexOf(":"), t = r.trim(e.substr(0, o)).toLowerCase(), n = r.trim(e.substr(o + 1)), t && (i[t] = i[t] ? i[t] + ", " + n : n)
            }), i) : i
        }
    }, function (e, t, n) {
        "use strict";
        var r = n(2);
        e.exports = r.isStandardBrowserEnv() ? function () {
            function e(e) {
                var t = e;
                return n && (o.setAttribute("href", t), t = o.href), o.setAttribute("href", t), {
                    href: o.href,
                    protocol: o.protocol ? o.protocol.replace(/:$/, "") : "",
                    host: o.host,
                    search: o.search ? o.search.replace(/^\?/, "") : "",
                    hash: o.hash ? o.hash.replace(/^#/, "") : "",
                    hostname: o.hostname,
                    port: o.port,
                    pathname: "/" === o.pathname.charAt(0) ? o.pathname : "/" + o.pathname
                }
            }

            var t, n = /(msie|trident)/i.test(navigator.userAgent), o = document.createElement("a");
            return t = e(window.location.href), function (n) {
                var o = r.isString(n) ? e(n) : n;
                return o.protocol === t.protocol && o.host === t.host
            }
        }() : function () {
            return function () {
                return !0
            }
        }()
    }, function (e, t) {
        "use strict";
        function n() {
            this.message = "String contains an invalid character"
        }

        function r(e) {
            for (var t, r, i = String(e), s = "", u = 0, a = o; i.charAt(0 | u) || (a = "=", u % 1); s += a.charAt(63 & t >> 8 - u % 1 * 8)) {
                if (r = i.charCodeAt(u += .75), r > 255)throw new n;
                t = t << 8 | r
            }
            return s
        }

        var o = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
        n.prototype = new Error, n.prototype.code = 5, n.prototype.name = "InvalidCharacterError", e.exports = r
    }, function (e, t, n) {
        "use strict";
        var r = n(2);
        e.exports = r.isStandardBrowserEnv() ? function () {
            return {
                write: function (e, t, n, o, i, s) {
                    var u = [];
                    u.push(e + "=" + encodeURIComponent(t)), r.isNumber(n) && u.push("expires=" + new Date(n).toGMTString()), r.isString(o) && u.push("path=" + o), r.isString(i) && u.push("domain=" + i), s === !0 && u.push("secure"), document.cookie = u.join("; ")
                }, read: function (e) {
                    var t = document.cookie.match(new RegExp("(^|;\\s*)(" + e + ")=([^;]*)"));
                    return t ? decodeURIComponent(t[3]) : null
                }, remove: function (e) {
                    this.write(e, "", Date.now() - 864e5)
                }
            }
        }() : function () {
            return {
                write: function () {
                }, read: function () {
                    return null
                }, remove: function () {
                }
            }
        }()
    }, function (e, t, n) {
        "use strict";
        function r() {
            this.handlers = []
        }

        var o = n(2);
        r.prototype.use = function (e, t) {
            return this.handlers.push({fulfilled: e, rejected: t}), this.handlers.length - 1
        }, r.prototype.eject = function (e) {
            this.handlers[e] && (this.handlers[e] = null)
        }, r.prototype.forEach = function (e) {
            o.forEach(this.handlers, function (t) {
                null !== t && e(t)
            })
        }, e.exports = r
    }, function (e, t, n) {
        "use strict";
        function r(e) {
            e.cancelToken && e.cancelToken.throwIfRequested()
        }

        var o = n(2), i = n(19), s = n(20), u = n(6);
        e.exports = function (e) {
            r(e), e.headers = e.headers || {}, e.data = i(e.data, e.headers, e.transformRequest), e.headers = o.merge(e.headers.common || {}, e.headers[e.method] || {}, e.headers || {}), o.forEach(["delete", "get", "head", "post", "put", "patch", "common"], function (t) {
                delete e.headers[t]
            });
            var t = e.adapter || u.adapter;
            return t(e).then(function (t) {
                return r(e), t.data = i(t.data, t.headers, e.transformResponse), t
            }, function (t) {
                return s(t) || (r(e), t && t.response && (t.response.data = i(t.response.data, t.response.headers, e.transformResponse))), Promise.reject(t)
            })
        }
    }, function (e, t, n) {
        "use strict";
        var r = n(2);
        e.exports = function (e, t, n) {
            return r.forEach(n, function (n) {
                e = n(e, t)
            }), e
        }
    }, function (e, t) {
        "use strict";
        e.exports = function (e) {
            return !(!e || !e.__CANCEL__)
        }
    }, function (e, t) {
        "use strict";
        e.exports = function (e) {
            return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(e)
        }
    }, function (e, t) {
        "use strict";
        e.exports = function (e, t) {
            return t ? e.replace(/\/+$/, "") + "/" + t.replace(/^\/+/, "") : e
        }
    }, function (e, t) {
        "use strict";
        function n(e) {
            this.message = e
        }

        n.prototype.toString = function () {
            return "Cancel" + (this.message ? ": " + this.message : "")
        }, n.prototype.__CANCEL__ = !0, e.exports = n
    }, function (e, t, n) {
        "use strict";
        function r(e) {
            if ("function" != typeof e)throw new TypeError("executor must be a function.");
            var t;
            this.promise = new Promise(function (e) {
                t = e
            });
            var n = this;
            e(function (e) {
                n.reason || (n.reason = new o(e), t(n.reason))
            })
        }

        var o = n(23);
        r.prototype.throwIfRequested = function () {
            if (this.reason)throw this.reason
        }, r.source = function () {
            var e, t = new r(function (t) {
                e = t
            });
            return {token: t, cancel: e}
        }, e.exports = r
    }, function (e, t) {
        "use strict";
        e.exports = function (e) {
            return function (t) {
                return e.apply(null, t)
            }
        }
    }])
});
//# sourceMappingURL=axios.min.map

;

!function (t) {
    var e = {
        modules: t.ymaps.modules
    };
    !function (t) {
        if ("undefined" == typeof t && "function" == typeof require) var t = require("ym");
        t.define("util.providePackage", ["system.mergeImports"], function (t, e) {
            t(function (t, n) {
                var o = n[0],
                    r = Array.prototype.slice.call(n, 1),
                    i = e.joinImports(t.name, {}, t.deps, r);
                o(i)
            })
        })
    }(e.modules), e.modules.define("PieChartClusterer", ["Clusterer", "util.defineClass", "util.extend", "PieChartClusterer.icon.params", "PieChartClusterer.component.Canvas"], function (t, e, n, o, r, i) {
        function a(t) {
            return t.match(s)[1]
        }

        var s = /#(.+?)(?=Icon|DotIcon|StretchyIcon|CircleIcon|CircleDotIcon)/,
            c = n(function (t) {
                c.superclass.constructor.call(this, t), this._canvas = new i(r.icons.large.size), this._canvas.options.setParent(this.options)
            }, e, {
                createCluster: function (t, e) {
                    var n = c.superclass.createCluster.call(this, t, e),
                        i = e.reduce(function (t, e) {
                            var n = a(e.options.get("preset", "islands#blueIcon"));
                            return t[n] = ++t[n] || 1, t
                        }, {}),
                        s = this._canvas.generateIconDataURL(i, e.length),
                        l = {
                            clusterIcons: [o({
                                href: s
                            }, r.icons.small), o({
                                href: s
                            }, r.icons.medium), o({
                                href: s
                            }, r.icons.large)],
                            clusterNumbers: r.numbers
                        };
                    return n.options.set(l), n
                }
            });
        t(c)
    }), e.modules.define("PieChartClustererLayout", ["templateLayoutFactory", "util.extend", "PieChartClusterer.icon.params", "PieChartClusterer.component.Canvas"], function (t, e, n, o, r) {
        function i(t) {
            return t.match(a)[1]
        }

        var a = /#(.+?)(?=Icon|DotIcon|StretchyIcon|CircleIcon|CircleDotIcon)/,
            s = ["font:10pt arial,sans-serif", "position:absolute", "display:block", "text-align:center", "background-repeat:no-repeat", "background-position:center"],
            c = e.createClass(['<ymaps style="' + s.join(";") + '">', "{% if features %}", "{{ features.length }}", "{% else %}", "{{ properties.geoObjects.length }}", "{% endif %}", "</ymaps>"].join(""), {
                build: function () {
                    c.superclass.build.call(this);
                    var t = this.getData().features || this.getData().properties.get("geoObjects"),
                        e = t.length >= 100 ? "large" : t.length >= 10 ? "medium" : "small",
                        n = this._icon = o.icons[e],
                        a = n.size,
                        s = n.offset,
                        l = this.getParentElement().firstChild.firstChild,
                        u = new r(o.icons.large.size);
                    u.options.setParent(this.options);
                    var h = t.reduce(function (t, e) {
                            var n = e.options.preset || "function" == typeof e.options.get && e.options.get("preset") || "islands#blueIcon",
                                o = i(n);
                            return t[o] = ++t[o] || 1, t
                        }, {}),
                        p = u.generateIconDataURL(h, t.length);
                    l.style.width = a[0] + "px", l.style.height = a[1] + "px", l.style.left = s[0] + "px", l.style.top = s[1] + "px", l.style["line-height"] = a[1] + "px", l.style["background-image"] = "url(" + p + ")", l.style["background-size"] = a[0] + "px " + a[1] + "px"
                },
                clear: function () {
                    c.superclass.clear.call(this)
                },
                getShape: function () {
                    return this._icon.shape
                }
            });
        t(c)
    }), e.modules.define("RandomPointsGenerator", ["coordSystem.geo"], function (t, e) {
        function n(t) {
            this.count = t || 0
        }

        n.generate = function (t) {
            return new n(t)
        }, n.prototype.generate = function (t) {
            return this.count = t, this
        }, n.prototype.atBounds = function (t) {
            for (var e = [t[1][0] - t[0][0], t[1][1] - t[0][1]], n = [], o = 0; o < this.count; o++) n[o] = this.createPlacemark([Math.random() * e[0] + t[0][0], Math.random() * e[1] + t[0][1]], o);
            return n
        }, n.prototype.atCenterAndRadius = function (t, n) {
            for (var o = [], r = 0; r < this.count; r++) {
                var i = [Math.random() - Math.random(), Math.random() - Math.random()],
                    a = n * Math.random(),
                    s = e.solveDirectProblem(t, i, a).endPoint;
                o[r] = this.createPlacemark(s, r)
            }
            return o
        }, n.prototype.atCenterAndSize = function () {
        }, n.prototype.createPlacemark = function (t, e) {
            return new ymaps.GeoObject({
                geometry: {
                    type: "Point",
                    coordinates: t
                },
                properties: this.getPointData(e)
            }, this.getPointOptions(e))
        }, n.prototype.getPointData = function () {
            return {}
        }, n.prototype.getPointOptions = function () {
            return {}
        }, t(n)
    }), e.modules.define("PieChartClusterer.component.Canvas", ["option.Manager", "PieChartClusterer.icon.colors"], function (t, e, n) {
        var o = {
                canvasIconStrokeStyle: "white",
                canvasIconLineWidth: 2,
                canvasIconCoreRadius: 23,
                canvasIconCoreFillStyle: "white"
            },
            r = function (t) {
                this._canvas = document.createElement("canvas"), this._canvas.width = t[0], this._canvas.height = t[1], this._context = this._canvas.getContext("2d"), this.options = new e({})
            };
        r.prototype.generateIconDataURL = function (t, e) {
            return this._drawIcon(t, e), this._canvas.toDataURL()
        }, r.prototype._drawIcon = function (t, e) {
            var n = 0,
                r = 360,
                i = this._context,
                a = this._canvas.width / 2,
                s = this._canvas.height / 2,
                c = this.options.get("canvasIconLineWidth", o.canvasIconLineWidth),
                l = this.options.get("canvasIconStrokeStyle", o.canvasIconStrokeStyle),
                u = Math.floor((a + s - c) / 2);
            i.strokeStyle = l, i.lineWidth = c, Object.keys(t).forEach(function (o) {
                var c = t[o];
                r = n + 360 * c / e, i.fillStyle = this._getStyleColor(o), e > c ? n = this._drawSector(a, s, u, n, r) : this._drawCircle(a, s, u)
            }, this), this._drawCore(a, s)
        }, r.prototype._drawCore = function (t, e) {
            var n = this._context,
                r = this.options.get("canvasIconCoreFillStyle", o.canvasIconCoreFillStyle),
                i = this.options.get("canvasIconCoreRadius", o.canvasIconCoreRadius);
            n.fillStyle = r, this._drawCircle(t, e, i)
        }, r.prototype._drawCircle = function (t, e, n) {
            var o = this._context;
            o.beginPath(), o.arc(t, e, n, 0, 2 * Math.PI), o.fill(), o.stroke()
        }, r.prototype._drawSector = function (t, e, n, o, r) {
            var i = this._context;
            return i.beginPath(), i.moveTo(t, e), i.arc(t, e, n, this._toRadians(o), this._toRadians(r)), i.lineTo(t, e), i.closePath(), i.fill(), i.stroke(), r
        }, r.prototype._toRadians = function (t) {
            return t * Math.PI / 180
        }, r.prototype._getStyleColor = function (t) {
            return n[t]
        }, t(r)
    }), e.modules.define("PieChartClusterer.icon.colors", [], function (t) {
        var e = {
            blue: "#1E98FF",
            red: "#ED4543",
            darkOrange: "#E6761B",
            night: "#0E4779",
            darkBlue: "#177BC9",
            pink: "#F371D1",
            gray: "#B3B3B3",
            brown: "#793D0E",
            darkGreen: "#1BAD03",
            violet: "#B51EFF",
            black: "#595959",
            yellow: "#FFD21E",
            green: "#56DB40",
            orange: "#FF931E",
            lightBlue: "#82CDFF",
            olive: "#97A100"
        };
        t(e)
    }), e.modules.define("PieChartClusterer.icon.params", ["shape.Circle", "geometry.pixel.Circle"], function (t, e, n) {
        t({
            icons: {
                small: {
                    size: [46, 46],
                    offset: [-23, -23],
                    shape: new e(new n([0, 2], 21.5))
                },
                medium: {
                    size: [58, 58],
                    offset: [-29, -29],
                    shape: new e(new n([0, 2], 27.5))
                },
                large: {
                    size: [71, 71],
                    offset: [-35.5, -35.5],
                    shape: new e(new n([0, 2], 34))
                }
            },
            numbers: [10, 100]
        })
    })
}(this);

/**/
window.w = window;
/**
 * Module variables.
 */

w.fancy = new Object();

/*!
 * work methods
 */

String.prototype.each = function (callback) {
    arr = String.split('');
};

String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

fancy.dom = function (dom) {
    for (q in dom) {
        for (e in dom[q]) {
            for (r in dom[q].this) {
                $(document).on(r, q, dom[q].this[r]);
            }
        }
    }
};

w.cloneArray = function (o, t) {
    for (q in o)
        t[q] = o[q];
}

w.getRandomInt = function (min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

w.sortRegions = function (a, b) {
    return a.name.localeCompare(b.name);
}

/*plugin*/

//
fancy.responseUnits = {
    options: {
        lastRegion: false,
        RepV: false,
        nameLast: false,
        grayPoint: false,
        listNews: [],
        lastSetGray: false,
        grayStop: false
    },
    init: function () {

        window.curr = currentRequest;

        $$.addRegions();
        $$.addPoints();

    },
    addRegions: function () {
        ymaps.regions.load('RU', {
            lang: 'ru',
            quality: 3
        }).then(function (result) {
            var regions = result.geoObjects;
            $$.options.regions = [];
            regions.each(function (reg) {
                reg.options.set('preset', {
                    fillColor: 'rgba(242,240,197,.5)',
                    strokeColor: '639EF1',
                    strokeWidth: 1
                });

                var gname = reg.properties.get('name');
                $$.options.regions[gname] = reg;
            });

            regions.events.add('click', function (event) {
                if ($$.options.RepV
                        //&&lr&&lr.properties.get('name')==event.get('target').properties.get('name')
                    && !$$.options.grayStop) {

                    var placemark = new ymaps.Placemark(event.get('coords'), {}, {
                        iconLayout: 'default#image',
                        iconImageHref: '/images/pointer_inactive_sm.png',
                        iconImageSize: [32, 32],
                        iconImageOffset: [-16, -32]
                    });
                    myMap.geoObjects.add(placemark);

                    if ($$.options.lastSetGray)
                        myMap.geoObjects.remove($$.options.lastSetGray)

                    $$.options.lastSetGray = placemark;

                    $$.options.grayPoint = placemark;

                    var coord = placemark.geometry.getCoordinates();
                    var point = coord[0] + ', ' + coord[1];
                    ymaps.geocode(coord).then(function (res) {
                        var firstGeoObject = res.geoObjects.get(0);
                        var address = firstGeoObject.properties.get('text').replace('Россия, ', '');
                        $('#addressGray').val(address);
                    });

                    $('.text-info')
                        .html('<div class="text-success" style="margin-bottom: 1em;"><i class="material-icons">done</i> Место указано</div>');

                    return false;
                }

                $$.options.lastRegion = event.get('target');
                var region = event.get('target').properties.get('name');
                $$.options.nameLast = region;
                $$.selRegion(region);
            });

            $.get('/regions/ajax').then(function (response) {
                $$.options.regionColors = response.colors;
            });

            myMap.geoObjects.add(regions);

            if (!curr)
                $$.myRegion();
        })
    },
    myRegion: function () {
        ymaps.geolocation.get({
            provider: 'yandex',
            mapStateAutoApply: true
        }).then(function (result) {
            window.regs_t = result.geoObjects.get(0).properties.get('metaDataProperty').GeocoderMetaData.text;
            var searched = false;

            for (q in $$.options.regions) {
                var th = $$.options.regions[q];
                if (regs_t.indexOf(q) > 0) {
                    searched = regs_t.substr(regs_t.indexOf(q), q.length)
                }
            }

            if (searched)
                $$.options.regions[searched].events.fire('click', {
                    target: $$.options.regions[searched]
                });
        });
    },
    selRegion: function (name) {
        $$.options.nameLast = name;
        var region = $$.options.regions[name];
        $.get('/departments/' + name, function (e) {
            var $$this = e.department;
            var html = '';
            html += '<div class="form"><a href="#" class="close"></a>\
            <div>\
                <div class="text" style="margin: 2em 0px;">\
                    <p><strong>' + $$this['name'] + '</strong></p>\
                    <div>' + $$this.description + '</div>\
                    <div></div>';

            if ($$this.personal[0])
                html += '<div class="personals" style="margin: 2em 0px;">\
                        <div class="personal" style="margin: 0px 0px 1em;"><strong>' + $$this.personal[0]['name'] + '</strong><br>' + $$this.personal[0].position + '<br>\
                          \
                        </div>\
                    </div></div><div>';

            if (!$$.options.grayStop)
                html += '<a href="#" class="btn btn-primary btn1">Сообщить о нарушении</a><br><a href="#" class="btn btn-secondary" style="margin-top: 1em;">Закрыть</a>';

            html += '</div></div></div>';
            html += '</div>';

            $$.forms(html);
            $('.request-gallery .gallery-image a').fancybox();
        });

        myMap.setBounds(region.geometry.getBounds())

        for (q in $$.options.regions) {
            $$.options.regions[q].options.set({
                fillColor: 'rgba(242,240,197,.5)',
                strokeColor: '639EF1',
                strokeWidth: 1
            });
        }

        region.options.set({
            fillColor: 'rgba(255,243,239,.5)',
            strokeColor: '4CAF50',
            strokeWidth: 3
        });

        window.regtest = region;

    },
    addPoints: function () {
        var points = [];

        $.get('/requests/2/ajaxOne', function (response) {
            var requests = response.requests;

            $.get('/requests/2/ajaxTwo', function (response) {

                for (q in response.requests) {
                    requests[q] = response.requests[q];
                }

                $$.options.requests = requests;

                ymaps.modules.require(['PieChartClusterer'], function (PieChartClusterer) {
                    var clusterer = new PieChartClusterer();

                    var color;
                    var image;
                    var i = 0;

                    for (var request in requests) {
                        var obj_id = i++;
                        var tessss = requests[request].map_point;

                        if (0 == requests[request].status) {
                            color = 'islands#grayIcon';
                            image = "http://kartasvalok.ru/images/pointer_inactive_sm.png";
                        }

                        if (1 == requests[request].status) {
                            color = 'islands#redIcon';
                            image = "http://kartasvalok.ru/images/pointer_pending_sm.png";
                        }

                        if (2 == requests[request].status) {
                            color = 'islands#orangeIcon';
                            image = "http://kartasvalok.ru/images/pointer_in_progress_sm.png";
                        }

                        if (3 == requests[request].status) {
                            color = 'islands#greenStretchyIcon';
                            image = "http://kartasvalok.ru/images/pointer_done_sm.png";
                        }

                        var plac = new ymaps.Placemark(
                            [parseFloat(tessss[0]), parseFloat(tessss[1])],
                            requests[request].map_point, {
                                preset: color,
                                balloonContent: obj_id,
                                iconLayout: 'default#image',
                                iconImageHref: image,
                                iconImageSize: [30, 30],
                                iconImageOffset: [-5, -38]
                            }
                        );

                        plac.events.add('click', function (e) {
                            var thisPlacemark = e.get('target');
                            var id_t = parseFloat(thisPlacemark.options._options.balloonContent);

                            $.get('/port_query/' + $$.options.requests[id_t].id).then(function (response) {
                                var svalka = response.test[0];
                                $$.options.requests[id_t] = svalka;

                                myMap.setZoom(16);
                                myMap.setCenter(thisPlacemark.geometry.getCoordinates());
                                $$.showPointInfo(id_t);
                            })
                        });

                        if (!!curr && requests[request].id == curr) {
                            plac.events.fire('click', {
                                coordPosition: plac.geometry.getCoordinates(),
                                target: plac
                            });
                        }
                        points.push(plac);
                    }
                    clusterer.add(points);
                    myMap.geoObjects.add(clusterer);
                });

            })
        });
    },
    showPointInfo: function (id) {


        var $$this = $$.options.requests[id];
        var req_id = $$this.id;
        var uri = 'request/' + req_id;
        var meta_img = 'http://kartasvalok.ru' + $$this.map_image;
        var meta_url = 'http://kartasvalok.ru/' + uri;
        $('meta[property="og:image"]').attr('content', meta_img);
        $('link[rel="image_src"]').attr('href', meta_img);
        $('meta[property="image"]').attr('content', meta_img);
        $('meta[name="twitter:card"]').attr('content', meta_img);
        $('meta[name="twitter:image:src"]').attr('content', meta_img);

        $('meta[property="og:url"]').attr('content', meta_url);
        $('meta[name="twitter:url"]').attr('content', meta_url);




        var html = '';
        html += '<div class="form"><a href="#" class="close"></a>';

        if ($$this.status == 2) {
            html += '<p class="status small text-warning"><i aria-hidden="true" class="fa fa-hourglass"></i> В работе</p>';
        }

        if ($$this.status == 1) {
            html += '<p class="status small text-danger"><i aria-hidden="true" class="fa fa-info-circle"></i> Обращение находится в стадии рассмотрения</p>';
        }

        if ($$this.status == 3) {
            html += '<p class="status small text-success"><i aria-hidden="true" class="fa fa-check"></i> Проблема решена</p>';
        }

        html += '<p style="margin:1em 0 5px;white-space: pre-line;"><i>' + $$this.comment + '</i><br></p>';
        html += '';

        html += '<div class="request-gallery request-info">';
        for (q in $$this.photo_preview_admin) {
            var srcPhoto = $$this.photo_preview_admin[q]['preview'];
            var stylePhoto = '';
            if (/\.doc(?=\?|#|$)/.test(srcPhoto) || /\.docx(?=\?|#|$)/.test(srcPhoto)) {
                srcPhoto = '/images/doc.png';
                stylePhoto = 'width:130px !important;height:130px;border:none;';
                html += '<div >';
                html += '<a href="' + $$this.photo_preview_admin[q]['file'] + '" rel="gallery_admin" target="_blank" class="fancybox">';
            }
            else {
                html += '<div class="gallery-image">';
                html += '<a href="' + $$this.photo_preview_admin[q]['file'] + '" rel="gallery_admin"  class="fancybox">';
            }
            html += '<img src="' + srcPhoto + '" style="max-width:100%;' + stylePhoto + '"></a></div>';
        }
        html += '</div>';

        html += '<span class="info__note info__note--time"><i class="fa fa-clock-o"></i>' + $$this.updated_at + '</span>';
        html += '<p class="info__header">' + $$this.subject + '</p>';
        html += '<p class="info__description">' + $$this.description + '</p>';
        html += '<p class="info__note"><i class="fa fa-map-marker"></i>' + $$this.address + '</p>';

        html += '<div class="request-gallery request-info">';
        for (q in $$this.photo_preview) {
            var srcPhoto = $$this.photo_preview[q]['preview'];
            var stylePhoto = '';
            if (/\.doc(?=\?|#|$)/.test(srcPhoto) || /\.docx(?=\?|#|$)/.test(srcPhoto)) {
                srcPhoto = '/images/doc.png';
                stylePhoto = 'width:130px !important;height:130px;border:none;';
                html += '<div >';
                html += '<a href="' + $$this.photo_preview[q]['file'] + '" target="_blank" rel="gallery_admin" class="fancybox">';
            } else {
                html += '<div class="gallery-image">';
                html += '<a href="' + $$this.photo_preview[q]['file'] + '" rel="gallery_admin" class="fancybox">';
            }
            html += '<img src="' + srcPhoto + '" style="max-width:100%;' + stylePhoto + '"></a></div>';
        }
        html += '</div>';

        $('.pluso').remove();

        html += '<div style="margin-top:2em;margin-bottom:1.5em;"><input type="text" id="lnk-field" class="lnk-input" value=http://kartasvalok.ru/request/' + req_id + '>' +
            '<button class="btn btn-primary lnk-copy btn2">Скопировать</button> <div class="copy-hint"><small>Ссылка скопирована в буфер обмена</small></div></div>' +
            '<div class="pluso" data-background="transparent" data-options="medium,square,line,horizontal,nocounter,theme=04" data-services="vkontakte,facebook,twitter,odnoklassniki"' +
            ' data-url="'+meta_url+'"></div>' +
            '<br/><a href="#" class="btn btn-secondary">Закрыть</a>';

        html += '</div>';

        $$.forms(html);
        $('.request-gallery .gallery-image a').fancybox();

        var stateParameters = {};

        history.pushState(stateParameters, 'Интерактивная карта свалок', '/');
        history.pushState(stateParameters, 'Интерактивная карта свалок', uri);
        history.pathname = uri;
        //social
        if (window.pluso)
            window.pluso.start();

    },
    regions: function () {

        var $$this = $$.options.requests[id];
        var html = '';

        html += '<div class="form"><a href="#" class="close"></a>';


        $$.forms(html);

    },
    forms: function (html) {
        var code = '<div class="body active"><div class="body-filter"></div>';
        code += html;
        code += '</div>';

        $(code).appendTo('body')
    },
    removeForm: function () {
        //if($$.options.lastSetGray)
        //myMap.geoObjects.remove($$.options.lastSetGray)

        $$.options.RepV = false;
        $('.body.active').remove();
        for (q in $$.options.regions) {
            $$.options.regions[q].options.set({
                fillColor: 'rgba(242,240,197,.5)',
                strokeColor: '639EF1',
                strokeWidth: 1
            });
        }

        var stateParameters = {};
        var uri = '/';
        history.pushState(stateParameters, 'Интерактивная карта свалок', uri);
        history.pathname = uri;

    },
    newsShow: function () {
        var html = '<div class="page"><a href="#" class="close"></a>';
        html += '<h1 style="text-align:center;margin:0px 0px 1em;">Новости</h1>';

        function htmlfunc() {
            for (q in $$.options.listNews) {
                html += '<div class="news__item clickOnf" data-index="' + q + '"><a href="#">' + $$.options.listNews[q].name + '</a></div>';
            }

            $$.forms(html);
        }

        if ($$.options.listNews.length > 0) {
            htmlfunc();
        } else {
            $.get('news', function (data) {
                $$.options.listNews = data.news.data;
                $.get('news?page=2', function (data) {
                    for (q in data.news.data) {
                        $$.options.listNews.push(data.news.data[q]);
                    }
                    htmlfunc();
                });
            })
        }

    }
};

ymaps.ready(init);
var myMap;

var $$ = fancy.responseUnits;

function init() {
    myMap = new ymaps.Map("map", {
        center: [55.76, 37.64],
        zoom: 5,
        controls: ['zoomControl', 'typeSelector']
    });

    fancy.responseUnits.init()
    $('.navigation a').on('click', function () {
        var type = $(this).attr('id');
        var html = '';

        html += '<div class="page"><a href="#" class="close"></a>';


        if (type == 'news') {
            $$.newsShow();
        } else {
            if (type == 'page?id=2')
                html += '<h1 style="text-align:center;margin:0 0 1em;">О проекте</h1> <div>Президент России, лидер Общероссийского народного фронта Владимир Путин по итогам «Форума действий» ОНФ, выступил за создание общественной Интернет-карты, на который любой пользователь мог бы оставить сообщение и обозначить на ней незаконную свалку. В исполнении поручения Президента, Центр общественного мониторинга ОНФ по проблемам экологии и защиты леса в Год экологии (2017) запустил проект «Генеральная уборка» </div> <h2 style="text-align:center;margin:1em 0px;">Партнёры:</h2> <table style="width:60%;margin:0px auto;"><tr><td colspan="3" style="text-align:center;"><img src="/images/russia-today.png" style="margin-left:2em;"></td></tr> <tr><td style="text-align:right;"><img src="/images/greenparty.svg" style="width:200px;margin-left:1.5em;"></td> <td style="text-align:right;"><img src="/images/logo_RSSM.jpg" style="width:200px;margin-left:1.5em;"></td> <td style="text-align:left;"><img src="/images/unarmy.png" style="width:180px;margin-left:2em;"></td></tr> <tr><td colspan="3" style="text-align:center;"><img src="/images/green-patrol.png" style="margin-left:2em;"></td></tr></table></div>';
            if (type == 'page?id=1')
                html += '<h1 style="text-align:center;margin:0 0 1em;">Контакты</h1> <div>Адрес: 119285, г. Москва, ул. Мосфильмовская, д. 40<br>Телефон: 8 (495) 981-56-99<br>Факс: 8 (495) 980-25-61<br>E-mail: eco@onf.ru<br><br>Пресс-служба Телефон: 8 (495) 980-25-62 (для представителей СМИ) E-mail: press@onf.ru</div></div>';
            if (type == 'instructions')
                html += '<h1 style="text-align:center;margin:0 0 1em;">Инструкция по работе с «Интерактивной картой свалок»</h1> <div class="news"><p><img src="/images/helps/help.jpg" alt="Инструкция" style="width:100%;"></p><p><img src="/images/helps/requests.jpg" alt="Инструкция" style="width:100%;"></p></div></div>';

            $$.forms(html);
        }


    })
}

fancy.dom({
    '.body .close,.body .body-filter,.body .btn-secondary': {
        this: {
            'click': function (e) {
                $$.removeForm();
            }
        }
    },
    '.form-control': {
        this: {
            'change': function (e) {
                var name = $(this).find('option:selected').text();
                $$.selRegion(name);
            }
        }
    },
    '[fil=image_file]': {
        this: {
            'change': function (e) {
                var $files = $(this)[0].files;
                var $par = $(this).closest('.form-group').find('.request-gallery');
                var cour = 0;

                function for_img(i) {
                    var reader = new FileReader();
                    reader.readAsDataURL($files[i]);
                    reader.onload = function (e) {
                        $('<div class="gallery-image"><img ' +
                            'onclick="this.src=rotateBase64Image90deg(this.src,true);" ' +
                            'src="' + e.target.result + '">' +
                            '<button class="removeImgGal">×</button>' +
                            '</div>')
                            .appendTo($par);

                        if ($files.length > (cour + 1))
                            for_img(++cour);
                    };
                }

                for_img(cour);
            }
        }
    },
    '.btn-primary.btn2': {
        this: {
            'click': function (e) {
                $inp = $('#lnk-field');
                $this = $(this);

                if ($('.copy-hint.active').length == 0)
                    $('<div class="copy-hint active"><small>Ссылка скопирована в буфер обмена</small></div>')
                        .insertAfter($this);

                $inp.select();
                document.execCommand("copy");
            }
        }
    },
    '.btn-primary.btn1': {
        this: {
            'click': function (e) {
                if ($(this).attr('disabled')) {
                    return;
                }
                $(this).attr('disabled', 'disabled');
                if (!$$.options.RepV) {
                    var html = '<div class="form"><a href="#" class="close"></a><div><div class="form__fields">' +
                        '<div class="form-group"><div class="form-control__container">' +
                        '<input fil="subject" type="text" id="" aria-describedby="emailHelp" placeholder="Тема" multiple="multiple" class="form-control check_have"> </div>' +
                        '<div class="form-control__error"></div></div><div class="form-group">' +
                        '<div class="form-control__container">' +
                        '<input type="text" aria-describedby="emailHelp" id="addressGray" fil="address" placeholder="Адрес обнаружения проблемы" ' +
                        'multiple="multiple" class="form-control check_have"> </div><div class="form-control__error"></div></div>' +
                        '<div class="text-info" style="margin-bottom:1em;"><i class="material-icons">info outline</i> Укажите место на карте</div>' +
                        '<div class="form-group"><div class="form-control__container">' +
                        '<textarea id="" aria-describedby="emailHelp" fil="description" placeholder="Описание жалобы" class="form-control check_have"></textarea>' +
                        '</div></div><div class="form-group"><label for="">Фото и/или видео</label><div class="request-gallery"></div>' +
//                        '<div class="form-control__container"><input type="file" fil="image_file" class="check_have" aria-describedby="emailHelp" placeholder="" multiple="multiple">' +
'<div class="form-control__container"><div class="fileform"><div class="selectbutton">Загрузить фото</div><input id="upload" name="upload" type="file" fil="image_file" class="check_have" aria-describedby="emailHelp"  placeholder="" multiple="multiple"></div>'+
                        '</div><div class="form-control__error"><small class="form-text text-muted">Несколько файлов, не более 50 Мб</small></div></div>' +
                        '<div class="form__header">Контактная информация</div><div class="form-group"><div class="form-control__container">' +
                        '<input type="text" id="" aria-describedby="emailHelp" fil="name" placeholder="Ваше ФИО" multiple="multiple" class="form-control check_have">' +
                        '</div><div class="form-control__error"><small class="form-text text-muted">Видны только администрации сайта</small></div></div>' +
                        '<div class="form-group"><div class="form-control__container"><input type="text" id="" aria-describedby="emailHelp" fil="phone" placeholder="Номер телефона" ' +
                        'multiple="multiple" class="form-control check_have"> </div><div class="form-control__error"><small class="form-text text-muted">' +
                        'Контакты нужны для оповещения о статусе вашей заявки</small></div></div><div class="form-group">' +
                        '<div class="form-control__container"><input type="email" id="" aria-describedby="emailHelp" fil="email" placeholder="Email" multiple="multiple" class="form-control"> ' +
                        '</div><div class="form-control__error"></div></div><div class="form-group">' +
                        '<div class="form-control__container"><input fil="agree" type="checkbox" id="ch-agree" aria-describedby="emailHelp" placeholder="Пользовательское соглашение" ' +
                        'multiple="multiple" value=""> <label for="ch-agree" class="request-agree">Я принимаю условия <a href="#">Пользовательского соглашения</a></label> ' +
                        '</div><div class="form-control__error"></div></div><div class="form-group"><div class="form-control__container">' +
                        '<input fil="participate" type="checkbox" id="ch-participate" aria-describedby="emailHelp" placeholder="Участие в ликвидации" multiple="multiple" value="">' +
                        '<label for="ch-participate" class="request-agree">Я хочу поучаствовать в ликвидации экологического нарушения</label></div>' +
                        '<div class="form-control__error"></div></div></div> <a href="#" class="btn btn-primary btn1">Сообщить о нарушении</a>' +
                        '<a href="#" class="btn btn-secondary">Отмена</a></div></div>';
                    $$.options.RepV = true;
                    $$.forms(html);
                    $('.body-filter').remove();
                    $(this).removeAttr('disabled');
                } else {
                    $('.form__fields .form-control__error').remove();
                    $('.form__fields .has-error').removeClass('has-error');

                    var err_check = false;
                    $('.form__fields .check_have').each(function () {
                        var $this = $(this);
                        var $parent = $this.closest('.form-group');
                        if (!$this.val()) {
                            err_check = true;
                            $parent.addClass('has-error');
                            $('<div class="form-control__error"><small class="form-text text-danger">Обязательное поле</small></div>')
                                .appendTo($parent);
                        }
                    });

                    if (!$('#ch-agree').prop('checked')) {
                        var $this = $('#ch-agree');
                        var $parent = $this.closest('.form-group');
                        err_check = true;
                        $parent.addClass('has-error');
                        $('<div class="form-control__error"><small class="form-text text-danger">Обязательное поле</small></div>')
                            .appendTo($parent);
                    }

                    if ($('[fil=phone]').val() && $('[fil=phone]').val().length < 10) {
                        err_check = true;
                        var $par = $('[fil=phone]').closest('.form-group')
                        $('<div class="form-control__error"><small class="form-text text-danger">Минимум 10 цифр</small></div>')
                            .appendTo($par);
                    }

                    if (!err_check) {

                        var formData = new FormData();
                        formData.append('subject', $('[fil=subject]').val());
                        formData.append('description', $('[fil=description]').val());
                        formData.append('address', $('[fil=address]').val());

                        for (var file in $('[fil=image_file]')[0].files) {
                            formData.append('photo[]', $('[fil=image_file]')[0].files[file]);
                        }

                        formData.append('name', $('[fil=name]').val());
                        formData.append('phone', $('[fil=phone]').val());
                        formData.append('email', $('[fil=email]').val());
                        formData.append('youtube', '');
                        if ($('select[name=region_name] option:selected').text() === "Выбрать регион") {
                            if ($$.options.nameLast) {
                                formData.append('region_name', $$.options.nameLast);
                                //console.log('$$.options.nameLast', $$.options.nameLast);
                            } else {

                                if ($$.options.lastRegion) {
                                    formData.append('region_name', $$.options.lastRegion.properties.get('name'));
                                    //console.log('$$.options.lastRegion', $$.options.lastRegion);
                                }
                            }
                        }
                        else {
                            formData.append('region_name', $('select[name=region_name] option:selected').text());
                        }
                        formData.append('agree', '1');
                        formData.append('participate', $('#ch-participate').prop('checked') ? 1 : 0);
                        formData.append('map_point', $$.options.grayPoint.geometry.getCoordinates());

                        window.testx = formData;

                        axios.post('/requests', testx)
                            .then(function (response) {
                                var data_id = response.data.request_id;

                                $$.removeForm();
                                var html = '<div class="page"><a href="#" class="close"></a>' +
                                    '<div class="thank-you"><div><h1>Спасибо!</h1> ' +
                                    '<p>Вашему обращению присвоен номер ' + data_id + '</p>' +
                                    '<p>Ваше обращение будет рассмотрено региональными модераторами в течение 2-х рабочих дней.' +
                                    'После чего обращению будет присвоен статус «на рассмотрении», и оно появится на публичной карте.</p>' +
                                    '<p>Сейчас Вы можете видеть свое обращение в виде серой метки на карте. После прохождения модерации она станет красной.</p>' +
                                    '<p>Оповещения по статусу Вашего обращения будут высылаться на указанный Вами адрес электронной почты.</p></div></div></div>';
                                $$.forms(html);
                                $$.options.lastSetGray = false;
                                $$.options.grayStop = true;
                                setTimeout(function () {
                                    $$.options.grayStop = false;
                                }, 1000 * 60);
                                $(this).removeAttr('disabled');
                            })
                    } else {
                        $(this).removeAttr('disabled');
                    }
                }
            }
        }
    },
    '.removeImgGal': {
        this: {
            'click': function (e) {
                $(this).closest('.gallery-image')
                    .remove();
            }
        }
    },
    '.clickOnf a': {
        this: {
            'click': function (e) {
                var conn = $(this).closest('.news__item').data('index');
                var $th = $$.options.listNews[conn];
                var html = '<div class="page"><a href="#" class="close"></a><h1 style="text-align:center;margin:0px 0px 1em;">' + $th.name + '</h1>';
                html += '<div style="text-align: center;"><a href="#" style="font-size: 0.9em; margin-bottom: 2em; display: block;" id="backNews">Вернуться к новостям</a></div>';
                html += '<div class="news">' + $th.text + '</div>';

                html += '</div></div>';

                $$.removeForm();
                $$.forms(html);
            }
        }
    },
    '#backNews': {
        this: {
            'click': function (e) {
                $$.newsShow();
            }
        }
    }
});

function rotateBase64Image90deg(base64Image, isClockwise) {
    // create an off-screen canvas
    var offScreenCanvas = document.createElement('canvas');
    offScreenCanvasCtx = offScreenCanvas.getContext('2d');

    // cteate Image
    var img = new Image();
    img.src = base64Image;

    // set its dimension to rotated size
    offScreenCanvas.height = img.width;
    offScreenCanvas.width = img.height;

    // rotate and draw source image into the off-screen canvas:
    if (isClockwise) {
        offScreenCanvasCtx.rotate(90 * Math.PI / 180);
        offScreenCanvasCtx.translate(0, -offScreenCanvas.width);
    } else {
        offScreenCanvasCtx.rotate(-90 * Math.PI / 180);
        offScreenCanvasCtx.translate(-offScreenCanvas.height, 0);
    }
    offScreenCanvasCtx.drawImage(img, 0, 0);

    // encode image to data-uri with base64
    return offScreenCanvas.toDataURL("image/jpeg", 100);
}

