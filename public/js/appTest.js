! function(t) {
    var e = {
        modules: t.ymaps.modules
    };
    ! function(t) {
        if ("undefined" == typeof t && "function" == typeof require) var t = require("ym");
        t.define("util.providePackage", ["system.mergeImports"], function(t, e) {
            t(function(t, n) {
                var o = n[0],
                    r = Array.prototype.slice.call(n, 1),
                    i = e.joinImports(t.name, {}, t.deps, r);
                o(i)
            })
        })
    }(e.modules), e.modules.define("PieChartClusterer", ["Clusterer", "util.defineClass", "util.extend", "PieChartClusterer.icon.params", "PieChartClusterer.component.Canvas"], function(t, e, n, o, r, i) {
        function a(t) {
            return t.match(s)[1]
        }
        var s = /#(.+?)(?=Icon|DotIcon|StretchyIcon|CircleIcon|CircleDotIcon)/,
            c = n(function(t) {
                c.superclass.constructor.call(this, t), this._canvas = new i(r.icons.large.size), this._canvas.options.setParent(this.options)
            }, e, {
                createCluster: function(t, e) {
                    var n = c.superclass.createCluster.call(this, t, e),
                        i = e.reduce(function(t, e) {
                            var n = a(e.options.get("preset", "islands#blueIcon"));
                            return t[n] = ++t[n] || 1, t
                        }, {}),
                        s = this._canvas.generateIconDataURL(i, e.length),
                        l = {
                            clusterIcons: [o({
                                href: s
                            }, r.icons.small), o({
                                href: s
                            }, r.icons.medium), o({
                                href: s
                            }, r.icons.large)],
                            clusterNumbers: r.numbers
                        };
                    return n.options.set(l), n
                }
            });
        t(c)
    }), e.modules.define("PieChartClustererLayout", ["templateLayoutFactory", "util.extend", "PieChartClusterer.icon.params", "PieChartClusterer.component.Canvas"], function(t, e, n, o, r) {
        function i(t) {
            return t.match(a)[1]
        }
        var a = /#(.+?)(?=Icon|DotIcon|StretchyIcon|CircleIcon|CircleDotIcon)/,
            s = ["font:10pt arial,sans-serif", "position:absolute", "display:block", "text-align:center", "background-repeat:no-repeat", "background-position:center"],
            c = e.createClass(['<ymaps style="' + s.join(";") + '">', "{% if features %}", "{{ features.length }}", "{% else %}", "{{ properties.geoObjects.length }}", "{% endif %}", "</ymaps>"].join(""), {
                build: function() {
                    c.superclass.build.call(this);
                    var t = this.getData().features || this.getData().properties.get("geoObjects"),
                        e = t.length >= 100 ? "large" : t.length >= 10 ? "medium" : "small",
                        n = this._icon = o.icons[e],
                        a = n.size,
                        s = n.offset,
                        l = this.getParentElement().firstChild.firstChild,
                        u = new r(o.icons.large.size);
                    u.options.setParent(this.options);
                    var h = t.reduce(function(t, e) {
                            var n = e.options.preset || "function" == typeof e.options.get && e.options.get("preset") || "islands#blueIcon",
                                o = i(n);
                            return t[o] = ++t[o] || 1, t
                        }, {}),
                        p = u.generateIconDataURL(h, t.length);
                    l.style.width = a[0] + "px", l.style.height = a[1] + "px", l.style.left = s[0] + "px", l.style.top = s[1] + "px", l.style["line-height"] = a[1] + "px", l.style["background-image"] = "url(" + p + ")", l.style["background-size"] = a[0] + "px " + a[1] + "px"
                },
                clear: function() {
                    c.superclass.clear.call(this)
                },
                getShape: function() {
                    return this._icon.shape
                }
            });
        t(c)
    }), e.modules.define("RandomPointsGenerator", ["coordSystem.geo"], function(t, e) {
        function n(t) {
            this.count = t || 0
        }
        n.generate = function(t) {
            return new n(t)
        }, n.prototype.generate = function(t) {
            return this.count = t, this
        }, n.prototype.atBounds = function(t) {
            for (var e = [t[1][0] - t[0][0], t[1][1] - t[0][1]], n = [], o = 0; o < this.count; o++) n[o] = this.createPlacemark([Math.random() * e[0] + t[0][0], Math.random() * e[1] + t[0][1]], o);
            return n
        }, n.prototype.atCenterAndRadius = function(t, n) {
            for (var o = [], r = 0; r < this.count; r++) {
                var i = [Math.random() - Math.random(), Math.random() - Math.random()],
                    a = n * Math.random(),
                    s = e.solveDirectProblem(t, i, a).endPoint;
                o[r] = this.createPlacemark(s, r)
            }
            return o
        }, n.prototype.atCenterAndSize = function() {}, n.prototype.createPlacemark = function(t, e) {
            return new ymaps.GeoObject({
                geometry: {
                    type: "Point",
                    coordinates: t
                },
                properties: this.getPointData(e)
            }, this.getPointOptions(e))
        }, n.prototype.getPointData = function() {
            return {}
        }, n.prototype.getPointOptions = function() {
            return {}
        }, t(n)
    }), e.modules.define("PieChartClusterer.component.Canvas", ["option.Manager", "PieChartClusterer.icon.colors"], function(t, e, n) {
        var o = {
                canvasIconStrokeStyle: "white",
                canvasIconLineWidth: 2,
                canvasIconCoreRadius: 23,
                canvasIconCoreFillStyle: "white"
            },
            r = function(t) {
                this._canvas = document.createElement("canvas"), this._canvas.width = t[0], this._canvas.height = t[1], this._context = this._canvas.getContext("2d"), this.options = new e({})
            };
        r.prototype.generateIconDataURL = function(t, e) {
            return this._drawIcon(t, e), this._canvas.toDataURL()
        }, r.prototype._drawIcon = function(t, e) {
            var n = 0,
                r = 360,
                i = this._context,
                a = this._canvas.width / 2,
                s = this._canvas.height / 2,
                c = this.options.get("canvasIconLineWidth", o.canvasIconLineWidth),
                l = this.options.get("canvasIconStrokeStyle", o.canvasIconStrokeStyle),
                u = Math.floor((a + s - c) / 2);
            i.strokeStyle = l, i.lineWidth = c, Object.keys(t).forEach(function(o) {
                var c = t[o];
                r = n + 360 * c / e, i.fillStyle = this._getStyleColor(o), e > c ? n = this._drawSector(a, s, u, n, r) : this._drawCircle(a, s, u)
            }, this), this._drawCore(a, s)
        }, r.prototype._drawCore = function(t, e) {
            var n = this._context,
                r = this.options.get("canvasIconCoreFillStyle", o.canvasIconCoreFillStyle),
                i = this.options.get("canvasIconCoreRadius", o.canvasIconCoreRadius);
            n.fillStyle = r, this._drawCircle(t, e, i)
        }, r.prototype._drawCircle = function(t, e, n) {
            var o = this._context;
            o.beginPath(), o.arc(t, e, n, 0, 2 * Math.PI), o.fill(), o.stroke()
        }, r.prototype._drawSector = function(t, e, n, o, r) {
            var i = this._context;
            return i.beginPath(), i.moveTo(t, e), i.arc(t, e, n, this._toRadians(o), this._toRadians(r)), i.lineTo(t, e), i.closePath(), i.fill(), i.stroke(), r
        }, r.prototype._toRadians = function(t) {
            return t * Math.PI / 180
        }, r.prototype._getStyleColor = function(t) {
            return n[t]
        }, t(r)
    }), e.modules.define("PieChartClusterer.icon.colors", [], function(t) {
        var e = {
            blue: "#1E98FF",
            red: "#ED4543",
            darkOrange: "#E6761B",
            night: "#0E4779",
            darkBlue: "#177BC9",
            pink: "#F371D1",
            gray: "#B3B3B3",
            brown: "#793D0E",
            darkGreen: "#1BAD03",
            violet: "#B51EFF",
            black: "#595959",
            yellow: "#FFD21E",
            green: "#56DB40",
            orange: "#FF931E",
            lightBlue: "#82CDFF",
            olive: "#97A100"
        };
        t(e)
    }), e.modules.define("PieChartClusterer.icon.params", ["shape.Circle", "geometry.pixel.Circle"], function(t, e, n) {
        t({
            icons: {
                small: {
                    size: [46, 46],
                    offset: [-23, -23],
                    shape: new e(new n([0, 2], 21.5))
                },
                medium: {
                    size: [58, 58],
                    offset: [-29, -29],
                    shape: new e(new n([0, 2], 27.5))
                },
                large: {
                    size: [71, 71],
                    offset: [-35.5, -35.5],
                    shape: new e(new n([0, 2], 34))
                }
            },
            numbers: [10, 100]
        })
    })
}(this);

	/**/
		window.w = window;
	/**
	 * Module variables.
	 */
	
		w.fancy = new Object();
	
	/*!
	 * work methods
	 */

	String.prototype.each = function (callback) {
		arr = String.split('');
	};

	String.prototype.replaceAll = function(search, replacement) {
    	var target = this;
    	return target.replace(new RegExp(search, 'g'), replacement);
	};
	
	fancy.dom = function (dom) {
		for (q in dom) {			
			for (e in dom[q]) {	
				for (r in dom[q].this) {
					$(document).on(r, q, dom[q].this[r]);	
				}			
			}		
		}
	};

	w.cloneArray = function (o,t) {
		for(q in o)
			t[q] = o[q];
	}

	w.getRandomInt = function (min, max) {
  	return Math.floor(Math.random() * (max - min)) + min;
	}

 	/*plugin*/

 	//
 	fancy.responseUnits = {
 		init: function(){
 			var points = [];
 			$.get('/requests/2/ajaxOne',function(response){
 				var requests=response.requests;

 				ymaps.modules.require(['PieChartClusterer'],function(PieChartClusterer){
 					var clusterer = new PieChartClusterer();

 					var color;
        	var image;
        	var i = 0;
	
 					for (var request in requests) {
        	  var obj_id = i++;
        	  var tessss = requests[request].map_point;
	
        	  var plac = new ymaps.Placemark(
        	      [parseFloat(tessss[0]), parseFloat(tessss[1])],
        	      requests[request].map_point, {
        	        preset: 'islands#grayIcon',
        	        balloonContent: obj_id,
        	        iconLayout: 'default#image',
        	        iconImageHref: image,
        	        iconImageSize: [30, 30],
        	        iconImageOffset: [-5, -38]
        	      }
        	  );
	
        	  points.push(plac);
        	}

        	clusterer.add(points);
          myMap.geoObjects.add(clusterer);

        });

 			});
		}
 	};
	
	ymaps.ready(init);
  var myMap;

  function init(){     
   	myMap = new ymaps.Map("map", {
       center: [55.76, 37.64],
       zoom: 7
   	});

   	fancy.responseUnits.init()
  }