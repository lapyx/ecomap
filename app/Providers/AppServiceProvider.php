<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
		/**
		 * Validate coordinates
		 */
		Validator::extend('mappoint', function ($attribute, $value, $parameters, $validator) {
			$result = false;
			$points = explode(', ', $value);

			if(isset($points[0]) && isset($points[1]) && count($points) == 2) {
				if(preg_match('/^[+\-]?[0-9]{1,3}\.[0-9]{3,}\z/', $points[0]) && preg_match('/^[+\-]?[0-9]{1,3}\.[0-9]{3,}\z/', $points[1])) {
					$result = true;
				}
			}
			return $result;
		});
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
