<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
	public $fillable = ['color', 'uuid'];

    // Регионы для которых есть возможность отправлять обращения в систему "Наша природа"
    const REGIONS_PRIRODA_OK = [176095];

	public function requests()
	{
		return $this->hasMany('App\Request', 'region_id');
	}
}
