<?php

namespace App\Http\Controllers\Admin;

use App\Department;
use App\Http\Controllers\Controller;
use App\Region;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Maatwebsite\Excel\Collections\SheetCollection;
use phpDocumentor\Reflection\DocBlock\Tags\Formatter;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends Controller
{

    const SORTBY_MODERATE = 0;
    const SORTBY_RASSMOTRENIE = 1;
    const SORTBY_IN_WORK = 2;
    const SORTBY_SOLVE = 3;
    const SORTBY_REGION = 'name';
    const SORTBY_EFFECT = 'effective';
    const SORTBY_NUMBERS = 'total';
	const SORTBY_EFFECT2 = 'effect2';

    /**
     * @param DatabaseManager $databaseManager
     */
    public function __construct(DatabaseManager $databaseManager)
    {
        $this->databaseManager = $databaseManager;
    }

    public function index(Request $request)
    {

        if (Auth::user()->department_id) {
            $department = Department::find(Auth::user()->department_id);

            return view('admin.index', [
                'total' => \App\Request::where('region_name', $department->region_name)->where('status', '>=', 0)->count(),
                'moderate' => \App\Request::where('region_name', $department->region_name)->where('status', 0)->count(),
                'pending' => \App\Request::where('region_name', $department->region_name)->where('status', 1)->count(),
                'work' => \App\Request::where('region_name', $department->region_name)->where('status', 2)->count(),
                'done' => \App\Request::where('region_name', $department->region_name)->where('status', 3)->count(),
                'department_title' => $department->region_name
            ]);
        }

        $sortable_value = $request->get('sort');
        $regions = $this->getRegionsArray($sortable_value);


        return view('admin.index', [
            'total' => \App\Request::where('status', '>=', 0)->count(),
            'moderate' => \App\Request::where('status', 0)->count(),
            'pending' => \App\Request::where('status', 1)->count(),
            'work' => \App\Request::where('status', 2)->count(),
            'done' => \App\Request::where('status', 3)->count(),
            'regions' => $regions,
        ]);
    }


    public function export()
    {
        $sortable_value = self::SORTBY_EFFECT2;
        session(['sort' => ['what' => $sortable_value, 'desc' => false]]);
        $data = $this->getRegionsArray($sortable_value);


        return \Excel::create('Stat', function ($excel) use ($data) {

            /** @var Maatwebsite\Excel\Writers\LaravelExcelWriter $excel */
            $excel->sheet(
                'Статистика', function ($sheet) use ($data) {


                $stat = [
                    'total' =>  \App\Request::where('status', '>=', 0)->count(),
                    'moderate' => \App\Request::where('status', 0)->count(),
                    'pending' => \App\Request::where('status', 1)->count(),
                    'work' => \App\Request::where('status', 2)->count(),
                    'done' => \App\Request::where('status', 3)->count()];
                $i = 1;
                $sheet->appendRow(['По состоянию на '.date('H:i:s').'    '.date('Y.m.d')]);
                $sheet->appendRow(++$i,['Всего обращений: '.$stat['total'].', из них:']);
                $sheet->appendRow(++$i,['На модерации: '.$stat['moderate']]);
                $sheet->appendRow(++$i,['Обращений в стадии рассмотрения: '.$stat['pending']]);
                $sheet->appendRow(++$i,['В работе: '.$stat['work']]);
                $sheet->appendRow(++$i,['Проблем решено: '.$stat['done']]);


                $i++;
                /** @var \Maatwebsite\Excel\Classes\LaravelExcelWorksheet $sheet */
                $sheet->appendRow(++$i,[
                    'Регион',
                    'На модерации',
                    'На рассмотрении',
                    'В работе',
                    'Проблема решена',
                    'Всего',
                    'Эффективность',
                ]);



                foreach ($data as $item) {
                    array_splice($item, 0, 2);
                    $sheet->appendRow(++$i, $item);
                }

            });


        })->download();

    }

    private function getRegionsArray($sortable_value = null)
    {
        $requests_status0 = \App\Request::
        where('status', '=', 0)
            ->groupBy('region_id')
            ->orderBy('region_id')
            ->get(['region_id', $this->databaseManager->raw("count(region_id) AS count")])
            ->keyBy('region_id')->toArray();
        $requests_status1 = \App\Request::
        where('status', '=', 1)
            ->groupBy('region_id')
            ->orderBy('region_id')
            ->get(['region_id', $this->databaseManager->raw("count(region_id) AS count")])
            ->keyBy('region_id')->toArray();
        $requests_status2 = \App\Request::
        where('status', '=', 2)
            ->groupBy('region_id')
            ->orderBy('region_id')
            ->get(['region_id', $this->databaseManager->raw("count(region_id) AS count")])
            ->keyBy('region_id')->toArray();
        $requests_status3 = \App\Request::
        where('status', '=', 3)
            ->groupBy('region_id')
            ->orderBy('region_id')
            ->get(['region_id', $this->databaseManager->raw("count(region_id) AS count")])
            ->keyBy('region_id')->toArray();
        $requests_status_all = \App\Request::
        groupBy('region_id')
            ->orderBy('region_id')
            ->get(['region_id', $this->databaseManager->raw("count(region_id) AS count")])
            ->keyBy('region_id')->toArray();

        $regions = Region::all(['id', 'region_id', 'name'])->toArray();
        foreach ($regions as &$region) {
            $region[\App\Request::STATUS_MODERATE] = isset($requests_status0[$region['region_id']]) ? $requests_status0[$region['region_id']]['count'] : 0;
            $region[\App\Request::STATUS_RASSMOTRENIE] = isset($requests_status1[$region['region_id']]) ? $requests_status1[$region['region_id']]['count'] : 0;
            $region[\App\Request::STATUS_IN_WORK] = isset($requests_status2[$region['region_id']]) ? $requests_status2[$region['region_id']]['count'] : 0;
            $region[\App\Request::STATUS_SOLVE] = isset($requests_status3[$region['region_id']]) ? $requests_status3[$region['region_id']]['count'] : 0;
            $region['total'] = isset($requests_status_all[$region['region_id']]) ? $requests_status_all[$region['region_id']]['count'] : 0;
			//
			if ($region['total'] !== 0) {
                $region['effect2'] = ( $region[\App\Request::STATUS_IN_WORK] + 2 * $region[\App\Request::STATUS_SOLVE] ) / $region['total'];
            } else {
                $region['effect2'] = 0;
            }
			//
        }
        unset($region);

        $this->sortArray($regions, $sortable_value);
        return $regions;
    }

    private function sortArray(array &$regions, $sortable_value = null)
    {
        if ($sortable_value !== null) {
            $sort_desc = false;
            $old_sort = session('sort');

            if ($old_sort['what'] == $sortable_value) {
                $sort_desc = !$old_sort['desc'];
                session(['sort' => ['what' => $sortable_value, 'desc' => $sort_desc]]);
            } else {
                session(['sort' => ['what' => $sortable_value, 'desc' => false]]);
            }
            uasort($regions, function ($a, $b) use ($sortable_value, $sort_desc) {
                if ($a[$sortable_value] == $b[$sortable_value]) {
                    return 0;
                }

                if (is_string($a[$sortable_value])) {
                    if ($sort_desc) {
                        return strcmp($b[$sortable_value], $a[$sortable_value]);
                    }
                    return strcmp($a[$sortable_value], $b[$sortable_value]);
                }

                if ($sort_desc) {
                    return ($a[$sortable_value] < $b[$sortable_value]) ? 1 : -1;
                }

                return ($a[$sortable_value] < $b[$sortable_value]) ? -1 : 1;
            });
        }
    }
}
