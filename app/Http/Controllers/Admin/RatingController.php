<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Region;
use App\Request;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class RatingController extends Controller
{
    /**
     * @param DatabaseManager $databaseManager
     */
    public function __construct(DatabaseManager $databaseManager)
    {
        $this->databaseManager = $databaseManager;
    }

    /* Rating */
    public function index()
    {
        if (!Auth::guest() && Auth::user()->department_id) {
            return redirect('/admin');
        }
//\DB::enableQueryLog();


        $requests_status0 = Request::
        where('status', '=', 0)
            ->groupBy('region_id')
            ->orderBy('region_id')
            ->get(['region_id', $this->databaseManager->raw("count(region_id) AS count")])
            ->keyBy('region_id')->toArray();
        $requests_status1 = Request::
        where('status', '=', 1)
            ->groupBy('region_id')
            ->orderBy('region_id')
            ->get(['region_id', $this->databaseManager->raw("count(region_id) AS count")])
            ->keyBy('region_id')->toArray();
        $requests_status2 = Request::
        where('status', '=', 2)
            ->groupBy('region_id')
            ->orderBy('region_id')
            ->get(['region_id', $this->databaseManager->raw("count(region_id) AS count")])
            ->keyBy('region_id')->toArray();
        $requests_status3 = Request::
        where('status', '=', 3)
            ->groupBy('region_id')
            ->orderBy('region_id')
            ->get(['region_id', $this->databaseManager->raw("count(region_id) AS count")])
            ->keyBy('region_id')->toArray();

        $regions = Region::all(['id','region_id', 'name']);
//dd($requests_status1);
//		dd(\DB::getQueryLog());
        return view('admin.rating.index', ['regions' => $regions,
            'requests_status0' => $requests_status0,
            'requests_status1' => $requests_status1,
            'requests_status2' => $requests_status2,
            'requests_status3' => $requests_status3,]);
    }
}
