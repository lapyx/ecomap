<?php

namespace App\Http\Controllers\Admin;

use App\Department;
use App\Region;
use App\RequestPriroda;
use App\Services\ApiPriroda;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class PrirodaController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();

        $filtered = false;
        /** @var Builder $prirodaRequest */
        $prirodaRequest = RequestPriroda::where('created_at', '>', 0);
        /**
         * Filter status
         */
        $filter = $request->all();
        if (array_get($filter, 'state_id')) {
            $prirodaRequest = $prirodaRequest->where('state_id', '=', $filter['state_id']);
            $filtered = true;
        } else {
            if ($user->isAdmin()) {
                $prirodaRequest = $prirodaRequest->where('state_id', '=', RequestPriroda::STATE_ADMIN);
            } else {
                $prirodaRequest = $prirodaRequest->whereNotIn('state_id', [RequestPriroda::STATE_OK, RequestPriroda::STATE_ADMIN]);
            }
        }
        /**
         * Filter region
         */
        if (array_get($filter, 'region')) {
            $prirodaRequest = $prirodaRequest->where('region_id', '=', $filter['region']);
            $filtered = true;
        }
        /**
         * Filter search
         */
        if (isset($filter[\App\Request::FILTER_ID]) && $filter[\App\Request::FILTER_ID] !== '') {
            $prirodaRequest = $prirodaRequest->where('number', '=', $filter[\App\Request::FILTER_ID]);
            $filtered = true;
        }
        if (isset($filter[\App\Request::FILTER_NUMBER]) && $filter[\App\Request::FILTER_NUMBER] !== '') {
            $prirodaRequest = $prirodaRequest->where('priroda_id', 'LIKE', '%' . $filter[\App\Request::FILTER_NUMBER] . '%');
            $filtered = true;
        }
//        if (isset($filter[\App\Request::FILTER_ZAYAV]) && $filter[\App\Request::FILTER_ZAYAV] !== '') {
//            $prirodaRequest = $prirodaRequest->where('name', 'LIKE', '%' . $filter[\App\Request::FILTER_ZAYAV] . '%');
//            $filtered = true;
//        }

        $department_title = '';
        if ($user->department_id) {
            $department = Department::find($user->department_id);
            $region = Region::whereName($department->region_name)->first();
            $prirodaRequest = $prirodaRequest->where('region_id', $region->region_id);
            $department_title = $department->region_name;
        }

        $prirodaRequest = $prirodaRequest
            ->orderBy('state_id', 'asc')
            ->orderBy('id', 'desc')
            ->paginate(10);

        $regions = \App\Region::orderBy('name', 'ASC')->pluck('name', 'region_id');

        return view('admin.requests_priroda.index', [
            'prirodaRequest' => $prirodaRequest,
            'department_title' => $department_title,
            'admin'    => $user->isAdmin(),
            'regions'  => $regions,
            'filter'   => $filter,
            'filtered' => $filtered
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function update(Request $request, $id)
    {
        /** @var User $user */
        $user = Auth::user();
        /** @var RequestPriroda $requestObject */
        $requestObject = RequestPriroda::where('id', $id)->first();

        $data = $request->all();

        $validator_rules = [
            'subject'       => 'required|min:3|max:255',
            'address'       => 'required|min:3|max:255',
            'description'   => 'required',
            'map_point'     => 'mappoint',
            'name'          => 'min:3|max:255',
            'comment_admin' => 'min:3|max:3000',
        ];

        $validator = Validator::make($data, $validator_rules, [
            'map_point.mappoint' => 'Формат геопозиции задан неверно'
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.request_priroda.edit', $requestObject)
                ->withErrors($validator)
                ->withInput();
        } else {
            /**
             * Map point
             */
            if (isset($data['map_point'])) {
                $data['map_point'] = explode(',', str_replace(' ', '', $data['map_point']));
            }

            /**
             * Delete admin photos
             */
//            $user_photos = $requestObject->photo_admin;
//
//            if (isset($data['deleted_photos'])) {
//                foreach ($user_photos as $k => $photo) {
//                    if (in_array($photo['file'], $data['deleted_photos'])) {
//                        unset($user_photos[$k]);
//                    }
//                }
//            }
//
//            $data['photo_admin'] = $user_photos;

            /**
             * Delete user photos
             */
            $user_photos = $requestObject->photo;

            if (isset($data['deleted_photos'])) {
                foreach ($user_photos as $k => $photo) {
                    if (in_array($photo['file'], $data['deleted_photos'])) {
                        unset($user_photos[$k]);
                    }
                }
                unset($data['deleted_photos']);
            }

            $data['photo'] = $user_photos;

            $requestObject->update(array_only($data, [
                'subject',
                'description',
                'address',
                'map_point',
                'name',
                'comment',
                'photo',
                'photo_admin'
            ]));

            if ($request->has('accept')) {
                if ($request->get('accept')) {
                    $requestObject->state_id = RequestPriroda::STATE_ADMIN;
                    //$requestObject->date_refusal = Carbon::now();
                    $requestObject->save();
                } else {
                    return $this->action($id, false);
                }
            } elseif ($request->has('accept_admin')) {
                if (!$request->get('accept_admin')) {
                    $requestObject->state_id = RequestPriroda::STATE_PROGRESS;
                    $requestObject->comment_admin = trim($request->get('comment_admin'))?:'Отказ администратора, без указания причины';
                    $requestObject->date_refusal = Carbon::now()->addMinute(1);
                    $requestObject->save();
                } else {
                    return $this->action($id, true);
                }
            }

            return redirect()
                ->route('admin.request_priroda.edit', $requestObject)
                ->with([
                    'message'     => 'Успешное сохранение',
                    'alert-class' => 'alert-success'
                ]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::user()->isAdmin())
            return back()->with(['message' => "Недостаточно прав"]);

        $requestObject = RequestPriroda::where('id', $id)->first();
        $requestObject->delete();

        return redirect()->route('admin.request_priroda.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $user = Auth::user();
        $requestObject = RequestPriroda::find($id);
        //$requestObject->photo = @unserialize($requestObject->photo);
        //$requestObject->map_point = @unserialize($requestObject->map_point);

        $department_title = '';
        if ($user->department_id) {
            $department_title = Department::find($user->department_id)->region_name;
        }

        return view('admin.requests_priroda.edit', [
            'priroda' => $requestObject,
            'department_title' => $department_title,
            'admin' => $user->isAdmin()
        ]);
    }

    /**
     * @param $id
     * @param $accept
     * @return \Illuminate\Http\RedirectResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function action($id, $accept)
    {
        try {
            /** @var RequestPriroda $priroda */
            $priroda = RequestPriroda::find($id);
            if (!$priroda)
                throw new \Exception("Не найдено обращение id:$id");

            DB::beginTransaction();

            if ($accept) {

                if ($priroda->request) {
                    $priroda->request->setStatusSolved($priroda);
                }

                $values = [
                    'state_id' => RequestPriroda::STATE_OK,
                    'date_state_repeat' => null,
                ];

                $message = 'Обращение подтверждено!';
            } else {
                $values = [
                    'state_id' => RequestPriroda::STATE_REPEAT,
                    'date_state_repeat' => Carbon::now(),
                    'date_refusal' => Carbon::now()->addMinute(1)
                ];

                $message = 'Обращение переведено в статус "Отправить повторно"';
            }

            $api = new ApiPriroda();
            $api->signin();
            $ret = $api->action([
                'id'      => array_get($priroda->answer, 'id'),
                'action'  => $accept ? 'Подтвердить' : 'Отправить повторно',
                'comment' => [
                    'content'     => $priroda->comment,
                    'resolution'  => false,
                    'roleCode'    => 'Applicant',
                    'attachments' => $priroda->getPhotoId()
                ]
            ]);
            $api->signout();

            if (!array_get($ret, 'successful'))
                throw new \Exception('Сообщение не принято "Нашей природой"');

            $priroda->update($values);

            DB::commit();

            return back()->with([
                'message'     => $message,
                'alert-class' => 'alert-success'
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return back()->with([
                'message'     => "Ошибка! {$e->getMessage()}",
                'alert-class' => 'alert-danger'
            ]);
        }
    }
}
