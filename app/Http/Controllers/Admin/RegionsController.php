<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Region;
use Illuminate\Support\Facades\Auth;

class RegionsController extends Controller
{
    public function index(Request $request)
	{
		if (!Auth::guest() && Auth::user()->department_id) {
			return redirect('/admin');
		}

		if ('' != $request->input('search')) {
			$regions = Region::where('name', 'LIKE', '%' . $request->input('search') . '%')->orderBy('name', 'asc')->get();
		} else {
			$regions = Region::orderBy('name', 'asc')->get();
		}

		return view('admin.regions.index', [
			'regions' => $regions,
			'search' => '' != $request->input('search') ? $request->input('search') : false,
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int                      $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		Region::find($id)->update($request->all());

		return response()->json(['success' => true]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}
}
