<?php

namespace App\Http\Controllers\Admin;

use App\EventNotification;
use App\RequestChange;
use App\Services\ApiPriroda;
use App\User;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Session;
use Validator;
use App\Department;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image as Image;

class RequestsController extends Controller
{

    public function index(Request $request)
    {
        $filtered = false;
        /** @var Builder $requests */
        $requests = \App\Request::where('created_at', '>', 0);
        /**
         * Filter status
         */
        $filter = $request->all();
        if (isset($filter['status']) && $filter['status'] !== '') {
            $requests = $requests->where('status', '=', $request->input('status'));
            $filtered = true;
        }
        /**
         * Filter region
         */
        if (isset($filter['region']) && $filter['region'] !== '') {
            $requests = $requests->where('region_id', '=', $request->input('region'));
            $filtered = true;
        }
        /**
         * Filter search
         */
        if (isset($filter[\App\Request::FILTER_ID]) && $filter[\App\Request::FILTER_ID] !== '') {
            $requests = $requests->where('id', 'LIKE', '%' . $filter[\App\Request::FILTER_ID] . '%');
            $filtered = true;
        }
        if (isset($filter[\App\Request::FILTER_ZAYAV]) && $filter[\App\Request::FILTER_ZAYAV] !== '') {
            $requests = $requests->where('name', 'LIKE', '%' . $filter[\App\Request::FILTER_ZAYAV] . '%');
            $filtered = true;
        }
        if (isset($filter[\App\Request::FILTER_EMAIL]) && $filter[\App\Request::FILTER_EMAIL] !== '') {
            $requests = $requests->where('email', 'LIKE', '%' . $filter[\App\Request::FILTER_EMAIL] . '%');
            $filtered = true;
        }
        if (isset($filter[\App\Request::FILTER_PHONE]) && $filter[\App\Request::FILTER_PHONE] !== '') {
            $requests = $requests->where('phone', 'LIKE', '%' . $filter[\App\Request::FILTER_PHONE] . '%');
            $filtered = true;
        }


        $department_title = '';
        if (Auth::user()->department_id) {
            $department = Department::find((int)Auth::user()->department_id);
            $requests = $requests->where('region_name', $department->region_name);
            $department_title = $department->region_name;
        }

        $requests = $requests->orderBy('created_at', 'desc')->paginate(10);

        $var = $requests;

        foreach ($var as $key => $value) {
            $photos = @unserialize($value['photo']);
            $var[$key]['photo'] = (!empty($photos)) ? array_values($photos)[0] : '';
        }

        $regions = \App\Region::orderBy('name', 'ASC')->pluck('name', 'region_id');

        return view('admin.requests.index', [
            'requests' => $var,
            'department_title' => $department_title,
            'admin' => (Auth::user()->department_id) ? false : true,
            'regions' => $regions,
            'filter' => $filter,
            'filtered' => $filtered
        ]);
    }

    public function show($id)
    {
        $request = \App\Request::find($id);

        return view('admin.requests.show', [
            'request' => $request,
            'admin' => (Auth::user()->department_id) ? false : true
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $requestObject = \App\Request::find($id);
        $requestObject->photo = @unserialize($requestObject->photo);
        $requestObject->photo_admin = (isset($requestObject->photo_admin)) ? @unserialize($requestObject->photo_admin) : '';

        $department_title = '';
        $requestObject->map_point = @unserialize($requestObject->map_point);

        if (Auth::user()->department_id) {
            $department_title = Department::find((int)Auth::user()->department_id)->region_name;
        }

        return view('admin.requests.edit', [
            'request' => $requestObject,
            'department_title' => $department_title,
            'admin' => (Auth::user()->department_id) ? false : true
        ]);
    }

    /**
     *
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function rotateFiles(Request $request, $id)
    {
        $src = $request->get('src');


        $pathWeb = $request->get('path');
        if (strpos($pathWeb, '?')) {
            $pathWeb = substr($pathWeb, 0, strpos($pathWeb, '?'));
        }

        if (($tmp = strstr($pathWeb, '/images')) !== false) {
            $str = substr($tmp, 0);
            $path = public_path() . $str;
            Image::make($src)->save($path);
        }

        return response()->json([
            'path' => $pathWeb,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /** @var User $user */
        $user = \Auth::user();
        /** @var \App\Request $requestObject */
        $requestObject = \App\Request::where('id', $id)->first();

        /**
         * Delete photo
         */
        if ($request->input('delete_photo')) {
            if ($requestObject->photo_admin) {
                $data['photo_admin'] = unserialize($requestObject->photo_admin);

                foreach ($data['photo_admin'] as $k => $v) {
                    if (in_array($v, $request->input('delete_photo'))) {
                        unset($data['photo_admin'][$k]);
                    }
                }

                $data['photo_admin'] = (count($data['photo_admin'])) ? serialize($data['photo_admin']) : '';
                $requestObject->photo_admin = $data['photo_admin'];
                $requestObject->save();
            }

            return redirect(route('admin.requests.edit', ['request' => $requestObject->id]));
        }
        /**
         * If file upload
         */

        if ($request->file('files')) {
            $path = '/images/requests/';

            $files = ($requestObject->photo_admin) ? unserialize($requestObject->photo_admin) : [];

            foreach ($request->file('files') as $file) {
                $imageName = $requestObject->id . '-' . str_random() . '.' . $file->getClientOriginalExtension();
                $file->move(base_path() . '/public' . $path, $imageName);
                $files[] = $path . $imageName;
                $new_files[] = $path . $imageName;
            }

            $requestObject->photo_admin = serialize($files);
            $requestObject->save();

            return response()->json([
                'status' => 'ok',
                'photo' => $new_files
            ]);
        }

        /**
         * Submit form
         */

        $new_status = (int)$request->get('new_status');
        if ($new_status > 1) {
            $requestChange =
                RequestChange::where([
                    'user_id' => $user->id,
                    'request_status' => RequestChange::STATUS_OPEN,
                    'old_request_status' => $requestObject->status,
                    'new_request_status' => $new_status,
                    'request_id' => $requestObject->id

                ])->first();
            if ($requestChange === null) {
                $requestChange = new RequestChange();
                $requestChange->user_id = $user->id;
                $requestChange->request_status = RequestChange::STATUS_OPEN;
                $requestChange->old_request_status = $requestObject->status;
                $requestChange->new_request_status = $new_status;
                $requestChange->request_id = $requestObject->id;
            }
            $requestChange->save();
        }
        $data = $request->all();

        if (Auth::user()->department_id) {
            $validator_rules = [];
        } else {
            $validator_rules = [
                'subject' => 'required|min:3|max:255',
                'address' => 'required|min:3|max:255',
                'description' => 'required',
                'map_point' => 'mappoint',
                'name' => 'required|min:3|max:255',
                'phone' => 'required|min:10',
                'email' => 'email|max:255',
            ];
        }

        $validator = Validator::make($data, $validator_rules, [
            'map_point.mappoint' => 'Формат геопозиции задан неверно'
        ]);

        if ($validator->fails()) {
            return redirect(route('admin.requests.edit', ['request' => $requestObject->id]))
                ->withErrors($validator)
                ->withInput();
        } else {
            unset($data['_token']);
            unset($data['_method']);

            /**
             * Map point
             */
            if (isset($data['map_point'])) {
                $data['map_point'] = serialize(explode(',', str_replace(' ', '', $data['map_point'])));
            }

            /**
             * Delete user photos
             */
            $user_photos = unserialize($requestObject->photo);

            if (isset($data['deleted_photos'])) {
                foreach ($user_photos as $k => $photo) {
                    if (in_array($photo, $data['deleted_photos'])) {
                        unset($user_photos[$k]);
                    }
                }

                unset($data['deleted_photos']);
            }

            $data['photo'] = serialize($user_photos);

            $requestObject->update($data);

            if ($requestObject->status && $requestObject->email) {
                EventNotification::send(EventNotification::EVENT_EDIT_REQUEST, $requestObject);
            }

            return redirect(route('admin.requests.edit', ['request' => $requestObject->id]));
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $requestObject = \App\Request::where('id', $id)->first();
        $requestObject->delete();

        return redirect(route('admin.requests.index'));
    }


    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function toWork($id)
    {
        try {
            /** @var \App\Request $requestModel */
            $requestModel = \App\Request::find($id);

            /** @var ApiPriroda $api */
            $api = app(ApiPriroda::class);

            if (! $api->signin())
                throw new \Exception('Невозможно получить токен или сервер "Наша природа" не досутпен');

            foreach($requestModel->attachments() as $file) {
                $attach = $api->attachSend([$file]);
                if ($attach) {
                    $attachmentIds[] = $attach['id'];
                }
            }

            if (empty($attachmentIds))
                throw new \Exception('Без фотографий отправить в систему "Наша природа" невозможно');

            $externalCode = $requestModel->uuid();

            $ret = $api->createAndSend([
                'title'                 => mb_substr($requestModel->subject, 0, 255),                  // заголовок
                'description'           => mb_substr($requestModel->description, 0, 255),              // текст сообщения
                'originalApplicationId' => null,                                    // В случае связанных заявок
                'categoryId'            => 'aca48aff-4008-f27c-84e6-6d10ba771dc2',  // id категории (Несанкционированное размещение отходов)
                'externalCode'          => $externalCode,                           // Ваш идентификатор сообщений из Карты свалок
                'sourceSystem'          => ['code' => 'SMAP'],                      // код платформы
                'attachmentIds'         => $attachmentIds,                          // вложения
                'place' => [                                                        // место
                    'address'          => $requestModel->address,                        // адрес
                    'latitude'         => round($requestModel->latitude(),8),   // широта
                    'longitude'        => round($requestModel->longitude(),8),  // долгота
                    'federalSubjectId' => $requestModel->region->federal_uuid,           // федеральный округ
                    'subjectId'        => $requestModel->region->uuid,                   // федеральный субъект
                    'municipalityName' => null                                           // муниципалитет
                ],
            ]);

            if (!$ret)
                throw new \Exception('При отправке обращения произошла ошибка, обратитесь к администратору');

            $requestModel->status       = \App\Request::STATUS_IN_WORK;
            $requestModel->priroda_id   = $externalCode;
            $requestModel->priroda_date = Carbon::now();
            $requestModel->save();

            $api->signout();

            return back()->with([
                'message'     => 'Обращение отправлено!',
                'alert-class' => 'alert-success'
            ]);
        } catch (\Exception $e) {
            Log::error($e);
            return back()->with([
                'message'     => 'Ошибка! '.$e->getMessage(),
                'alert-class' => 'alert-danger'
            ]);
        }
    }
}
