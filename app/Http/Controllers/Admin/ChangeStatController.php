<?php

namespace App\Http\Controllers\Admin;

use App\RequestChange;
use App\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChangeStatController extends Controller
{
    public function index(Request $request)
    {
        /** @var User $user */
        $user = \Auth::user();
        $requests_stat = RequestChange::
            where('request_change.user_id', '=', $user->id)
            ->where('request_change.request_status', '!=', RequestChange::STATUS_APPLY)
            ->get();
        return view('admin.requests_changes.index', [
            'requests' => $requests_stat,
            'department_title' => 'Резолюции по запросам по сменам статуса',
        ]);
    }

    public function show($id)
    {
        /** @var RequestChange $request */
        $request = RequestChange::find($id);

        return view('admin.requests_changes.show', [
            'request' => $request,
            'department_title' => 'Резолюции по запросу о смене статуса'.$request->request()->first()->subject,
        ]);
    }

}
