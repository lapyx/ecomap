<?php

namespace App\Http\Controllers\Admin;

use App\RequestChange;
use App\User;
use Validator;
use App\Department;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ChangeController extends Controller
{
    public function index(Request $request)
    {
        $filtered = false;
        $requests = \App\Request::
        join('request_change', 'requests.id', '=', 'request_change.request_id')
            ->where('requests.created_at', '>', 0)
            ->where('request_change.request_status', '=', 0);

        /**
         * Filter status
         */
        $filter = $request->all();
        /**
         * Filter region
         */
        if (isset($filter['region']) && $filter['region'] !== '') {
            $requests = $requests->where('requests.region_id', '=', $request->input('region'));
            $filtered = true;
        }
        /**
         * Filter search
         */
        if (isset($filter['search']) && $filter['search'] !== '') {
            $requests = $requests->where('requests.id', 'LIKE', '%' . $filter['search'] . '%');
            $filtered = true;
        }

        $department_title = '';
        if (Auth::user()->department_id) {
            $department = Department::find((int)Auth::user()->department_id);
            $requests = $requests->where('requests.region_name', $department->region_name);
            $department_title = $department->region_name;
        }

        $requests = $requests->orderBy('request_change.created_at', 'asc')->paginate(10);

        $var = $requests;

        foreach ($var as $key => $value) {
            $photos = @unserialize($value['photo']);
            $var[$key]['photo'] = (!empty($photos)) ? array_values($photos)[0] : '';
        }

        foreach (\App\Request::orderBy('region_name', 'ASC')->get() as $request) {
            $regions[$request->region_id] = $request->region_name;
        }
        return view('admin.changes.index', [
            'requests' => $var,
            'department_title' => $department_title,
            'admin' => (Auth::user()->department_id) ? false : true,
            'regions' => $regions,
            'filter' => $filter,
            'filtered' => $filtered
        ]);
    }

    public function show($id)
    {
        $request = \App\Request::find($id);

        return view('admin.changes.show', [
            'request' => $request,
            'admin' => (Auth::user()->department_id) ? false : true
        ]);
    }

    public function update()
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        /** @var RequestChange $requestObject */
        $requestObject = RequestChange::find($id);

        $requestObject->photo = @unserialize($requestObject->request()->first()->photo);
        $requestObject->photo_admin = (isset($requestObject->photo_admin)) ? @unserialize($requestObject->photo_admin) : '';

        $department_title = '';
        $requestObject->map_point = @unserialize($requestObject->request()->first()->map_point);


        if (Auth::user()->department_id) {
            $department_title = Department::find((int)Auth::user()->department_id)->region_name;
        }

        return view('admin.changes.edit', [
            'request' => $requestObject,
            'department_title' => $department_title,
            'admin' => (Auth::user()->department_id) ? false : true
        ]);
    }

    public function apply(Request $request, $id)
    {
        $resolution = $request->get('resolution');

        /** @var RequestChange $rejectChange */
        $rejectChange = RequestChange::find($id);
        $rejectChange->reason = $request->get('reason');
        if ($resolution === 'apply') {
            $rejectChange->apply();
        } else {
            $rejectChange->reject();
        }

        return redirect(route('admin.changes.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $requestObject = \App\Request::where('id', $id)->first();
        $requestObject->delete();

        return redirect(route('admin.requests.index'));
    }
}
