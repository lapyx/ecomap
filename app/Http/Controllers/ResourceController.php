<?php

namespace App\Http\Controllers;

use App\Department;
use App\EventNotification;
use App\Personal;
use App\User;
use App\Region;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

class ResourceController extends Controller
{

    public function port_query($id, Request $request)
    {

        $requests = \App\Request::where('id', '=', $id)->get();




        foreach ($requests as $key => $request) {

            $requests[$key]['id'] = $requests[$key]['id'];
            $requests[$key]['map_point'] = unserialize($requests[$key]['map_point']);
            $requests[$key]['photo'] = @unserialize($requests[$key]['photo']);
            $requests[$key]['photo_admin'] = @unserialize($requests[$key]['photo_admin']);
            $requests[$key]['photo_preview_admin'] = $request->photo_admin_preview;
            $requests[$key]['photo_preview'] = $request->photo_preview;


            $marker = 'gr';
            switch ($request->status) {
                case \App\Request::STATUS_RASSMOTRENIE:
                    $marker = 'rd';
                    break;
                case \App\Request::STATUS_MODERATE:
                    $marker = 'gr';
                    break;
                case \App\Request::STATUS_IN_WORK:
                    $marker = 'or';
                    break;
                case \App\Request::STATUS_SOLVE:
                    $marker = 'gn';
                    break;
            };

            $point = $request->map_point;
            $coord = (isset($point) && isset($point[0]) && isset($point[1])) ? $point[1] . ',' . $point[0] : false;
            $map_image = 'https://static-maps.yandex.ru/1.x/?ll=' . $coord . '&size=450,450&z=11&l=map&pt=' . $coord . ',pm2' . $marker . 'l';

            $path = '/images/maps/' . $id . '.jpg';
            file_put_contents(public_path() . $path, file_get_contents($map_image));
            $requests[$key]['map_image'] = $path;
        }

        return response()->json([
            'test' => $requests
        ]);
    }

    public function ajaxOne2(Request $request)
    {

        if ($request->cookie('requests')) {
            $cookie_requests = unserialize($request->cookie('requests'));
            $requests = new Collection(\App\Request::whereIn('id', $cookie_requests)->get());
            $requests = $requests->merge(new Collection(\App\Request::where('status', '>=', 1)->get()));
        } else {
            $requests = \App\Request::where('status', '>=', 1)->get();
        }

        $i = 0;

        foreach ($requests as $key => $request) {
            if ($i > 13000) {
                unset($requests[$key]);
            } else {
                $i++;
                $requests[$key]['id'] = $requests[$key]['id'];
                $requests[$key]['map_point'] = unserialize($requests[$key]['map_point']);
                $requests[$key]['photo'] = @unserialize($requests[$key]['photo']);
                $requests[$key]['photo_admin'] = @unserialize($requests[$key]['photo_admin']);

                unset($requests[$key]['address']);
                unset($requests[$key]['comment']);
                unset($requests[$key]['created_at']);
                unset($requests[$key]['description']);
                unset($requests[$key]['email']);
                unset($requests[$key]['name']);
                unset($requests[$key]['participate']);
                unset($requests[$key]['phone']);
                unset($requests[$key]['photo']);
                unset($requests[$key]['photo_admin']);
                unset($requests[$key]['region_name']);
                unset($requests[$key]['subject']);
                unset($requests[$key]['updated_at']);
                unset($requests[$key]['user_id']);
                unset($requests[$key]['youtube']);
                unset($requests[$key]['region_id']);

            }
        }

        //$requests=array_slice($requests,0,100);

        return response()->json([
            'requests' => $requests,
            'test' => 'true'
        ]);
    }

    public function ajaxTwo2(Request $request)
    {

        if ($request->cookie('requests')) {
            $cookie_requests = unserialize($request->cookie('requests'));
            $requests = new Collection(\App\Request::whereIn('id', $cookie_requests)->get());
            $requests = $requests->merge(new Collection(\App\Request::where('status', '>=', 1)->get()));
        } else {
            $requests = \App\Request::where('status', '>=', 1)->get();
        }

        $i = 0;

        foreach ($requests as $key => $request) {
            if ($i < 13000) {
                unset($requests[$key]);
                $i++;
            } else {
                if ($i > 26000) {
                    unset($requests[$key]);
                } else {
                    $i++;
                    $requests[$key]['id'] = $requests[$key]['id'];
                    $requests[$key]['map_point'] = unserialize($requests[$key]['map_point']);
                    $requests[$key]['photo'] = @unserialize($requests[$key]['photo']);
                    $requests[$key]['photo_admin'] = @unserialize($requests[$key]['photo_admin']);

                    unset($requests[$key]['address']);
                    unset($requests[$key]['comment']);
                    unset($requests[$key]['created_at']);
                    unset($requests[$key]['description']);
                    unset($requests[$key]['email']);
                    unset($requests[$key]['name']);
                    unset($requests[$key]['participate']);
                    unset($requests[$key]['phone']);
                    //unset($requests[$key]['photo']);
                    unset($requests[$key]['photo_admin']);
                    unset($requests[$key]['region_name']);
                    unset($requests[$key]['subject']);
                    unset($requests[$key]['updated_at']);
                    unset($requests[$key]['user_id']);
                    unset($requests[$key]['youtube']);
                    unset($requests[$key]['region_id']);

                }
            }
        }


        return response()->json([
            'requests' => $requests,
            'test' => 'true'
        ]);
    }

    public function regions(Request $request)
    {
        $regions = Region::all();

        foreach ($regions as $reg) {
            $colors[$reg->region_id] = ['id' => $reg->region_id, 'color' => $reg->color];
        }

        return response()->json([
            'colors' => $colors
        ]);
    }

    public function point_all(Request $request)
    {

        if ($request->cookie('requests')) {
            $cookie_requests = unserialize($request->cookie('requests'));
            $requests = new Collection(\App\Request::whereIn('id', $cookie_requests)->get());
            $requests = $requests->merge(new Collection(\App\Request::where('status', '>=', 1)->get()));
        } else {
            $requests = \App\Request::where('status', '>=', 0)->get();
        }

        $i = 0;

        foreach ($requests as $key => $request) {

        }

        return response()->json([
            'requests' => count($requests)
        ]);
    }

    public function point_orange(Request $request)
    {
        if ($request->cookie('requests')) {
            $cookie_requests = unserialize($request->cookie('requests'));
            $requests = new Collection(\App\Request::whereIn('id', $cookie_requests)->get());
            $requests = $requests->merge(new Collection(\App\Request::where('status', '>=', 1)->get()));
        } else {
            $requests = \App\Request::where('status', '>=', 1)->get();
        }

        $i = 0;

        foreach ($requests as $key => $request) {
            if ($requests[$key]['status'] == '2') {
                $i++;
            }
        }

        return response()->json([
            'requests' => $i
        ]);
    }

    public function point_green(Request $request)
    {
        if ($request->cookie('requests')) {
            $cookie_requests = unserialize($request->cookie('requests'));
            $requests = new Collection(\App\Request::whereIn('id', $cookie_requests)->get());
            $requests = $requests->merge(new Collection(\App\Request::where('status', '>=', 1)->get()));
        } else {
            $requests = \App\Request::where('status', '>=', 1)->get();
        }

        $i = 0;

        foreach ($requests as $key => $request) {
            if ($requests[$key]['status'] == '3') {
                $i++;
            }
        }

        return response()->json([
            'requests' => $i
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        App::setLocale('ru');

        try {
            $id = \App\Request::generateId();

            $this->validate($request, [
                'subject' => 'required|min:3|max:255',
                'address' => 'required|min:3|max:255',
                'description' => 'required',
                'photo' => 'required',
                'map_point' => '',
                'name' => 'required|min:3|max:255',
                'phone' => 'required|min:10',
                'email' => 'email|max:255',
                'agree' => 'required'
            ]);

            $attributes = $request->all();
            $attributes['map_point'] = serialize(explode(',', $attributes['map_point']));
            $attributes['status'] = 0;
            $attributes['id'] = $id;
            $region = $request->input('region_name');

            if ($region == '') {
                $region = explode(',', $request->input('address'));
                $region = $region[0];
                $attributes['region_name'] = $region;
            }

            $attributes['region_id'] = Region::where('name', '=', $region)->first()->region_id;

            /**
             * Participate
             */
            if ($attributes['participate'] == '') {
                unset($attributes['participate']);
            }

            /**
             * Youtube
             */
            if ($attributes['youtube'] !== '') {
                $yt_link = explode('/', $attributes['youtube']);
                $attributes['youtube'] = str_replace('watch?v=', '', $yt_link[sizeof($yt_link) - 1]);
            }

            unset($attributes['photo']);
            $requestObject = \App\Request::create($attributes);

            if ($request->file('photo')) {
                $path = '/images/requests/';

                $files = [];

                foreach ($request->file('photo') as $file) {
                    $imageName = $requestObject->id . '-' . str_random() . '.' . $file->getClientOriginalExtension();
                    $file->move(base_path() . '/public' . $path, $imageName);
                    $files[] = $path . $imageName;
                }

                $requestObject->photo = serialize($files);
                $requestObject->save();
            }

            $requestObject->id = $id;

            /**
             * Set cookie
             */
            if ($request->cookie('requests')) {
                $cookie_requests = unserialize($request->cookie('requests'));
            }
            $cookie_requests[] = $requestObject->id;
            $cookie = cookie('requests', serialize($cookie_requests), time() + 31556926);

            EventNotification::send(EventNotification::EVENT_NEW_REQUEST, $requestObject);

            return response()->json([
                'status' => 'ok',
                'request_id' => $requestObject->id
            ])->cookie($cookie);
        } catch (ValidationException $e) {
            return response()->json([
                'status' => 'error',
                'errors' => $e->validator->getMessageBag()->getMessages(),
            ], 422);
        }
    }

    public function getDepartmentInfo(Request $request)
    {
        $department = Department::where('region_name', '=', $request->region_name)->first();

        $department->description = nl2br($department->description);
        $department->contacts = nl2br($department->contacts);

        $department->personal = Personal::where('department_id', $department->id)->get();

        return response()->json([
            'status' => 'ok',
            'department' => $department,
            'requested' => $request->region_name,
        ]);
    }

    public function getDepartmentInfoGet($region, Request $request)
    {
        $department = Department::where('region_name', '=', $request->region)->first();

        $department->description = nl2br($department->description);
        $department->contacts = nl2br($department->contacts);

        $department->personal = Personal::where('department_id', $department->id)->get();

        return response()->json([
            'status' => 'ok',
            'department' => $department,
            'requested' => $request->region_name,
        ]);
    }
}
