<?php

namespace App\Http\Controllers;

use App\News;
use App\Pages;
use DiDom\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class AppController extends Controller
{
    public function index(Request $request, $id = false)
    {
        if (isset($_GET['ttt'])) {
            echo \App\Request::where('status', '>=', 1)->count();
            die();
        }
        if ($id) {
            $request = \App\Request::find($id);
            $point = unserialize($request->map_point);
            $coord = (isset($point) && isset($point[0]) && isset($point[1])) ? $point[1] . ',' . $point[0] : false;
            $marker = 'gr';
            switch ($request->status){
                case \App\Request::STATUS_RASSMOTRENIE: $marker='rd'; break;
                case \App\Request::STATUS_MODERATE: $marker='gr'; break;
                case \App\Request::STATUS_IN_WORK:$marker='or'; break;
                case \App\Request::STATUS_SOLVE:$marker='gn'; break;
            };

            $map_image = 'https://static-maps.yandex.ru/1.x/?ll=' . $coord . '&size=450,450&z=11&l=map&pt=' . $coord.',pm2'.$marker.'l';

            $path = '/images/maps/' . $id . '.jpg';

        } else {
            $map_image = 'https://static-maps.yandex.ru/1.x/?ll=94.15,66.25&size=450,450&z=2&l=map';
            $path = '/images/maps/all.jpg';
        }
        file_put_contents(public_path() . $path, file_get_contents($map_image));

        return view('home', [
            'user' => Auth::user() ?: false,
            'request_id' => (isset($request)) ? $request->id : false,
            'map_point' => (isset($point) && isset($point[0]) && isset($point[1])) ? $point[0] . ',' . $point[1] : false,
            'map_image' => $path,
            'region' => (isset($request)) ? $request->region_name : false
        ]);
    }

    public function page(Request $request)
    {
        $page = Pages::find($request->id);
        $page->text = nl2br($page->text);

        return response()->json([
            'status' => 'ok',
            'page' => $page,
        ]);
    }

    public function news(Request $request)
    {
        $news = News::where('status', 1)->orderBy('created_at', 'desc')->paginate(15);

        foreach ($news as $key => $value) {
            $value['text'] = nl2br($value['text']);
            $news[$key] = $value;
        }

        return response()->json([
            'status' => 'ok',
            'news' => $news
        ]);
    }

    public function help(Request $request)
    {
//		$helps = \App\Help::where('status', 1)->orderBy('created_at', 'desc')->paginate(15);
//
//		foreach ($helps as $key => $value) {
//			$value['text'] = nl2br($value['text']);
//			$helps[$key] = $value;
//		}

        $help = \App\Help::find(1);

        return response()->json([
            'status' => 'ok',
            'item' => $help
            //'helps'      => $helps
        ]);
    }

    public function test()
    {
        $path = 'https://onf.ru/generalnaya_uborka';

        if ('' != $this->argument('page')) {
            $path .= '?' . $this->argument('page');
        }

        $document = new Document($path, $isFile = true);
        $entryBlock = '.view-content .node';

        $items = $document->find($entryBlock);
        $items = array_reverse($items);

        foreach ($items as $item) {
            $entryMeta = $item->find('h2 a')[0];
            $entryName = $entryMeta->text();
            $entryLink = $entryMeta->attr('href');

            $news = News::where('name', $entryName)->first();
            if (isset($news) && $news->id) {
                continue;
            }

            $entryBody = new Document('https://onf.ru' . $entryLink, $isFile = true);
            $text = $entryBody->find('.field-name-body .field-item')[0]->text();

            $photos = $entryBody->find('.field-name-field-photos .field-item a');

            foreach ($photos as $photo) {
                $url = $photo->attr('href');
                $text .= '<p class="news__image"><img src="' . $url . '"></p>';
            }

            $news = new News();

            $news->name = $entryName;
            $news->text = $text;
            $news->status = 1;

            $news->save();
        }

    }
}
