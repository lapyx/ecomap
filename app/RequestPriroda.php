<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class RequestPriroda extends Model
{
    // статусы из "Нашей природы"
    const DRAFT        = 'DRAFT'; // Черновик
    const IS_NEW       = 'NEW'; // Отправлено
    const IN_PROGRESS  = 'IN_PROGRESS'; // В работе
    const CANCELLED    = 'CANCELLED'; // Отменено
    const REJECTED     = 'REJECTED'; // Отказано в публикации
    const FINISHED     = 'FINISHED'; // Завершено
    const CLOSED       = 'CLOSED'; // Закрыто
    const PUBLISHED    = 'PUBLISHED'; // Опубликовано

    // статусы для адина, чтобы знать, что нужно проверить,
    // а что уже отправлено повторно
    const STATE_PROGRESS = 1; // на рассмотрении
    const STATE_REPEAT   = 2; // отправлено повторно
    const STATE_OK       = 3; // Проблема решена
    const STATE_ADMIN    = 4; // на проверку админу

    public $table = 'request_priroda';
    public $guarded = [];

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'date_refusal'
    ];

    protected $casts = [
        'answer'      => 'array',
        'photo'       => 'array',
        'map_point'   => 'array',
        'photo_admin' => 'array'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region()
    {
        return $this->belongsTo('App\Region', 'region_id', 'region_id');
    }

    public function request()
    {
        return $this->belongsTo(Request::class, 'priroda_id', 'priroda_id');
    }

    /**
     * @param null $code
     * @return array|mixed
     */
    public static function getStatus($code = null)
    {
        $status = [
            self::DRAFT        => 'Черновик',
            self::IS_NEW       => 'Отправлено',
            self::IN_PROGRESS  => 'В работе',
            self::CANCELLED    => 'Отменено',
            self::REJECTED     => 'Отказано в публикации',
            self::FINISHED     => 'Завершено',
            self::CLOSED       => 'Закрыто',
            self::PUBLISHED    => 'Опубликовано',
        ];

        if ($code) {
            return array_get($status, $code, $code);
        }
        return $status;
    }

    /**
     * @param null $code
     * @return array|mixed
     */
    public static function getState($code = null)
    {
        $status = [
            self::STATE_PROGRESS => 'На рассмотрении',
            self::STATE_REPEAT   => 'Отправлено повторно',
            self::STATE_ADMIN    => 'Проверка администратором',
            self::STATE_OK       => 'Проблема решена',
        ];

        if ($code) {
            return array_get($status, $code, $code);
        }
        return $status;
    }

    /**
     * @return array|mixed
     */
    public function statusName()
    {
        return self::getStatus($this->status);
    }

    /**
     * @return array|mixed
     */
    public function stateName()
    {
        return self::getState($this->state_id);
    }

    public function getPhotoFile()
    {
        $ret = [];
        if ($this->photo) {
            foreach ($this->photo as $one) {
                $ret[] = $one['file'];
            }
        }
        return $ret;
    }

    /**
     * @param int $limit
     * @return array
     */
    public function getPhotoId($limit=5)
    {
        $ret = [];
        if ($this->photo) {
            foreach ($this->photo as $i => $one) {
                if ($i >= $limit) break;
                $ret[]['id'] = $one['id'];
            }
        }
        return $ret;
    }

    /**
     * @param bool $check_new_comment
     * @return array
     */
    public function getCommentsPriroda($check_new_comment = false)
    {
        $ret = [];
        foreach (array_get($this->answer, 'comments', []) as $one) {
            // берем только сообщения, которые были после отказа
            if ($check_new_comment && $this->date_refusal && ($this->date_refusal->timestamp*1000) > $one['created'])
                continue;

            @$created = Carbon::createFromTimestampUTC(ceil($one['created']/1000));
            @$ret[] = $created->format('d.m.Y').' '.str_replace("\n", ' ', $one['content']);
        }
        return $ret;
    }
}
