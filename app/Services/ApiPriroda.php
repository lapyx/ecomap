<?php
namespace App\Services;

use GuzzleHttp\Client;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;

class ApiPriroda {
    /** @var Client */
    protected $client;
    protected $base_uri;
    protected $token;
    public $statusCode;
    protected $errors = [];
    public $response;

    /**
     * ApiPriroda constructor.
     * @throws \Exception
     */
    public function __construct() {
        $this->base_uri = env('PRIRODA_API_URL');

        if (!$this->base_uri)
            throw new \Exception('Not found env param PRIRODA_API_URL');

        $this->client = new Client(['base_uri' => $this->base_uri, 'http_errors' => false]);
    }

    protected function requestClear()
    {
        $this->errors     = [];
        $this->response   = null;
        $this->token      = null;
        $this->statusCode = null;
    }

    /**
     * @param $method
     * @param $url
     * @param array $params
     * @param array $multipart
     * @return mixed|\Psr\Http\Message\StreamInterface|array
     * @throws \GuzzleHttp\Exception\GuzzleException|Exception
     */
    protected function request($method, $url, array $params = [], array $multipart = [])
    {
        $settings = [
            'headers' => [
                'Content-Type'  => 'application/json',
                'Accept'        => 'application/json',
                'Platform-code' => 'SMAP',
                'Auth-Token'    => $this->token
            ],
        ];

        if ($multipart) {
            $settings['multipart'] = $multipart;
            unset($settings['headers']['Content-Type']);
            //$url = 'http://localhost:8080/'.$url;
        } elseif($method == Request::METHOD_GET) {
            $settings['query'] = $params;
        } else {
            $settings['json'] = $params;
        }

        $this->response = $this->client->request($method, $url, $settings);

        if ($body = $this->response->getBody()) {
            $body = json_decode($body, 1);
        }

        $this->statusCode = array_get($body, 'statusCode');

        if (! in_array($this->response->getStatusCode(), [200, 201, 204])) {
            Log::error('ApiPriroda ('.$this->base_uri.$url.'): method: '.$method.', settings: '.print_r($settings, 1));
            throw new \Exception('Server error status code:' . $this->response->getStatusCode());
        }

        if ($this->statusCode > 0) {
            Log::error('ApiPriroda ('.$this->base_uri.$url.'): method: '.$method.', response: '.print_r($body,1).', settings: '.print_r($settings, 1));

            $message = array_get(trans('priroda.message_error'), $this->statusCode);
            if (! $message) {
                $message = 'method:'.$url.', message:'.array_get($body, 'statusMessage').', code:'.$this->statusCode;
            }
            throw new \Exception($message);
        }

        return array_get($body, 'response', []);
    }

    /**
     * @param bool $longLife
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function signin($longLife = false)
    {
        $res = $this->request('POST', 'security/signin', [
            'login'    => env('PRIRODA_API_LOGIN', 'dumpmap'),	// логин
            'password' => env('PRIRODA_API_PASSWORD', 'smapsoesg'),	// пароль
            'longLife' => $longLife     // длинную ли сессию создавать
        ]);

        if ($res) {
            $this->token = array_get($res, 'sessionId');
            return true;
        }
        return false;
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function signout()
    {
        $this->request('GET', 'security/signout');
        $this->requestClear();
    }

    /**
     * @param array $params {
            limit: INTEGER,             // кол-во записей (max: 1000)
            offset: INTEGER,            // с какой записи искать
            mine: BOOLEAN,              // только собственные (обязательна авторизация)
            applicationId: UUID,        // id обращения
            applicationNumber: STRING,  // уникальный номер обращения (или часть номера)
            federalSubjectId: UUID,     // ф. округ
            subjectId: UUID,            // субъект РФ
            municipalityId: UUID,       // муниципалитет
            categoryId: UUID,           // id категории
            createdFrom: LONG,          // начало временного интервала создания обращения
            createdTill: LONG,          // конец временного интервала создания обращения
            updateFrom:  LONG,          // начало временного интервала  обновления  обращения
            updateTill:  LONG,          // конец временного интервала обновления обращения
            status: STRING,             // статус сообщения
            orderby: STRING,            // критерий сортировки
            groupId: STRING,            // inbox - поиск обращений со статусами 20 и 100 и др.
            likes: INTEGER,             // количество лайков у сообщения
            square: {                   // Площадь
                topLat: DOUBLE,
                leftLong: DOUBLE,
                bottomLat: DOUBLE,
                rightLong: DOUBLE
            }
     }
     * @return mixed|\Psr\Http\Message\StreamInterface|array
     * @throws \GuzzleHttp\Exception\GuzzleException
     *
     */
    public function find(array $params = [])
    {
        return $this->request('POST', 'api/application/find', $params);
    }

    /**
     * @param array $params
     * @return array|mixed|\Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function action(array $params)
    {
        return $this->request('POST', 'api/application/action', $params);
    }

    /**
     * @param array $params
     * @return mixed|\Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createAndSend(array $params)
    {
        return $this->request('POST', 'api/application/createAndSend', $params);
    }

    /**
     * @param array $files
     * @return mixed|\Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function attachSend(array $files)
    {
        return $this->request('POST', 'api/attachment', [], $files);
    }

    /**
     * @param $id
     * @return array|mixed|\Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAttach($id)
    {
        return $this->request('GET', 'api/attachment/'.$id);
    }

    /**
     * @return mixed|\Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getFederalDistricts()
    {
        return $this->request('GET', 'api/nsi/region/federalDistrict', [
            'include_subjects' => true
        ]);
    }

}