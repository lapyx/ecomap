<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestChange extends Model
{
    //protected $table = 'requests_test';
    const STATUS_OPEN = 0;
    const STATUS_APPLY = 1;
    const STATUS_REJECT = 2;
    public $incrementing = false;
    protected $table = 'request_change';
    public $fillable = [
        'id',
        'user_id',
        'request_status',
        'old_request_status',
        'new_request_status',
        'request_id',
        'created_at',
        'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\\User', 'user_id');
    }

    public function request()
    {
        return $this->belongsTo('App\Request', 'request_id');
    }

    public function apply()
    {
        $this->request_status = self::STATUS_APPLY;
        /** @var Request $request */
        $request = $this->request()->first();
        $request->status = $this->new_request_status;
        $request->save();
        $this->save();
    }

    public function reject()
    {
        $this->request_status = self::STATUS_REJECT;
        $this->save();
    }
}
