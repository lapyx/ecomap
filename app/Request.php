<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
//    protected $table = 'requests_test';

    public $incrementing = false;

    const STATUS_MODERATE = 0;
    const STATUS_RASSMOTRENIE = 1;
    const STATUS_IN_WORK = 2;
    const STATUS_SOLVE = 3;

    const FILTER_ID = 'search';
    const FILTER_ZAYAV = 'search_zayav';
    const FILTER_EMAIL = 'search_email';
    const FILTER_PHONE = 'search_phone';
    const FILTER_NUMBER = 'search_number';

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'priroda_date'
    ];

    public $fillable = [
        'id',
        'subject',
        'address',
        'description',
        'map_point',
        'address',
        'name',
        'phone',
        'email',
        'status',
        'comment',
        'region_name',
        'user_id',
        'region_id',
        'participate',
        'youtube',
        'photo',
        'photo_admin',
        'priroda_id',
        'priroda_date'
    ];

    /**
     * @return string
     * @throws \Exception
     */
    public static function generateId()
    {
        $out = date('ymd') . '-' . random_int(100, 999);

        if (Request::find($out)) {
            return self::generateId();
        }
        return $out;
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function region()
    {
        return $this->belongsTo('App\Region', 'region_id', 'region_id');
    }

    public function priroda()
    {
        return $this->belongsTo(RequestPriroda::class, 'priroda_id', 'priroda_id');
    }

    private function is_serialized($data)
    {
        // if it isn't a string, it isn't serialized
        if (!is_string($data))
            return false;
        $data = trim($data);
        if ('N;' == $data)
            return true;
        if (!preg_match('/^([adObis]):/', $data, $badions))
            return false;
        switch ($badions[1]) {
            case 'a' :
            case 'O' :
            case 's' :
                if (preg_match("/^{$badions[1]}:[0-9]+:.*[;}]\$/s", $data))
                    return true;
                break;
            case 'b' :
            case 'i' :
            case 'd' :
                if (preg_match("/^{$badions[1]}:[0-9.E-]+;\$/", $data))
                    return true;
                break;
        }
        return false;
    }

    public function getPhotoAdminPreviewAttribute()
    {
        if ($this->is_serialized($this->attributes['photo_admin'])) {
            $photo_preview = unserialize($this->attributes['photo_admin']);
        } else {
            $photo_preview = $this->attributes['photo_admin'];
        }

        $result = [];
        if (is_array($photo_preview)) {
            foreach ($photo_preview as $preview) {
                $this->generatePreview($result, $preview);
            }
        } else {
            $this->generatePreview($result, $photo_preview);
        }

        if (count($result) === 1 && isset($result[0]['file']) && $result[0]['file'] == '') {
            return null;
        }

        return $result;
    }

    public function getPhotoPreviewAttribute()
    {
        if ($this->is_serialized($this->attributes['photo'])) {
            $photo_preview = unserialize($this->attributes['photo']);
        } else {
            $photo_preview = $this->attributes['photo'];
        }
        $result = [];
        if (is_array($photo_preview)) {
            foreach ($photo_preview as $preview) {
                $this->generatePreview($result, $preview);
            }
        } else {
            $this->generatePreview($result, $photo_preview);
        }

        if (count($result) === 1 && isset($result[0]['file']) && $result[0]['file'] == '') {
            return null;
        }
        return $result;
    }

    /**
     * @param $result
     * @param $preview
     * @return array
     * @throws \ImagickException | \Exception
     */
    private function generatePreview(&$result, $preview)
    {
        $path = public_path() . $preview;
        $path_preview = $path . '.preview.jpg';
        if (!file_exists($path)) {

            $result[] = [
                'path' => $path,
                'file' => $preview,
                'preview' => $preview
            ];
            return $result;
        }

        if (file_exists($path_preview)) {
            $result[] = [
                'path' => $path,
                'file' => $preview,
                'preview' => $preview . '.preview.jpg'
            ];
            return $result;
        }

        if (stripos($path, '.pdf')) {
            try {
                /** @var \Imagick $imagick */
                @$imagick = new \Imagick($path . '[0]'); // 0 specifies the first page of the pdf
                @$imagick->setImageFormat('jpg'); // set the format of the output image
                @$imagick->setImageAlphaChannel(\Imagick::VIRTUALPIXELMETHOD_WHITE);
                @$imagick->setCompressionQuality(100);
                @$imagick->writeImage($path_preview);
            } catch (\Exception $e) {}
        }
        if (
            stripos($path, '.jpg') ||
            stripos($path, '.jpeg') ||
            stripos($path, '.png') ||
            stripos($path, '.gif')

        ) {
            try {
                /** @var \Imagick $imagick */
                @$imagick = new \Imagick($path . '[0]'); // 0 specifies the first page of the pdf
                @$imagick->setImageFormat('jpg'); // set the format of the output image
                @$imagick->scaleImage(100, 0);
                @$imagick->writeImage($path_preview);
            } catch (\Exception $e) {}
        }

        if (file_exists($path_preview)) {
            $result[] = [
                'path' => $path,
                'file' => $preview,
                'preview' => $preview . '.preview.jpg'
            ];

        } else {
            $result[] = [
                'path' => $path,
                'file' => $preview,
                'preview' => $preview
            ];
        }

        return $result;
    }

    public function hasStatusSendInPriroda()
    {
        return in_array($this->status, [Request::STATUS_RASSMOTRENIE, Request::STATUS_IN_WORK]);
    }

    /**
     * @return mixed
     */
    public function uuid()
    {
        return str_replace('{ID}', $this->id, '00{ID}0-0000-0000-000000000000');
    }

    /**
     * @return mixed
     */
    public function mapPoint()
    {
        if ($this->is_serialized($this->attributes['map_point'])) {
            $map_point = unserialize($this->attributes['map_point']);
        } else {
            $map_point = $this->attributes['map_point'];
        }

        return $map_point;
    }

    /**
     * @return null
     */
    public function latitude()
    {
        $map_point = $this->mapPoint();
        return isset($map_point[0]) ? $map_point[0] : null;
    }

    /**
     * @return null
     */
    public function longitude()
    {
        $map_point = $this->mapPoint();
        return isset($map_point[1]) ? $map_point[1] : null;
    }

    public function attachments()
    {
        $files = [];
        $photo_preview = $this->photo_preview;

        if (is_array($photo_preview) && isset($photo_preview['file'])) {
            $photo_preview = [$this->photo_preview];
        }

        if ($photo_preview) {
            foreach($photo_preview as $photo) {
                if (file_exists($photo['path'])) {
                    $files[] = [
                        'name'     => 'file',
                        'contents' => fopen($photo['path'], 'r'),
                        'filename' => basename($photo['path'])
                    ];
                }
            }
        }

        return $files;
    }

    /**
     * Выставляем статус подтверждено и перекидываем фото и комментарии осталвенные модераторами из Нашей природы
     * @param RequestPriroda $priroda
     * @return bool
     */
    public function setStatusSolved(RequestPriroda $priroda)
    {
        @$photo_request = unserialize($this->photo_admin);
        // Собираем все старые фото и загружаем к ним новые
        $photo_request  = is_array($photo_request) ? $photo_request : [];
        $photo_priroda  = is_array($priroda->photo_admin) ? array_pluck($priroda->photo_admin, 'file') : [];
        $photos         = array_merge($photo_request, $photo_priroda);

        $this->comment     = implode(PHP_EOL, $priroda->getCommentsPriroda(true));
        $this->photo_admin = serialize(array_unique($photos));
        $this->status      = Request::STATUS_SOLVE;
        return $this->save();
    }
}
