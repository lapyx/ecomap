<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Help extends Model
{
	public $fillable = ['name', 'text', 'status'];
}
