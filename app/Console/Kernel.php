<?php

namespace App\Console;

use Illuminate\Support\Facades\Log;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
		Commands\ParseNews::class,
		Commands\Parse2News::class,
        Commands\RemoveNews::class,
        Commands\InitNews::class,
        Commands\PrirodaFind::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
    	//Log::info('start_parse', [date('Y-m-d H:i:s')]);

		$schedule->command('news:init', ['page' => ''])
			->daily();
			//->dailyAt(date('H:i'));

		$schedule->command('news:remove')
			->daily();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
