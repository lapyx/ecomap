<?php

namespace App\Console\Commands;

use App\News;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class RemoveNews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'news:remove';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove all news from database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$news = \App\News::orderBy('created_at', 'desc')->limit(30)->get();

		foreach ($news as $new) {
			$news_ids[] = $new->id;
		}

		DB::table('news')->whereNotIn('id', $news_ids)->delete();

		//News::truncate();
        //$this->info('News truncated');
    }
}
