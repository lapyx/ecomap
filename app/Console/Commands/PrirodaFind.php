<?php

namespace App\Console\Commands;

use App\Region;
use App\Request;
use App\RequestPriroda;
use App\Services\ApiPriroda;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PrirodaFind extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'priroda:find';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Api Priroda find';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        try {
            $this->info('Start find new requests');

            /** @var ApiPriroda $api */
            $api = app(ApiPriroda::class);
            $api->signin(true);

            $limit   = 100;
            $saved   = 0;
            $updated = 0;
            //$skiped  = 0;
            $offset  = 0;

            while (true) {
                $data = $api->find([
                    'limit'       => $limit,
                    'offset'      => $offset,
                    'mine'        => true,
                    'status'      => RequestPriroda::FINISHED, // Завершено
                    'updatedFrom' => Carbon::yesterday()->toDateString() // за вчерашний день
                ]);

                //$total   = array_get($data, 'total', 0);
                $data    = array_get($data, 'applications');
                $offset += $limit;

                $this->info("Total find request is ".count($data)." rows");

                if (! $data) break;

                foreach ($data as $i => $one) {
                    $priroda_id = $one['externalCode'];

                    /** @var RequestPriroda $prirodaRequest */
                    $prirodaRequest = RequestPriroda::where('priroda_id', $priroda_id)->first();

                    $region = null;
                    if ($region_uuid = array_get($one, 'place.subjectId')) {
                        $region = Region::where('uuid', $region_uuid)->first();
                    }

                    $lat = array_get($one, 'place.latitude');
                    $lon = array_get($one, 'place.longitude');

                    $values = [
                        'number'      => $one['number'],
                        'subject'     => $one['title'],
                        'address'     => array_get($one, 'place.address'),
                        'description' => $one['description'],
                        'name'        => $one['author'],
                        //'comment'     => $one['categoryName'],
                        //'phone'     => $one[''],
                        'answer'      => (array) $one,
                        'photo'       => $prirodaRequest ? $prirodaRequest->photo : [],
                        'state_id'    => array_get($one, 'approved') ? RequestPriroda::STATE_OK : RequestPriroda::STATE_PROGRESS, // approved - свалка уже подтверждена
                        'date_state_repeat' => null,
                        'status'      => $one['status'],
                        'status_code' => array_get($one, 'processStatus.code'),
                        'region_id'   => $region ? $region->region_id : null,
                        'map_point'   => $lat && $lon ? [$lat, $lon] : null,
                    ];

                    $path = '/images/requests/';

                    if (!empty($one['attachments']) && is_array($one['attachments'])) {
                        foreach ($one['attachments'] as $file) {
                            $filename     = $one['number'].'_'.$file['id'] . '_' . basename($file['url']);
                            $fullFilePath = base_path() . '/public' . $path . $filename;

                            // такой файл уже качали, пропускаем
                            if ($prirodaRequest && file_exists($fullFilePath)) continue;

                            @$content = file_get_contents($file['url']);
                            if ($content) {
                                $image = [
                                    'id'      => $file['id'],
                                    'path'    => $fullFilePath,
                                    'file'    => $path . $filename,
                                    'preview' => $path . $filename
                                ];
                                $values['photo'][] = $image;

                                @file_put_contents($image['path'], $content);
                            } else {
                                $this->info("File ({$file['url']}) download failed, requestId:{$priroda_id}");
                            }
                        }
                    }

                    if (!empty($one['comments']) && is_array($one['comments'])) {
                        foreach ($one['comments'] as $comment) {
                            if (!empty($comment['attachments']) && is_array($comment['attachments'])) {
                                foreach ($comment['attachments'] as $file) {
                                    $filename     = $one['number'].'_'.$file['id'] . '_' . basename($file['url']);
                                    $fullFilePath = base_path() . '/public' . $path . $filename;

                                    // такой файл уже качали, пропускаем
                                    if ($prirodaRequest && file_exists($fullFilePath)) continue;

                                    @$content = file_get_contents($file['url']);
                                    if ($content) {
                                        $image = [
                                            'id'      => $file['id'],
                                            'path'    => $fullFilePath,
                                            'file'    => $path . $filename,
                                            'preview' => $path . $filename
                                        ];
                                        $values['photo_admin'][] = $image;

                                        @file_put_contents($image['path'], $content);
                                    } else {
                                        $this->info("File ({$file['url']}) download failed, requestId:{$priroda_id}");
                                    }
                                }
                            }
                        }
                    }

                    if ($prirodaRequest) {
                        $prirodaRequest->update($values);
                        $this->info("Updated request id:{$prirodaRequest->id}");
                        $updated++;
                    } else {
                        $values['priroda_id'] = $priroda_id;
                        $prirodaRequest = RequestPriroda::create($values);
                        $this->info("Saved new request id:{$prirodaRequest->id}");
                        $saved++;
                    }

                    // если подтвержден
                    if ($prirodaRequest->state_id == RequestPriroda::STATE_OK) {
                        /** @var Request $request */
                        $request = $prirodaRequest->request;
                        if ($request && $request->status != Request::STATUS_SOLVE) {
                            $request->setStatusSolved($prirodaRequest);
                        }
                    }
                }
            }

            $api->signout();

            $this->info("Finished find new requests");
            $this->info("Saved rows $saved");
            $this->info("Updated rows $updated");
            //$this->info("Skiped rows $skiped");
        } catch (\Exception $e) {
            Log::error($e);
            $this->error($e->getMessage());
        }
    }
}
