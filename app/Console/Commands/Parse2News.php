<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DiDom\Document;
use App\News;

class Parse2News extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'news:parse2';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import news from https://onf.ru/generalnaya_uborka';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$path = 'https://onf.ru/generalnaya_uborka';

//		if ('' != $this->argument('page')) {
//			$path .= '?page=' . $this->argument('page');
//		}

		$document = new Document($path, $isFile = true);
		$entryBlock = '.view-content .node';

		$items = $document->find($entryBlock);
		$items = array_reverse($items);

		foreach($items as $item)
		{
			$entryMeta = $item->find('h2 a')[0];
			$entryName = $entryMeta->text();
			$entryLink = $entryMeta->attr('href');

			$news = News::where('name', $entryName)->first();
			if (isset($news) && $news->id) {
				continue;
			}

			$entryBody = new Document('https://onf.ru'.$entryLink, $isFile = true);
			$text = $entryBody->find('.field-name-body .field-item')[0]->text();

			$photos = $entryBody->find('.field-name-field-photos .field-item a');

			foreach ($photos as $photo) {
				$url = $photo->attr('href');
				$text .= '<p class="news__image"><img src="' . $url . '"></p>';
			}

			$news = new News();

			$news->name   = $entryName;
			$news->text   = $text;
			$news->status = 1;

			$news->save();
		}
    }
}
