<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', 'AppController@index');
Route::get('/requests/{id}/to_work', '\App\Http\Controllers\Admin\RequestsController@toWork')->name('admin.requests.to_work');
Route::get('/request/{id}', 'AppController@index');
Route::get('page', 'AppController@page');
Route::get('news', 'AppController@news');
Route::get('help', 'AppController@help');
Route::get('news-item', 'AppController@newsItem');

Route::get('point_all', 'ResourceController@point_all');
Route::get('point_orange', 'ResourceController@point_orange');
Route::get('point_green', 'ResourceController@point_green');

Route::get('/port_query/{id}', 'ResourceController@port_query');

Route::get('/requests/1/ajax_all1', 'ResourceController@ajax_all1');
Route::get('/requests/1/ajax_all2', 'ResourceController@ajax_all2');


Route::get('/requests/2/ajaxOne', 'ResourceController@ajaxOne2');
Route::get('/requests/2/ajaxTwo', 'ResourceController@ajaxTwo2');

Route::get('/requests/2/marker', 'ResourceController@marker2');

Route::get('/regions/ajax', 'ResourceController@regions');

Route::resource('requests', 'ResourceController');

Route::get('departments/{region}', 'ResourceController@getDepartmentInfoGet');
Route::post('departments', 'ResourceController@getDepartmentInfo');

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');

//Route::get('/home', 'HomeController@index');

include 'admin.php';
include 'dashboard.php';