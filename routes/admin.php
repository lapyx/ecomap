<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'auth'], function() {
    Route::get('/', 'Admin\AdminController@index')->name('stat');
    Route::get('/stat_export', 'Admin\AdminController@export')->name('stat.export');
    Route::any('faq', 'Admin\AdminController@faq')->name('faq');
	Route::resource('departments', 'Admin\DepartmentController');
	Route::resource('requests', 'Admin\RequestsController');
	Route::resource('request_priroda', 'Admin\PrirodaController');
	//Route::get('request_priroda/action/{id}', 'Admin\PrirodaController@action')->name('request_priroda.action');
	Route::resource('changes', 'Admin\ChangeController');
	Route::resource('personal', 'Admin\PersonalController');
	Route::resource('news', 'Admin\NewsController');
	Route::resource('pages', 'Admin\PagesController');
	Route::resource('instructions', 'Admin\InstructionsController');
	Route::resource('regions', 'Admin\RegionsController');
});


Route::put('admin/changes/{id}/apply', 'Admin\ChangeController@apply')->name('admin.changes.apply');
Route::get('admin/changes_stat', 'Admin\ChangeStatController@index')->name('admin.changes_stat.index');
Route::get('admin/changes_stat/{id}', 'Admin\ChangeStatController@show')->name('admin.changes_stat.show');
Route::post('admin/requests/{id}/rotate_img', 'Admin\RequestsController@rotateFiles')->name('admin.requests.rotate_img');
