<?php

use App\Region;
use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('get_federal_okrug', function () {
    /** @var \App\Services\ApiPriroda $api */
    $api = app(\App\Services\ApiPriroda::class);
    $api->signin();
    $ret = $api->getFederalDistricts();
    \Illuminate\Support\Facades\Log::debug($ret);
    print_r($ret);
    $api->signout();
})->describe('Display an inspiring quote');

Artisan::command('set_federal_uuid', function () {
    /** @var \App\Services\ApiPriroda $api */
    $api = app(\App\Services\ApiPriroda::class);
    $api->signin();
    $ret = $api->getFederalDistricts();

    foreach(array_get($ret,'federalDistricts', []) as $one){
        $id = $one['id'];
        $regions = [];
        foreach ($one['subjects'] as $reg) {
            $regions[] = $reg['id'];
        }
        Region::whereIn('uuid', $regions)->update(['federal_uuid' => $id]);
        echo "federalDistricts id:$id is set".PHP_EOL;
    }

    $api->signout();
})->describe('Display an inspiring quote');