@extends('base')


@section('map_image')http://kartasvalok.ru{{$map_image}}@endsection
@section('content')
    <div id="app">
        <app></app>
    </div>

    <script>
        var user = '{{ $user ? $user->name : false }}';
        var currentRequest = '{{ $request_id ? $request_id : false }}';
        var currentPoint = '{{ $map_point ? $map_point : false }}';
        var currentRegion = '{{ $region }}';
    </script>
@endsection
