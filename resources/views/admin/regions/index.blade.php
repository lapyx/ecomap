@extends('admin.base')

@section('content')
	<h1>Регионы</h1>

	{{--
	<form class="pull-right">
		<input class="form-control" type="text" placeholder="Поиск" name="search">
	</form>
	<div class="clearfix"></div>

	@if ($search)
		<p style="margin-top: 1em;">
			Поиск: "{{ $search }}"<br>
			&larr;&nbsp;<a href="/admin/regions">Все регионы</a>
		</p>
	@endif
	--}}

	<table class="table" style="margin-top: .75em;">
		<thead>
		<td>Регион</td>
		<td>Цвет</td>
		</thead>
		@foreach($regions as $reg)
			<tr>
				<td>
					{{ $reg->name }}
				</td>
				<td>
					<ul class="reg-colors" id="reg-colors" data-regid="{{ $reg->id }}" data-action="{{ route('admin.regions.update', ['region' => $reg->id]) }}">
						<li class="reg-green @if($reg->color == '#E6EE9C') active @endif" data-color="#E6EE9C"><i></i></li>
						<li class="reg-orange @if($reg->color == '#FFA93F') active @endif" data-color="#FFA93F"><i></i></li>
						<li class="reg-red @if($reg->color == '#EF0000') active @endif" data-color="#EF0000"><i></i></li>
						<li class="reg-green2 @if($reg->color == '#5dbc7c') active @endif" data-color="#5dbc7c"><i></i></li>
					</ul>
				</td>
			</tr>
		@endforeach
	</table>
@endsection