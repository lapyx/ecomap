@extends('admin.base')

@section('content')
    <h1>Статистика</h1>
    <p style="margin-bottom: 30px">Всего обращений: <b>{{ $total }}</b>, из них:</p>

    <p class="status small" style="margin-bottom: 20px">
        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> На модерации: <b>{{ $moderate }}</b>
    </p>

    <p class="status small text-danger" style="margin-bottom: 20px">
        <i class="fa fa-info-circle" aria-hidden="true"></i> В стадии рассмотрения: <b>{{ $pending }}</b>
    </p>

    <p class="status small text-warning" style="margin-bottom: 20px">
        <i class="fa fa-hourglass" aria-hidden="true"></i> В работе: <b>{{ $work }}</b>
    </p>

    <p class="status small text-success">
        <i class="fa fa-check" aria-hidden="true"></i> Проблем решено: <b>{{ $done }}</b>
    </p>
    @if(isset($regions))
    @include('admin.rating.index')
    @endif
@endsection