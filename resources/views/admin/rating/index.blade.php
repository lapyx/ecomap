<div class="col-md-12">
    <a class="btn  btn-info" href="{{route('admin.stat.export')}}" target="_blank">Выгрузка </a>
</div>

<table class="table" style="margin-top: .75em;">
    <thead>
    <th>
        <a href="{{route('admin.stat',['sort'=>App\Http\Controllers\Admin\AdminController::SORTBY_REGION])}}">Регион</a>
    </th>
    <th>
        <a href="{{route('admin.stat',['sort'=>App\Http\Controllers\Admin\AdminController::SORTBY_MODERATE])}}">На модерации</a>
    </th>
    <th>
        <a href="{{route('admin.stat',['sort'=>App\Http\Controllers\Admin\AdminController::SORTBY_RASSMOTRENIE])}}">На рассмотрении</a>
    </th>
    <th>
        <a href="{{route('admin.stat',['sort'=>App\Http\Controllers\Admin\AdminController::SORTBY_IN_WORK])}}">В работе</a>
    </th>
    <th>
        <a href="{{route('admin.stat',['sort'=>App\Http\Controllers\Admin\AdminController::SORTBY_SOLVE])}}">Проблем решено</a>
    </th>
    <th>
        <a href="{{route('admin.stat',['sort'=>App\Http\Controllers\Admin\AdminController::SORTBY_NUMBERS])}}">Количество</a>
    </th>
	<th><a href="{{route('admin.stat',['sort'=>App\Http\Controllers\Admin\AdminController::SORTBY_EFFECT2])}}">Эффективность</a>
    </th>
    </thead>
    @foreach($regions as $reg)
        <tr>
            <td>
                <b>{{ $loop->index+1 }}</b>. {{ $reg['name'] }}
            </td>
            <td>
                <b>{{$reg[\App\Request::STATUS_MODERATE]}} </b>
            </td>
            <td>
                <b style="color: red">{{$reg[\App\Request::STATUS_RASSMOTRENIE]}}           </b>
            </td>
            <td>
                <b style="color: orange">{{$reg[\App\Request::STATUS_IN_WORK]}}</b>
            </td>
            <td>
                <b style="color: green">{{$reg[\App\Request::STATUS_SOLVE]}}</b>
            </td>
            <td>
                <b style="color: black"> {{$reg['total']}}</b>
            </td>
			<td>
                <b>{{round($reg['effect2'],2)}} </b>
            </td>
        </tr>
    @endforeach
</table>