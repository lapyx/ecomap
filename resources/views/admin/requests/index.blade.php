@extends('admin.base')

@section('content')
    <form action="{{ \Illuminate\Support\Facades\Request::url() }}" method="get" id="filter-form">
        @if($filtered)
            <a href="/admin/requests">&larr; Все обращения</a>
            <hr>

        @endif
        <div class="row">
            <div class="col-sm-12">
                <h1>Обращения</h1>
            </div>
        </div>
        <div class="row">
            @if($admin)
                <div class="col-sm-2">
                    <select name="region" class="form-control" id="select-region">
                        <option value=""
                                @if(!isset($filter['region']) || $filter['region'] == '') selected="selected" @endif>
                            Выбор региона
                        </option>
                        @foreach($regions as $k => $region)
                            <option value="{{ $k }}"
                                    @if(isset($filter['region']) && $filter['region'] == $k) selected="selected" @endif>{{ $region }}</option>
                        @endforeach
                    </select>
                </div>
            @endif
            <div class="col-sm-2">
                <input type="text" name="{{\App\Request::FILTER_ID}}" placeholder="Номер" class="form-control"
                       @if(isset($filter[\App\Request::FILTER_ID])) value="{{ $filter[\App\Request::FILTER_ID] }}" @endif>
            </div>
            <div class="col-sm-2">
                <input type="text" name="{{\App\Request::FILTER_ZAYAV}}" placeholder="Заявитель"
                       class="form-control"
                       @if(isset($filter[\App\Request::FILTER_ZAYAV])) value="{{ $filter[\App\Request::FILTER_ZAYAV] }}" @endif>
            </div>
            <div class="col-sm-3">
                <input type="text" name="{{\App\Request::FILTER_EMAIL}}" placeholder="Email"
                       class="form-control"
                       @if(isset($filter[\App\Request::FILTER_EMAIL])) value="{{ $filter[\App\Request::FILTER_EMAIL] }}" @endif>
            </div>
            <div class="col-sm-3">
                <input type="text" name="{{\App\Request::FILTER_PHONE}}" placeholder="Телефон"
                       class="form-control"
                       @if(isset($filter[\App\Request::FILTER_PHONE])) value="{{ $filter[\App\Request::FILTER_PHONE] }}" @endif>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <input style="margin-top: 10px;display:block;;width: 100%;" class=" btn btn-info" type="submit"
                       value="Поиск">
            </div>
        </div>
        <table class="table">
            <thead>
            <td width="50">#</td>
            <td width="100">Файлы</td>
            <td>Тема</td>
            @if($admin)
                <td>Регион</td>
            @endif
            <td>Заявитель</td>
            <td width="170">
                <select name="status" id="select-status" class="form-control">
                    <option value=""
                            @if(!isset($filter['status']) || $filter['status'] == '') selected="selected" @endif>Cтатус
                    </option>
                    <option value="0"
                            @if(isset($filter['status']) && $filter['status'] == '0') selected="selected" @endif>На
                        модерации
                    </option>
                    <option value="1"
                            @if(isset($filter['status']) && $filter['status'] == '1') selected="selected" @endif>На
                        рассмотрении
                    </option>
                    <option value="2"
                            @if(isset($filter['status']) && $filter['status'] == '2') selected="selected" @endif>В
                        работе
                    </option>
                    <option value="3"
                            @if(isset($filter['status']) && $filter['status'] == '3') selected="selected" @endif>
                        Проблема решена
                    </option>
                </select>
            </td>
            </thead>
            @foreach($requests as $request)
                <tr>
                    <td>
                        <small class="text-nowrap">{{ $request->id }}</small>
                    </td>
                    <td>
                        @if($request->photo_preview[0]['file']!=='')
                            <a href="{{ $request->photo_preview[0]['file'] }}" target="_blank" class="fancybox">
                                <img src="{{ $request->photo_preview[0]['preview'] }}" style="width: 100px">
                            </a>
                        @endif
                    </td>
                    <td>
                        <small>
                            <a href="{{ route('admin.requests.edit', ['request' => $request->id]) }}">{{ $request->subject }}</a>
                        </small>
                    </td>
                    @if($admin)
                        <td>
                            <small>{{ $request->region_name }}</small>
                        </td>@endif
                    <td>
                        <small>{{ $request->name }}</small>
                    </td>
                    <td>
                        <small class="text-nowrap">
                            @php
                            switch ($request->status) {
                            case 0:
                            echo 'На модерации';
                            break;
                            case 1:
                            echo 'На рассмотрении';
                            break;
                            case 2:
                            echo 'В работе';
                            break;
                            case 3:
                            echo 'Проблема решена';
                            break;
                            }
                            @endphp
                        </small>
                    </td>
                </tr>
            @endforeach
        </table>
    </form>
    <div>
        {{ $requests->appends($filter)->links() }}
    </div>
@endsection