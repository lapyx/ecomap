@extends('admin.base')

@section('content')
    <!-- Modal -->
    <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Вы уверены, что хотите удалить это фото?</h4>
                </div>
                <div class="modal-body">
                    <div id="comfirm-photo"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                    <button type="button" class="btn btn-danger">Удалить</button>
                </div>
            </div>
        </div>
    </div>

    <h1>Редактировать обращение</h1>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif
    <form action="{{ route('admin.requests.update', ['request' => $request->id]) }}" method="post"
          id="edit-request-form"
          enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="put"/>

        <table class="table">
            <tr>
                <td width="180">#</td>
                <td>{{ $request->id }}</td>
            </tr>
            <tr>
                <td>Тема</td>
                <td>
                    <input type="text" class="form-control" name="subject" value="{{ $request->subject }}">
                </td>
            </tr>
            <tr>
                <td>Сообщение</td>
                <td>
                    <textarea name="description" class="form-control">{{ $request->description }}</textarea>
                </td>
            </tr>
            <tr>
                <td>Адрес</td>
                <td>
                    <input type="text" class="form-control" name="address" value="{{ $request->address }}">
                </td>
            </tr>
            <tr>
                <td>Геопозиция</td>
                <td>
                    <input type="text" class="form-control" name="map_point"
                           @if(isset($request->map_point[0]) && isset($request->map_point[1])) value="{{ $request->map_point[0] }}, {{ $request->map_point[1] }}"
                           @endif required>
                </td>
            </tr>
            <tr>
                <td>Заявитель</td>
                <td>
                    @if($admin)
                        <input type="text" class="form-control" name="name" value="{{ $request->name }}">
                    @else
                        {{ $request->name }}
                    @endif
                </td>
            </tr>
            <tr>
                <td>Телефон</td>
                <td>
                    @if($admin)
                        <input type="text" class="form-control" name="phone" value="{{ $request->phone }}">
                    @else
                        {{ $request->phone }}
                    @endif
                </td>
            </tr>
            <tr>
                <td>E-mail</td>
                <td>
                    @if($admin)
                        <input type="text" class="form-control" name="email" value="{{ $request->email }}">
                    @else
                        {{ $request->email }}
                    @endif
                </td>
            </tr>
            <tr>
                <td>Фото</td>
                <td>
                    <div class="requset-photos">

                        @if (is_array($request->photo_preview))
                            @if(isset($request->photo_preview['file']))
                                <div class="photo">
                                    <a href="{{ $request->photo_preview['file'] }}" rel="gallery" class="fancybox"
                                       target="_blank">
                                        <img src="{{ $request->photo_preview['preview'] }}" style="width: 100px"></a>
                                    @if($admin)
                                        <button type="button" class="btn btn-xs btn-danger btn-block delete-user-photo">
                                            Удалить
                                        </button>@endif
                                </div>
                            @else
                                @foreach($request->photo_preview as $ph)

                                    <div class="photo">
                                        <a href="{{ $ph['file'] }}" rel="gallery" class="fancybox" target="_blank">
                                            <img src="{{ $ph['preview'] }}" style="width: 100px"></a>
                                        @if($admin)
                                            <button type="button"
                                                    class="btn btn-xs btn-danger btn-block delete-user-photo">
                                                Удалить
                                            </button>@endif
                                    </div>
                                @endforeach
                            @endif
                        @endif
                    </div>
                    <div id="deleted-photos"></div>
                </td>
            </tr>
            @if($request->participate)
                <tr class="success">
                    <td></td>
                    <td>Готов участвовать ликвидации экологического нарушения</td>
                </tr>
            @endif
            <tr>
                <td>Статус</td>
                <td>
                    <select name="status" class="form-control">
                        <option value="0">На модерации</option>
                        <option value="1" @if(1 == $request->status) selected="selected" @endif>На рассмотрении</option>
                        <option value="2" @if(2 == $request->status) selected="selected"
                                @endif
                                @if(Auth::user()->department_id)disabled="disabled"@endif
                        >В работе
                        </option>
                        <option value="3" @if(3 == $request->status) selected="selected"
                                @endif
                                @if(Auth::user()->department_id)disabled="disabled"@endif
                        >Проблема решена
                        </option>
                    </select>
                    @if(Auth::user()->department_id && $request->status > 1)
                        <input type="hidden" name="status" value="{{ $request->status }}">
                    @endif
                </td>
            </tr>

            <tr>
                <td>Запрос на смену статуса</td>
                <td>
                    <select name="new_status" class="form-control">
                        <option value="-1">Выберите новый статус</option
                        @if($request->status!=0)
                            <option value="2">На модерации</option>
                        @endif
                        @if($request->status!=1)
                            <option value="1">На рассмотрении</option>
                        @endif
                        @if($request->status!=2)
                            <option value="2">В работе</option>
                        @endif
                        @if($request->status!=3)
                            <option value="3">Проблема решена</option>
                        @endif
                    </select>
                    @if(Auth::user()->department_id && $request->status > 1)
                        <input type="hidden" name="status" value="{{ $request->status }}">
                    @endif
                </td>
            </tr>
            <tr>
                <td>Комментарий</td>
                <td>
                    <textarea name="comment" rows="6" class="form-control">{{ $request->comment }}</textarea>
                </td>
            </tr>
            <tr>
                <td>Загрузить фотографии</td>
                <td>
                    <div id="uploaded" class="uploaded-photos">
                        @if (is_array($request->photo_admin_preview))

                            @if(isset($request->photo_admin_preview['file']))
                                <div class="uploaded-photo">
                                    <div class="preview">
                                        <a href="{{ $request->photo_admin_preview['file'] }}" rel="gallery_admin"
                                           class="fancybox"
                                           target="_blank"><img
                                                    src="{{ $request->photo_admin_preview['preview'] }}"></a>
                                    </div>
                                    <button type="button" class="btn btn-xs btn-block btn-danger delete-file">Удалить
                                    </button>
                                </div>
                            @else
                                @foreach($request->photo_admin_preview as $ph)
                                    <div class="uploaded-photo">
                                        <div class="preview">
                                            <a href="{{ $ph['file'] }}" rel="gallery_admin" class="fancybox"
                                               target="_blank"><img
                                                        src="{{ $ph['preview'] }}"></a>
                                        </div>
                                        <button type="button" class="btn btn-xs btn-block btn-danger delete-file">
                                            Удалить
                                        </button>
                                    </div>
                                @endforeach
                            @endif
                        @endif
                    </div>
                    <hr>
                    <div id="request-photos"></div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar"
                             aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%"></div>
                    </div>
                    <div class="form-group">
                        <input type="file" name="files[]" id="fileupload" multiple style="display: none">
                        <button type="button" class="btn btn-default" id="upload-photos">Выбрать файлы</button>
                    </div>
                </td>
            </tr>

            @if ($request->priroda)
                <tr>
                    <td width="180">Наша Природа</td>
                    <td>
                        <a href="{{ route('admin.request_priroda.edit', $request->priroda) }}">{{ $request->priroda_id }}</a>
                    </td>
                </tr>
            @endif

            @if($request->hasStatusSendInPriroda() && $request->region)
                @php $class = $request->priroda_id ? 'success' : !$request->region->priroda_active?'danger':''; @endphp
                <tr class="{{ $class }}">
                    <td colspan="3">
                        @if(!$request->region->priroda_active)
                            Нельзя регистрировать сообщения в системе "Наша природа" для региона {{ $request->region->name }}
                        @elseif ($request->priroda_id)
                            Обращение было отправлено в "Наша природа" ({{ $request->priroda_date }})
                        @else
                            <a href="{{ route('admin.requests.to_work',['id'=>$request->id]) }}" class="btn btn-warning">Отправить
                                в систему "Наша природа"</a>
                        @endif
                    </td>
                </tr>
            @endif
            <tr>

                    <td colspan="2">
                        <input type="submit" class="btn btn-primary" value="Сохранить">
                        <a href="{{ route('admin.requests.index') }}" class="btn btn-secondary">Отмена</a>
                    </td>
            </tr>
        </table>
    </form>

    @if(!Auth::user()->department_id)
        <table class="table">
            <tr>
                <td colspan="2">
                    <form action="{{ route('admin.requests.destroy', ['request' => $request->id]) }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="delete"/>
                        <input type="submit" class="btn btn-danger" value="Удалить"
                               onclick="return confirm('Вы уверены?');">
                    </form>
                </td>
            </tr>
        </table>
    @endif
@endsection