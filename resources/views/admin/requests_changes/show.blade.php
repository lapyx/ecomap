@extends('admin.base')

@section('content')
        <!-- Modal -->
<div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Вы уверены, что хотите удалить это фото?</h4>
            </div>
            <div class="modal-body">
                <div id="comfirm-photo"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" class="btn btn-danger">Удалить</button>
            </div>
        </div>
    </div>
</div>

<h1>Резолюция на обращение</h1>

<table class="table">
    <tr>
        <td width="180">#</td>
        <td>{{ $request->id }}</td>
    </tr>
    <tr>
        <td>Свалка номер</td>
        <td>
            <a target="_blank" href="{{route('admin.requests.edit',['id'=>$request->request()->first()->id])}}">
                {{ $request->request()->first()->id }}
            </a>
        </td>
    </tr>
    <tr>
        <td>Свалка</td>
        <td>
            {{ $request->request()->first()->subject }}
        </td>
    </tr>
    <tr>
        <td>Суть запроса</td>
        <td>
            @php
            switch($request->old_request_status){
            case 0 : echo "На модерации"; break;
            case 1 : echo "На рассмотрении"; break;
            case 2 : echo "В работе"; break;
            case 3 : echo "Проблема решена"; break;
            }
            @endphp
            ->
            @php
            switch($request->new_request_status){
            case 0 : echo "На модерации"; break;
            case 1 : echo "На рассмотрении"; break;
            case 2 : echo "В работе"; break;
            case 3 : echo "Проблема решена"; break;
            }
            @endphp
        </td>
    </tr>
    <tr>
        <td>Статус запроса</td>
        <td>
            @php
            switch ($request->status) {
            case 0:
            echo 'Открыт';
            break;
            case 1:
            echo 'Принят';
            break;
            case 2:
            echo 'Отклонен';
            break;
            }
            @endphp
        </td>
    </tr>
    <tr>
        <td>Резолюция</td>
        <td>
            {{$request->reason}}
        </td>
    </tr>

</table>


@endsection