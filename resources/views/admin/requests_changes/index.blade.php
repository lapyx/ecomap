@extends('admin.base')

@section('content')
    <form action="{{ \Illuminate\Support\Facades\Request::url() }}" method="get" id="filter-form">
        <div class="row">
            <div class="col-sm-6">
                <h1>Обращения</h1>
            </div>
        </div>
        <table class="table">
            <thead>
            <th width="50">Свалка №</th>
            <th>Старый статус</th>
            <th>Новый статус</th>
            <th>Резолюция</th>
            <th>Комментарий</th>
            </thead>
            @foreach($requests as $request)
                <tr>
                    <td>
                        @if($request->request()->first())
                            <a target="_blank"
                               href="{{route('admin.requests.edit',['id'=>$request->request()->first()->id])}}">
                                {{ $request->request()->first()->id }}
                            </a>
                        @endif
                    </td>

                    <td>
                        @php
                        switch($request->old_request_status){
                        case 0 : echo "На модерации"; break;
                        case 1 : echo "На рассмотрении"; break;
                        case 2 : echo "В работе"; break;
                        case 3 : echo "Проблема решена"; break;
                        }
                        @endphp
                    </td>
                    <td>
                        @php
                        switch($request->new_request_status){
                        case 0 : echo "На модерации"; break;
                        case 1 : echo "На рассмотрении"; break;
                        case 2 : echo "В работе"; break;
                        case 3 : echo "Проблема решена"; break;
                        }
                        @endphp
                    </td>

                    <td>
                        <small class="text-nowrap">
                            @php
                            switch ($request->request_status) {
                            case 0:
                            echo 'Открыт';
                            break;
                            case 1:
                            echo 'Принят';
                            break;
                            case 2:
                            echo 'Отклонен';
                            break;
                            }
                            @endphp
                        </small>
                    </td>
                    <td>
                        {{$request->reason}}
                    </td>
                    {{--<td>--}}
                    {{--<small>--}}
                    {{--<a href="{{ route('admin.changes_stat.show', ['request' => $request->id]) }}">--}}
                    {{--Смотреть резолюцию</a>--}}
                    {{--{{$request->reason}}--}}
                    {{--</small>--}}
                    {{--</td>--}}
                </tr>
            @endforeach
        </table>
    </form>
@endsection