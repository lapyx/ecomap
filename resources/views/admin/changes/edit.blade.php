@extends('admin.base')

@section('content')
        <!-- Modal -->
<div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Вы уверены, что хотите удалить это фото?</h4>
            </div>
            <div class="modal-body">
                <div id="comfirm-photo"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" class="btn btn-danger">Удалить</button>
            </div>
        </div>
    </div>
</div>

<h1>Редактировать обращение</h1>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('admin.changes.apply',['id' => $request->id]) }}" method="POST" id="edit-request-form"
      enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="put"/>

    <table class="table">
        <tr>
            <td width="180">#</td>
            <td>{{ $request->request()->first()->id }}</td>
        </tr>
        <tr>
            <td>Тема</td>
            <td>
                {{ $request->request()->first()->subject }}
            </td>
        </tr>
        <tr>
            <td>Сообщение</td>
            <td>
                {{ $request->request()->first()->description }}
            </td>
        </tr>
        <tr>
            <td>Адрес</td>
            <td>
                {{ $request->request()->first()->address }}
            </td>
        </tr>
        <tr>
            <td>Геопозиция</td>
            <td>
                {{ $request->map_point[0] }}, {{ $request->map_point[1] }}
            </td>
        </tr>
        <tr>
            <td>Заявитель</td>
            <td>
                {{ $request->request()->first()->name }}
            </td>
        </tr>
        <tr>
            <td>Телефон</td>
            <td>
                {{ $request->request()->first()->phone }}
            </td>
        </tr>
        <tr>
            <td>E-mail</td>
            <td>
                {{ $request->request()->first()->email }}
            </td>
        </tr>
        <tr>
            <td>Фото</td>
            <td>
                <div class="requset-photos">
                    @if (is_array($request->request()->first()->photo_preview))
                        @if(isset($request->request()->first()->photo_preview['file']))
                            <div class="photo">
                                <a href="{{ $request->photo_preview['file'] }}" rel="gallery" class="fancybox"
                                   target="_blank">
                                    <img src="{{ $request->photo_preview['preview'] }}" style="width: 100px"></a>
                            </div>
                        @else
                            @foreach($request->request()->first()->photo_preview as $ph)
                                <div class="photo">
                                    <a href="{{ $ph['file'] }}" rel="gallery" class="fancybox" target="_blank">
                                        <img src="{{ $ph['preview'] }}" style="width: 100px"></a>
                                </div>
                            @endforeach
                        @endif
                    @endif
                </div>
                <div id="deleted-photos"></div>
            </td>
        </tr>
        @if($request->participate)
            <tr class="success">

                <td colspan="2">Готов участвовать ликвидации экологического нарушения</td>
            </tr>
        @endif
        <tr>
            <td>Комментарий</td>
            <td>
                {{ $request->request()->first()->comment }}
            </td>
        </tr>
        <tr>
            <td>Загрузить фотографии</td>
            <td>
                <div id="uploaded" class="uploaded-photos">
                    @if (is_array($request->request()->first()->photo_admin_preview))
                        @if(isset($request->request()->first()->photo_admin_preview['file']))
                            <div class="uploaded-photo">
                                <div class="preview">
                                    <a href="{{ $request->request()->first()->photo_admin_preview['file'] }}"
                                       rel="gallery_admin" class="fancybox" target="_blank">
                                        <img src="{{ $request->request()->first()->photo_admin_preview['preview'] }}">
                                    </a>
                                </div>
                                <button type="button" class="btn btn-xs btn-block btn-danger delete-file">Удалить</button>
                            </div>
                        @else
                            @foreach($request->request()->first()->photo_admin_preview as $ph)
                                <div class="uploaded-photo">
                                    <div class="preview">
                                        <a href="{{ $ph['file'] }}" rel="gallery_admin" class="fancybox"
                                           target="_blank">
                                            <img src="{{ $ph['preview'] }}">
                                        </a>
                                    </div>
                                    <button type="button" class="btn btn-xs btn-block btn-danger delete-file">Удалить
                                    </button>
                                </div>
                            @endforeach
                        @endif
                    @endif
                </div>
                <hr>
                <div id="request-photos"></div>
                <div class="progress">
                    <div class="progress-bar progress-bar-success p
                    rogress-bar-striped active" role="progressbar"
                         aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%"></div>
                </div>
                <div class="form-group">
                    <input type="file" name="files[]" id="fileupload" multiple style="display: none">
                    <button type="button" class="btn btn-default" id="upload-photos">Выбрать файлы</button>
                </div>
            </td>
        </tr>
        <tr>
            <td>Cсылка на свалку</td>
            <td>
                <a target="_blank" href="{{route('admin.requests.edit',['id'=>$request->request()->first()->id])}}">Перейти</a>
            </td>
        </tr>
        <td>Текущий статус</td>
        <td>
            @php
            switch($request->request()->first()->status){
            case 0 : echo "На модерации"; break;
            case 1 : echo "На рассмотрении"; break;
            case 2 : echo "В работе"; break;
            case 3 : echo "Проблема решена"; break;
            }
            @endphp
        </td>
        </tr>

        <tr>
            <td>Запрос на смену статуса</td>
            <td>
                @php
                switch($request->old_request_status){
                case 0 : echo "На модерации"; break;
                case 1 : echo "На рассмотрении"; break;
                case 2 : echo "В работе"; break;
                case 3 : echo "Проблема решена"; break;
                }
                @endphp ->
                @php
                switch($request->new_request_status){
                case 0 : echo "На модерации"; break;
                case 1 : echo "На рассмотрении"; break;
                case 2 : echo "В работе"; break;
                case 3 : echo "Проблема решена"; break;
                }
                @endphp
            </td>
        </tr>
        <tr>
            <td>Комментарий к резолюции</td>
            <td>
                <textarea style="width: 100%;height: 200px;" name="reason"></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <button type="submit" name="resolution" value="apply" class="btn btn-success">Принять</button>
                <button type="submit" name="resolution" value="reject" class="btn btn-danger">Отклонить</button>
                <a href="{{ route('admin.changes.index') }}" class="btn btn-secondary">Отмена</a>
            </td>
        </tr>
    </table>
</form>

@if(!Auth::user()->department_id)
    <table class="table">
        <tr>
            <td colspan="2">
                <form action="{{ route('admin.changes.destroy', ['change' => $request->id]) }}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="delete"/>
                    <input type="submit" class="btn btn-danger" value="Удалить"
                           onclick="return confirm('Вы уверены?');">
                </form>
            </td>
        </tr>
    </table>
@endif
@endsection