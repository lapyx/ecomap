@extends('admin.base')

@section('content')
    <!-- Modal -->
    <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Вы уверены, что хотите удалить это фото?</h4>
                </div>
                <div class="modal-body">
                    <div id="comfirm-photo"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                    <button type="button" class="btn btn-danger">Удалить</button>
                </div>
            </div>
        </div>
    </div>

    <h1>Редактировать обращение из "Нашей природы"</h1>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif
    <form action="{{ route('admin.request_priroda.update', $priroda) }}" method="post"
          id="edit-request-form"
          enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="put"/>

        <table class="table">
            <tr>
                <td width="180">Обращение</td>
                <td>
                    @if ($priroda->request)
                        <a href="{{ route('admin.requests.edit', $priroda->request) }}">{{ $priroda->request->id }}</a>
                    @endif
                </td>
            </tr>
            <tr>
                <td>Тема</td>
                <td>
                    <input type="text" class="form-control" name="subject" value="{{ $priroda->subject }}">
                </td>
            </tr>
            <tr>
                <td>Сообщение</td>
                <td>
                    <textarea name="description" class="form-control">{{ $priroda->description }}</textarea>
                </td>
            </tr>
            <tr>
                <td>Адрес</td>
                <td>
                    <input type="text" class="form-control" name="address" value="{{ $priroda->address }}">
                </td>
            </tr>
            <tr>
                <td>Геопозиция</td>
                <td>
                    <input type="text" class="form-control" name="map_point"
                           @if(isset($priroda->map_point[0]) && isset($priroda->map_point[1])) value="{{ $priroda->map_point[0] }}, {{ $priroda->map_point[1] }}"
                           @endif required>
                </td>
            </tr>
            <tr>
                <td>Заявитель</td>
                <td>
                    @if($admin)
                        <input type="text" class="form-control" name="name" value="{{ $priroda->name }}">
                    @else
                        {{ $priroda->name }}
                    @endif
                </td>
            </tr>
            <tr>
                <td>Фото</td>
                <td>
                    <div class="requset-photos">

                        @if (is_array($priroda->photo))
                            @foreach($priroda->photo as $ph)
                                <div class="photo">
                                    <a href="{{ $ph['file'] }}" rel="gallery" class="fancybox" target="_blank">
                                        <img src="{{ $ph['preview'] }}" style="width: 100px"></a>
                                    @if($admin)
                                        <button type="button"
                                                class="btn btn-xs btn-danger btn-block delete-user-photo">
                                            Удалить
                                        </button>
                                    @endif
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div id="deleted-photos"></div>
                </td>
            </tr>
            @if($priroda->participate)
                <tr class="success">
                    <td></td>
                    <td>Готов участвовать ликвидации экологического нарушения</td>
                </tr>
            @endif
            <tr>
                <td>Фото с "Нашей природы"</td>
                <td>
                    <div class="requset-photos">

                        @if (is_array($priroda->photo_admin))
                            @foreach($priroda->photo_admin as $ph)
                                <div class="photo">
                                    <a href="{{ $ph['file'] }}" rel="gallery" class="fancybox" target="_blank">
                                        <img src="{{ $ph['preview'] }}" style="width: 100px"></a>
                                    @if($admin)
                                        {{--<button type="button"--}}
                                                {{--class="btn btn-xs btn-danger btn-block delete-user-photo">--}}
                                            {{--Удалить--}}
                                        {{--</button>--}}
                                    @endif
                                </div>
                            @endforeach
                        @endif
                    </div>
                </td>
            </tr>
            <tr>
                <td>Комментарии с "Нашей природы"</td>
                <td style="font-size:80%;">
                    {!!  implode('<br />', $priroda->getCommentsPriroda()) !!}
                </td>
            </tr>
            <tr>
                <td>Комментарий в "Наша природа"</td>
                <td>
                    <textarea name="comment" rows="6" class="form-control">{{ $priroda->comment }}</textarea>
                    <small><i>уходит в "Наша природа" при подверждении администратором*</i></small>
                </td>
            </tr>
            @if ($priroda->state_id == \App\RequestPriroda::STATE_PROGRESS && $priroda->comment_admin)
                <tr>
                    <td>Сообщение от администратора</td>
                    <td>
                        {{ $priroda->comment_admin }}
                    </td>
                </tr>
            @elseif ($admin && $priroda->state_id == \App\RequestPriroda::STATE_ADMIN)
                <tr>
                    <td>Комментарий для РИК ОНФ</td>
                    <td>
                        <textarea name="comment_admin"
                                  class="form-control"
                                  onfocus="$(this).select()"
                                  style="margin-top: 5px;"
                        >{{ old('comment_admin', $priroda->comment_admin) }}</textarea>
                    </td>
                </tr>
            @endif
            <tr>
            </tr>
            <tr>
                @if (!$admin && $priroda->state_id == \App\RequestPriroda::STATE_PROGRESS)
                    <td colspan="2">
                        <button name="accept" value="1" class="btn btn-success">Подтвердить</button>
                        <button name="accept" value="0" class="btn btn-danger">Отказать</button>
                    </td>
                @elseif ($admin && $priroda->state_id == \App\RequestPriroda::STATE_ADMIN)
                    <td colspan="2">
                        <button name="accept_admin" value="1" class="btn btn-success">Подтвердить</button>
                        <button name="accept_admin" value="0" class="btn btn-danger">Отказать</button>
                    </td>
                @elseif ($priroda->state_id == \App\RequestPriroda::STATE_ADMIN)
                    <td colspan="2" class="success">На подтверждении у администратора!</td>
                @elseif ($priroda->state_id == \App\RequestPriroda::STATE_OK)
                    <td colspan="2" class="success">Обращение подтверждено!</td>
                @elseif ($priroda->state_id == \App\RequestPriroda::STATE_REPEAT)
                    <td colspan="2" class="danger">Обращение "Отправлено повторно" ({{ $priroda->date_state_repeat }})</td>
                @endif
            </tr>

            <tr>
                <td colspan="2">
                    <input type="submit" class="btn btn-primary" value="Сохранить">
                    <a href="{{ route('admin.request_priroda.index') }}" class="btn btn-secondary">Отмена</a>
                </td>
            </tr>
        </table>
    </form>

    @if($admin)
        <table class="table">
            <tr>
                <td colspan="2">
                    <form action="{{ route('admin.request_priroda.destroy', ['request' => $priroda->id]) }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="delete"/>
                        <input type="submit" class="btn btn-danger" value="Удалить"
                               onclick="return confirm('Вы уверены?');">
                    </form>
                </td>
            </tr>
        </table>
    @endif
@endsection