@extends('admin.base')

@section('content')
    <form action="{{ \Illuminate\Support\Facades\Request::url() }}" method="get" id="filter-form">
        @if($filtered)
            <a href="{{ route('admin.request_priroda.index') }}">&larr; Все обращения</a>
            <hr>
        @endif
        <div class="row">
            <div class="col-sm-12">
                <h1>Работа с "Нашей природой"</h1>
            </div>
        </div>
        <div class="row">
            @if($admin)
                <div class="col-sm-2">
                    <select name="region" class="form-control" id="select-region">
                        <option value=""
                                @if(!isset($filter['region']) || $filter['region'] == '') selected="selected" @endif>
                            Выбор региона
                        </option>
                        @foreach($regions as $k => $region)
                            <option value="{{ $k }}"
                                    @if(isset($filter['region']) && $filter['region'] == $k) selected="selected" @endif>{{ $region }}</option>
                        @endforeach
                    </select>
                </div>
            @endif
            <div class="col-sm-2">
                <input type="text" name="{{\App\Request::FILTER_ID}}" placeholder="Номер" class="form-control"
                       @if(isset($filter[\App\Request::FILTER_ID])) value="{{ $filter[\App\Request::FILTER_ID] }}" @endif>
            </div>
            <div class="col-sm-3">
                <input type="text" name="{{\App\Request::FILTER_NUMBER}}" placeholder="Исходный номер" class="form-control"
                       @if(isset($filter[\App\Request::FILTER_NUMBER])) value="{{ $filter[\App\Request::FILTER_NUMBER] }}" @endif>
            </div>
            {{--<div class="col-sm-2">--}}
                {{--<input type="text" name="{{\App\Request::FILTER_ZAYAV}}" placeholder="Заявитель"--}}
                       {{--class="form-control"--}}
                       {{--@if(isset($filter[\App\Request::FILTER_ZAYAV])) value="{{ $filter[\App\Request::FILTER_ZAYAV] }}" @endif>--}}
            {{--</div>--}}
        </div>
        <div class="row">
            <div class="col-md-12">
                <input style="margin-top: 10px;display:block;;width: 100%;" class=" btn btn-info" type="submit"
                       value="Поиск">
            </div>
        </div>
        <table class="table">
            <thead>
            <td width="50">Номер</td>
            <td width="100">Файлы</td>
            <td>Тема</td>
            @if($admin)
                <td>Регион</td>
            @endif
            <td>Исходный номер</td>
            <td width="170">
                <select name="state_id" id="select-status" class="form-control">
                    <option value=""
                            @if(!isset($filter['state_id']) || $filter['state_id'] == '') selected="selected" @endif>Состояние
                    </option>
                    @foreach(\App\RequestPriroda::getState() as $code => $name)
                        <option value="{{ $code }}"
                                @if(array_get($filter, 'state_id') == $code) selected="selected" @endif>{{ $name }}
                        </option>
                    @endforeach
                </select>
            </td>
            </thead>
            @foreach($prirodaRequest as $request)
                <tr>
                    <td>
                        <small class="text-nowrap">{{ $request->number }}</small>
                    </td>
                    <td>
                        @if($request->photo)
                            <a href="{{ $request->photo[0]['file'] }}" target="_blank" class="fancybox">
                                <img src="{{ $request->photo[0]['preview'] }}" style="width: 100px">
                            </a>
                        @endif
                    </td>
                    <td>
                        <small>
                            <a href="{{ route('admin.request_priroda.edit', ['request' => $request->id]) }}">{{ $request->subject }}</a>
                        </small>
                    </td>
                    @if($admin)
                        <td>
                            <small>{{ $request->region ? $request->region->name  : ''}}</small>
                        </td>
                    @endif
                    <td>
                        <small>{{ $request->request ? $request->request->id : '' }}</small>
                    </td>
                    <td>
                        <small class="text-nowrap">{{ $request->stateName() }} @if ($request->date_state_repeat)<br />({{ $request->date_state_repeat }})@endif</small>
                    </td>
                </tr>
            @endforeach
        </table>
    </form>
    <div>
        {{ $prirodaRequest->appends($filter)->links() }}
    </div>
@endsection