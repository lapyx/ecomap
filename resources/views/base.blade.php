<html lang="ru" class="fa-events-icons-ready">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta id="token" name="token" value="BHZPGh0Cr1Qsbwz1ZluFbtSy0R3GPzBr7VZPjoa0">
    <title>Интерактивная карта свалок</title>
    <meta property="og:title" content="Интерактивная карта свалок"/>


    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Интерактивная карта свалок">
    <meta property="og:title" content="Интерактивная карта свалок">
    <meta property="og:description" content="">
    <meta property="og:url" content="{{Request::url()}}">
    <meta property="og:locale" content="ru_RU">
    <meta property="og:image" content="@yield('map_image')"/>
    <link rel="image_src" href="@yield('map_image')"/>

    <meta property="image" content="@yield('map_image')"/>
    <meta property="og:image:width" content="450">
    <meta property="og:image:height" content="450">

    <meta name="twitter:card" content="@yield('map_image')">
    <meta name="twitter:title" content="Интерактивная карта свалок">
    <meta name="twitter:description" content="Интерактивная карта свалок">
    <meta name="twitter:image:src" content="@yield('map_image')">
    <meta name="twitter:url" content="{{Request::url()}}">
    <meta name="twitter:domain" content="kartasvalok.ru">
    <meta name="twitter:site" content="@Интерактивная карта свалок">
    <meta name="twitter:creator" content="@Интерактивная карта свалок">

    <link href="https://fonts.googleapis.com/css?family=Open Sans:100,600" rel="stylesheet" type="text/css">
    <style type="text/css"></style>
    <link rel="stylesheet" type="text/css" href="http://kartasvalok.ru/js/fancybox/jquery.fancybox.css">
    <link rel="stylesheet" type="text/css" href="/css/app.css?v=1500880262">
    <link href="https://use.fontawesome.com/1e1e181846.css" media="all" rel="stylesheet">
    <script>
        var user = '{{ $user ? $user->name : false }}';
        var currentRequest = '{{ $request_id ? $request_id : false }}';
        var currentPoint = '{{ $map_point ? $map_point : false }}';
        var currentRegion = '{{ $region }}';
    </script>

</head>

<body>
<div id="app">
    <div>
        <div id="map"></div>
        <div class="header"><img src="/images/logo.png" title="Экологическая карта">

            <div class="navigation">
                <a href="/">Главная</a>
                <a href="#" id="page?id=2">О проекте</a>
                <a href="#" id="news">Новости</a>
                <a href="#" id="page?id=1">Контакты</a>
                <a href="#" id="instructions">Инструкции</a>

            </div>
            <div class="select-region">
                Выберите регион:
                <select name="region_name" class="form-control">
                    <option>Выбрать регион</option>
                    <option data-id="253256" value="0">Адыгея</option>
                    <option data-id="144764" value="1">Алтайский край</option>
                    <option data-id="147166" value="2">Амурская область</option>
                    <option data-id="140337" value="3">Архангельская область</option>
                    <option data-id="112819" value="4">Астраханская область</option>
                    <option data-id="77677" value="5">Башкортостан</option>
                    <option data-id="83184" value="6">Белгородская область</option>
                    <option data-id="81997" value="7">Брянская область</option>
                    <option data-id="145729" value="8">Бурятия</option>
                    <option data-id="72197" value="9">Владимирская область</option>
                    <option data-id="77665" value="10">Волгоградская область</option>
                    <option data-id="115106" value="11">Вологодская область</option>
                    <option data-id="72181" value="12">Воронежская область</option>
                    <option data-id="109876" value="13">Дагестан</option>
                    <option data-id="147167" value="14">Еврейская автономная область</option>
                    <option data-id="145730" value="15">Забайкальский край</option>
                    <option data-id="85617" value="16">Ивановская область</option>
                    <option data-id="253252" value="17">Ингушетия</option>
                    <option data-id="145454" value="18">Иркутская область</option>
                    <option data-id="109879" value="19">Кабардино-Балкария</option>
                    <option data-id="103906" value="20">Калининградская область</option>
                    <option data-id="108083" value="21">Калмыкия</option>
                    <option data-id="81995" value="22">Калужская область</option>
                    <option data-id="151233" value="23">Камчатский край</option>
                    <option data-id="109878" value="24">Карачаево-Черкесия</option>
                    <option data-id="144763" value="25">Кемеровская область</option>
                    <option data-id="115100" value="26">Кировская область</option>
                    <option data-id="85963" value="27">Костромская область</option>
                    <option data-id="108082" value="28">Краснодарский край</option>
                    <option data-id="190090" value="29">Красноярский край</option>
                    <option data-id="140290" value="30">Курганская область</option>
                    <option data-id="72223" value="31">Курская область</option>
                    <option data-id="176095" value="32">Ленинградская область</option>
                    <option data-id="72169" value="33">Липецкая область</option>
                    <option data-id="151228" value="34">Магаданская область</option>
                    <option data-id="115114" value="35">Марий Эл</option>
                    <option data-id="72196" value="36">Мордовия</option>
                    <option data-id="102269" value="37">Москва</option>
                    <option data-id="51490" value="38">Московская область</option>
                    <option data-id="2099216" value="39">Мурманская область</option>
                    <option data-id="274048" value="40">Ненецкий автономный округ</option>
                    <option data-id="72195" value="41">Нижегородская область</option>
                    <option data-id="89331" value="42">Новгородская область</option>
                    <option data-id="140294" value="43">Новосибирская область</option>
                    <option data-id="140292" value="44">Омская область</option>
                    <option data-id="77669" value="45">Оренбургская область</option>
                    <option data-id="72224" value="46">Орловская область</option>
                    <option data-id="72182" value="47">Пензенская область</option>
                    <option data-id="115135" value="48">Пермский край</option>
                    <option data-id="151225" value="49">Приморский край</option>
                    <option data-id="155262" value="50">Псковская область</option>
                    <option data-id="145194" value="51">Республика Алтай</option>
                    <option data-id="393980" value="52">Республика Карелия</option>
                    <option data-id="115136" value="53">Республика Коми</option>
                    <option data-id="72639" value="54">Республика Крым</option>
                    <option data-id="85606" value="55">Ростовская область</option>
                    <option data-id="71950" value="56">Рязанская область</option>
                    <option data-id="72194" value="57">Самарская область</option>
                    <option data-id="337422" value="58">Санкт-Петербург</option>
                    <option data-id="72193" value="59">Саратовская область</option>
                    <option data-id="394235" value="60">Сахалинская область</option>
                    <option data-id="79379" value="61">Свердловская область</option>
                    <option data-id="1574364" value="62">Севастополь</option>
                    <option data-id="110032" value="63">Северная Осетия</option>
                    <option data-id="81996" value="64">Смоленская область</option>
                    <option data-id="108081" value="65">Ставропольский край</option>
                    <option data-id="72180" value="66">Тамбовская область</option>
                    <option data-id="79374" value="67">Татарстан</option>
                    <option data-id="2095259" value="68">Тверская область</option>
                    <option data-id="140295" value="69">Томская область</option>
                    <option data-id="81993" value="70">Тульская область</option>
                    <option data-id="145195" value="71">Тыва</option>
                    <option data-id="140291" value="72">Тюменская область</option>
                    <option data-id="115134" value="73">Удмуртия</option>
                    <option data-id="72192" value="74">Ульяновская область</option>
                    <option data-id="151223" value="75">Хабаровский край</option>
                    <option data-id="190911" value="76">Хакасия</option>
                    <option data-id="140296" value="77">Ханты-Мансийский автономный округ</option>
                    <option data-id="77687" value="78">Челябинская область</option>
                    <option data-id="109877" value="79">Чечня</option>
                    <option data-id="80513" value="80">Чувашия</option>
                    <option data-id="151231" value="81">Чукотский автономный округ</option>
                    <option data-id="151234" value="82">Якутия</option>
                    <option data-id="191706" value="83">Ямало-Ненецкий автономный округ</option>
                    <option data-id="81994" value="84">Ярославская область</option>
                </select>
            </div>
        </div>
        <div class="body">
            <div class="body-filter"></div>
        </div>
        <div class="footer">
            <div class="pull-left"><a href="http://onf.ru" target="_blank">
                    <img src="/images/logo_onf.png" title="Общероссийский народный фронт"/></a>
                <p class="credentials"><strong>Генеральная уборка</strong><br> Интерактивная карта свалок
                </p>
            </div>
            <div class="socials pull-right">
                Мы в соц. сетях:
                <a href="https://vk.com/ecoonf" target="_blank"><img src="/images/vk.png"></a> <a
                        href="https://www.facebook.com/ecoonf/" target="_blank"><img src="/images/facebook.png"></a> <a
                        href="https://twitter.com/ecoonf" target="_blank"><img src="/images/twitter.png"></a> <a
                        href="https://ok.ru/ecoonf" target="_blank"><img src="/images/ok.png"></a> <a
                        href="https://www.youtube.com/channel/UCsjCX2e5ywz-arCPN6_C-tQ" target="_blank"><img
                            src="/images/youtube.png"></a></div>
        </div>
    </div>
</div>
<canvas id="c"/>
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script src="http://kartasvalok.ru/js/jquery-1.10.1.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.js"></script>


<script src="/js/app2.js?{{rand(0,999)}}"></script>
<!--<script src="/js/social.js?{{rand(0,999)}}"></script>-->
<img title="Общероссийский народный фронт" src="@yield('map_image')"/>
<!--Разработка и поддержка: деловые-ресурсы.рф-->
</body>
</html>