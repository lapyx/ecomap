<div class="content">
	<div class="view view-project2-news view-id-project2_news view-display-id-block view-dom-id-e8585381a0d8d8ad10c8d6ee9bf471e1">
		<div class="view-content">
			<div class="views-row views-row-1 views-row-odd views-row-first">
				<script type="text/javascript" src="http://vk.com/js/api/share.js?11" charset="windows-1251"></script>
				<div id="node-47933" class="node node-article node-teaser clearfix" about="/2017/03/02/timofeeva-stavropole-mozhet-voyti-v-chislo-pilotnyh-regionov-gde-zapustyat-reformu/" typeof="sioc:Item foaf:Document">
					<h2 property="dc:title" datatype="">
						<a href="/2017/03/02/timofeeva-stavropole-mozhet-voyti-v-chislo-pilotnyh-regionov-gde-zapustyat-reformu/">Тимофеева: Ставрополье может войти в число пилотных регионов, где запустят реформу обращения с отходами</a>
					</h2>

					<div class="meta submitted"></div>

					<div class="persona">
						<div class="view view-persona view-id-persona view-display-id-block view-dom-id-12deb896fd1da2ec6559fbae82dea344">
							<div class="view-content">
								<div class="view-content-inner">
									<div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
										<div class="views-field views-field-title">
											<span class="field-content"><a href="/timofeeva-olga-viktorovna/">Ольга Тимофеева</a></span>
										</div>
										<div class="views-field views-field-field-image">
											<div class="field-content"><img typeof="foaf:Image" src="https://onf.ru/sites/default/files/styles/position/public/timofeeva-olga-viktorovna-centralnyy-shtab.png?itok=RJCLrn1l" width="200" height="200" alt=""></div>
										</div>
										<div class="views-field views-field-field-post">
											<div class="field-content">Сопредседатель Центрального штаба ОНФ, председатель комитета Государственной думы по экологии и охране окружающей среды, руководитель рабочей группы «Общество и власть: прямой диалог»</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="published">2 марта 2017 года, 17:11</div>
					<div class="content clearfix">
						<div class="field field-name-body field-type-text-with-summary field-label-hidden">
							<div class="field-items">
								<div class="field-item even" property="content:encoded">
									<p>До 70% населения не платят за вывоз мусора. Такую цифру озвучили главы сел Кочубеевского района Ставропольского края на встрече с сопредседателем Центрального штаба ОНФ, председателем комитета Госдумы по экологии и охране окружающей среды Ольгой Тимофеевой. Большинство сельских жителей либо самовольно вывозят мусор в лесополосы и на несанкционированные свалки, либо сжигают у себя во&nbsp;дворах.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="views-row views-row-2 views-row-even">
				<script type="text/javascript" src="http://vk.com/js/api/share.js?11" charset="windows-1251"></script>

				<div id="node-47918" class="node node-article node-teaser clearfix" about="/2017/03/02/aktivisty-onf-v-tatarstane-dobilis-ustraneniya-nesankcionirovannoy-svalki-v-kukmorskom/" typeof="sioc:Item foaf:Document">
					<h2 property="dc:title" datatype=""><a href="/2017/03/02/aktivisty-onf-v-tatarstane-dobilis-ustraneniya-nesankcionirovannoy-svalki-v-kukmorskom/">Активисты ОНФ в Татарстане добились устранения несанкционированной свалки в Кукморском районе</a></h2>

					<div class="meta submitted"></div>

					<div class="published">2 марта 2017 года, 14:45</div>

					<div class="content clearfix">
						<div class="field field-name-field-image field-type-image field-label-hidden">
							<div class="field-items">
								<div class="field-item even" rel="og:image rdfs:seeAlso" resource="https://onf.ru/sites/default/files/styles/onf_2_node_photo_middle/public/cms/wp-content/uploads/2017/03/bez_imeni-1.jpg?itok=Y0Xf0f3e">
									<a href="/2017/03/02/aktivisty-onf-v-tatarstane-dobilis-ustraneniya-nesankcionirovannoy-svalki-v-kukmorskom/"><img typeof="foaf:Image" src="https://onf.ru/sites/default/files/styles/onf_2_node_photo_middle/public/cms/wp-content/uploads/2017/03/bez_imeni-1.jpg?itok=Y0Xf0f3e" width="600" height="340" alt="" title="Активисты ОНФ в Татарстане добились устранения несанкционированной свалки в Кукморском районе"></a>
								</div>
							</div>
						</div>
						<div class="field field-name-body field-type-text-with-summary field-label-hidden">
							<div class="field-items">
								<div class="field-item even" property="content:encoded">
									<p>Активисты Общероссийского народного фронта в Татарстане добились ликвидации несанкционированной свалки твердых коммунальных отходов вблизи села Лубяны Кукморского муниципального района. В настоящий момент отходы вывезены на Кукморский полигон. На освободившемся участке леса проведена планировка территории. Доступ на место бывшей свалки&nbsp;ограничен.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="views-row views-row-3 views-row-odd">
				<script type="text/javascript" src="http://vk.com/js/api/share.js?11" charset="windows-1251"></script>

				<div id="node-47910" class="node node-article node-teaser clearfix" about="/2017/03/02/onf-v-altayskom-krae-vzyal-na-kontrol-likvidaciyu-nezakonnoy-svalki-proizvodstvennyh/" typeof="sioc:Item foaf:Document">
					<h2 property="dc:title" datatype=""><a href="/2017/03/02/onf-v-altayskom-krae-vzyal-na-kontrol-likvidaciyu-nezakonnoy-svalki-proizvodstvennyh/">ОНФ в Алтайском крае взял на контроль ликвидацию незаконной свалки производственных отходов в Камне-на-Оби</a></h2>
					<div class="meta submitted"></div>
					<div class="published">2 марта 2017 года, 13:27</div>
					<div class="content clearfix">
						<div class="field field-name-field-image field-type-image field-label-hidden">
							<div class="field-items">
								<div class="field-item even" rel="og:image rdfs:seeAlso" resource="https://onf.ru/sites/default/files/styles/onf_2_node_photo_middle/public/cms/wp-content/uploads/2017/03/svalka_v_ak_1.jpg?itok=ggaH8DfT">
									<a href="/2017/03/02/onf-v-altayskom-krae-vzyal-na-kontrol-likvidaciyu-nezakonnoy-svalki-proizvodstvennyh/"><img typeof="foaf:Image" src="https://onf.ru/sites/default/files/styles/onf_2_node_photo_middle/public/cms/wp-content/uploads/2017/03/svalka_v_ak_1.jpg?itok=ggaH8DfT" width="600" height="450" alt="" title="ОНФ в Алтайском крае взял на контроль ликвидацию незаконной свалки производственных отходов в Камне-на-Оби"></a>
								</div>
							</div>
						</div>
						<div class="field field-name-body field-type-text-with-summary field-label-hidden">
							<div class="field-items">
								<div class="field-item even" property="content:encoded"
								><p>Активисты Общероссийского народного фронта в Алтайском крае провели рейд по сигналу жителей Камня-на-Оби о незаконной свалке производственных отходов. Общественники обозначили проблемный объект на <a href="http://kartasvalok.ru/request/170215-214">интерактивной карте свалок</a> проекта ОНФ «Генеральная уборка» и направили обращения в региональные органы власти и надзорные ведомства для устранения&nbsp;нарушений.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="views-row views-row-4 views-row-even">
				<script type="text/javascript" src="http://vk.com/js/api/share.js?11" charset="windows-1251"></script>

				<div id="node-47909" class="node node-article node-teaser clearfix" about="/2017/03/02/v-ramkah-proekta-onf-generalnaya-uborka-v-leningradskoy-oblasti-likvidirovana-pervaya/" typeof="sioc:Item foaf:Document">
					<h2 property="dc:title" datatype=""> <a href="/2017/03/02/v-ramkah-proekta-onf-generalnaya-uborka-v-leningradskoy-oblasti-likvidirovana-pervaya/">В рамках проекта ОНФ «Генеральная уборка» в Ленинградской области ликвидирована первая свалка</a></h2>
					<div class="meta submitted"></div>
					<div class="published">2 марта 2017 года, 12:55</div>
					<div class="content clearfix">
						<div class="field field-name-field-image field-type-image field-label-hidden">
							<div class="field-items">
								<div class="field-item even" rel="og:image rdfs:seeAlso" resource="https://onf.ru/sites/default/files/styles/onf_2_node_photo_middle/public/cms/wp-content/uploads/2017/03/25.02.2017.jpeg?itok=s3APta2Z">
									<a href="/2017/03/02/v-ramkah-proekta-onf-generalnaya-uborka-v-leningradskoy-oblasti-likvidirovana-pervaya/">
										<img typeof="foaf:Image" src="https://onf.ru/sites/default/files/styles/onf_2_node_photo_middle/public/cms/wp-content/uploads/2017/03/25.02.2017.jpeg?itok=s3APta2Z" width="600" height="360" alt="" title="В рамках проекта ОНФ «Генеральная уборка» в Ленинградской области ликвидирована первая свалка">
									</a>
								</div>
							</div>
						</div>
						<div class="field field-name-body field-type-text-with-summary field-label-hidden">
							<div class="field-items">
								<div class="field-item even" property="content:encoded">
									<p>Активисты Общероссийского народного фронта в Ленинградской области в рамках проекта «Генеральная уборка» добились ликвидации свалки коммунальных отходов в поселке Пудость Гатчинского района. С момента нанесения проблемного объекта на <a href="http://kartasvalok.ru/">интерактивную карту свалок</a> до ликвидации свалки прошло пять&nbsp;дней.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="views-row views-row-5 views-row-odd views-row-last">
				<script type="text/javascript" src="http://vk.com/js/api/share.js?11" charset="windows-1251"></script>

				<div id="node-47903" class="node node-article node-teaser clearfix" about="/2017/03/02/k-proektu-onf-generalnaya-uborka-prisoedinyayutsya-molodezhnye-dvizheniya-astrahanskoy/" typeof="sioc:Item foaf:Document">
					<h2 property="dc:title" datatype=""><a href="/2017/03/02/k-proektu-onf-generalnaya-uborka-prisoedinyayutsya-molodezhnye-dvizheniya-astrahanskoy/">К проекту ОНФ «Генеральная уборка» присоединяются молодежные движения Астраханской области</a></h2>
					<div class="meta submitted"></div>
					<div class="published">2 марта 2017 года, 11:45</div>
					<div class="content clearfix">
						<div class="field field-name-field-image field-type-image field-label-hidden">
							<div class="field-items">
								<div class="field-item even" rel="og:image rdfs:seeAlso" resource="https://onf.ru/sites/default/files/styles/onf_2_node_photo_middle/public/cms/wp-content/uploads/2017/03/_mg_6524.jpg?itok=a40O0lKh">
									<a href="/2017/03/02/k-proektu-onf-generalnaya-uborka-prisoedinyayutsya-molodezhnye-dvizheniya-astrahanskoy/">
										<img typeof="foaf:Image" src="https://onf.ru/sites/default/files/styles/onf_2_node_photo_middle/public/cms/wp-content/uploads/2017/03/_mg_6524.jpg?itok=a40O0lKh" width="600" height="400" alt="" title="К проекту ОНФ «Генеральная уборка» присоединяются молодежные движения Астраханской области">
									</a>
								</div>
							</div>
						</div>
						<div class="field field-name-body field-type-text-with-summary field-label-hidden">
							<div class="field-items">
								<div class="field-item even" property="content:encoded">
									<p>В Астраханской области идет реализация проекта Общероссийского народного фронта «Генеральная уборка», направленного на борьбу с незаконными мусорными свалками и «серыми» полигонами. В преддверии Дня защитника Отечества была организована встреча с представителями поисковых отрядов. Активисты ОНФ рассказали поисковикам о целях и задачах проекта, а также продемонстрировали обучающий ролик по работе с <a href="http://kartasvalok.ru/">интерактивной картой свалок</a>, рассказывающий о возможности личного участия в улучшении экологической ситуации&nbsp;региона.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<h2 class="element-invisible">Страницы</h2>
		<div class="item-list">
			<ul class="pager">
				<li class="pager-current first"><span><span>1</span></span></li>
				<li class="pager-item"><a title="На страницу номер 2" href="/generalnaya_uborka/?page=1"><span>2</span></a></li>
				<li class="pager-item"><a title="На страницу номер 3" href="/generalnaya_uborka/?page=2"><span>3</span></a></li>
				<li class="pager-item"><a title="На страницу номер 4" href="/generalnaya_uborka/?page=3"><span>4</span></a></li>
				<li class="pager-item"><a title="На страницу номер 5" href="/generalnaya_uborka/?page=4"><span>5</span></a></li>
				<li class="pager-item"><a title="На страницу номер 6" href="/generalnaya_uborka/?page=5"><span>6</span></a></li>
				<li class="pager-item"><a title="На страницу номер 7" href="/generalnaya_uborka/?page=6"><span>7</span></a></li>
				<li class="pager-item"><a title="На страницу номер 8" href="/generalnaya_uborka/?page=7"><span>8</span></a></li>
				<li class="pager-item"><a title="На страницу номер 9" href="/generalnaya_uborka/?page=8"><span>9</span></a></li>
				<li class="pager-ellipsis">…</li>
				<li class="pager-last"><a title="На страницу номер 17" href="/generalnaya_uborka/?page=16"><span>17</span></a></li>
				<li class="pager-next last"><a title="На следующую страницу" href="/generalnaya_uborka/?page=1"><span>&gt;</span></a></li>
			</ul>
		</div>
		<div class="feed-icon">
			<a href="https://onf.ru/project2_news.rss/46338/" class="feed-icon" title="Подписка на Новости проекта"><img typeof="foaf:Image" src="https://onf.ru/misc/feed.png" width="16" height="16" alt="Подписка на Новости проекта"></a>
		</div>
	</div>
</div>

